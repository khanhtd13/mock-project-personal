﻿using Global.Shared.Commons;

namespace Global.Shared.RoothPathApplication
{
    public static class RootPath
    {
        public static string Get
        {
            get
            {
                var baseDicrectory = AppDomain.CurrentDomain.BaseDirectory;
                var rootPath = baseDicrectory.Substring(0, baseDicrectory.IndexOf(Constant.PROJECT_NAME));
                return rootPath + Constant.SOLUTION_NAME;
            }
        }
    }
}
