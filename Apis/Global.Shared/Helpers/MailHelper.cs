﻿using Global.Shared.Settings.Mail;
using System.Net;
using System.Net.Mail;
using System.Security;

namespace Global.Shared.Helpers
{
    public static class MailHelper
    {
        public static async Task<MailMessage> SendAsync(
            this MailMessage mail,
            UserMailCredential userMailCredential,
            SmtpClientSetting clientSetting)
        {
            using var client = CreateClient(userMailCredential, clientSetting);

            try
            {
                await client.SendMailAsync(mail);
            }
            catch (Exception)
            {
                mail.Body = "Failed to send email.";
            }

            return mail;
        }

        public static MailMessage CreateMail(MailSetting mailSetting)
        {
            if (mailSetting.To is null)
                throw new InvalidOperationException("To cannot be empty.");

            var mail = new MailMessage();

            mail.From = mailSetting.From;

            foreach (var item in mailSetting.To)
            {
                mail.To.Add(item);
            }

            if (mailSetting.CC != null)
            {
                foreach (var item in mailSetting.CC)
                {
                    mail.CC.Add(item);
                }
            }

            mail.Subject = mailSetting.Subject;
            mail.Body = mailSetting.Body;

            mail.IsBodyHtml = true;

            return mail;
        }

        private static SmtpClient CreateClient(UserMailCredential userMailCredential, SmtpClientSetting setting)
        {
            var client = new SmtpClient(setting.Host);

            client.Port = setting.Port;
            client.DeliveryMethod = setting.DeliveryMethod;
            client.EnableSsl = setting.EnableSsl;
            client.UseDefaultCredentials = setting.UseDefaultCredentials;

            var credential = new NetworkCredential(
                userMailCredential.Address, CreateSecureString(userMailCredential.SecureString!));

            client.Credentials = credential;

            return client;
        }

        private static SecureString CreateSecureString(string password)
        {
            var secureString = new SecureString();

            foreach (var c in password)
                secureString.AppendChar(c);

            secureString.MakeReadOnly();

            return secureString;
        }
    }
}