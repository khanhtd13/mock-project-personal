﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.Settings
{
    public class MailTemplateResult
    {
        public string Base { get; init; } = string.Empty;

        public string Styles { get; init; } = string.Empty;

        public string Body { get; init; } = string.Empty;

        public string Signature { get; init; } = string.Empty;
    }
}