﻿using Domain.Enums;
using Global.Shared.Helpers;
using Global.Shared.ViewModels.MailViewModels;
using System.Net.Mail;

namespace Global.Shared.Settings.Mail
{
    public class MailSetting
    {
        public MailSetting(string fromAddress, MailRequestViewModel request, string body)
        {
            From = new MailAddress(fromAddress);

            To = request.ToAddresses!.Split(',').Select(x => new MailAddress(x));

            CC = request.CCAddresses?.Split(',').Select(x => new MailAddress(x));

            Subject = request.MailType!;
            Body = body;
        }

        public MailSetting(string fromAddress, RemindMailRequestViewModel request, string body)
        {
            From = new MailAddress(fromAddress);

            To = request.ToAddresses!.Split(',').Select(x => new MailAddress(x));

            Subject = "Remind";
            Body = body;
        }

        public MailAddress From { get; }

        public IEnumerable<MailAddress> To { get; }

        public IEnumerable<MailAddress>? CC { get; }

        public string Subject { get; }

        public string Body { get; }
    }
}