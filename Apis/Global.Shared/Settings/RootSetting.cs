﻿using Global.Shared.Settings.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.Settings
{
    public class RootSetting
    {
        public SmtpClientSetting? SmtpClientSetting { get; set; }
    }
}
