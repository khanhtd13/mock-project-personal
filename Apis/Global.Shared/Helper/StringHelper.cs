﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.Helper
{
    public static class StringHelper
    {
        public static string[] Split(string stringToSplit)
            => stringToSplit.Split(".");
    }
}
