﻿using Global.Shared.Commons;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.Helper
{
    public static class DateTimeHelper
    {
        private static readonly string _dateFormat = Constant.DATE_TIME_FORMAT_MMddyyyy;
        private static readonly CultureInfo _cultureInfo = CultureInfo.InvariantCulture;
        private static readonly int _month = DateTime.UtcNow.Month;
        private static readonly int _year = DateTime.UtcNow.Year;
        private static readonly DateTime _currentDate = DateTime.UtcNow;
        public static DateTime GetStartDateOfMonth()
            => DateTime.ParseExact($"{_month:00}/01/{_year}",
                                   _dateFormat,
                                   _cultureInfo);

        public static DateTime GetEndDateOfMonth()
            => DateTime.ParseExact($"{_month:00}/{DateTime.DaysInMonth(_year, _month)}/{_year}",
                                   _dateFormat,
                                   _cultureInfo);

        public static DateTime GetMonday()
            => _currentDate.AddDays(
                (_currentDate.Day - (int)_currentDate.DayOfWeek + 1) - _currentDate.Day);

        public static DateTime GetSunday()
            => _currentDate.AddDays(
                (_currentDate.Day - (int)_currentDate.DayOfWeek + 7) - _currentDate.Day);
    }
}
