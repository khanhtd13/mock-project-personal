﻿namespace Global.Shared.ViewModels.ModuleViewModels
{
    public class ModuleUpdateViewModel: ModuleAddViewModel
    {
        public bool IsDeleted { get; set; }
    }
}
