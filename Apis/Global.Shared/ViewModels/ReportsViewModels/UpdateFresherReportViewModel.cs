﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.ViewModels.ReportsViewModels
{
    public class UpdateFresherReportViewModel
    {
        public StatusFresherEnum? Status { get; set; }
        public string Note { get; set; }
    }
}
