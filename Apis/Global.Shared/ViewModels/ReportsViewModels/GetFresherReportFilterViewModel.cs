﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.ViewModels.ReportsViewModels
{
    public class GetFresherReportFilterViewModel
    {
        public string? CourseCode { get; set; }
        public string? Account { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
    }
}
