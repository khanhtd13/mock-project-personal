﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Shared.ViewModels.ReportsViewModels
{
    public class GetWeeklyFresherReportFilterViewModel
    {
        public string? CourseCode { get; set; }
        public string? FromDate { get; set; }
        public string? ToDate { get; set; }
    }
}
