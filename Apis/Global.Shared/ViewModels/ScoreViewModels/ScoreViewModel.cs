﻿using Domain.Enums;

namespace Global.Shared.ViewModels.ScoreViewModels
{
    public class ScoreViewModel
    {
        public Guid Id { get; set; }

        public Guid FresherId { get; set; }

        public Guid ScoreMarkerId { get; set; }

        public Guid ModuleId { get; set; }

        public TypeScoreEnum TypeScore { get; set; }

        public double ModuleScore { get; set; }

        public DateTime CreationDate { get; set; }

    }
}
