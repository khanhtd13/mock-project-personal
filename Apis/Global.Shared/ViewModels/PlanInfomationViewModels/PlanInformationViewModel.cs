﻿namespace Global.Shared.ViewModels.PlanInfomationViewModels
{
    public class PlanInformationViewModel
    {
        public Guid Id { get; set; }
        public Guid ClassId { get; set; }
        public string? PlanName { get; set; }
        public string? ModuleName { get; set; }
        public string? TopicName { get; set; }
        public string? Pic { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? Duration { get; set; }
        public string? NoteDetail { get; set; }
        public bool IsDeleted { get; set; }
    }
}
