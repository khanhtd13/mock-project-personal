﻿namespace Global.Shared.ViewModels.ImportViewModels
{
    public class PackageReponseImportViewModel
    {
        public PackageReponseImportViewModel()
        {
            ListaccountImpVM = new List<AccountImportViewModel>();
            ListclassImpVM = new List<ClassImportViewModel>();
            ListfresherImpVM = new List<FresherImportViewModel>();
        }
        public IEnumerable<AccountImportViewModel> ListaccountImpVM { get; set; }
        public IEnumerable<ClassImportViewModel> ListclassImpVM { get; set; }
        public IEnumerable<FresherImportViewModel> ListfresherImpVM { get; set; }
    }
}
