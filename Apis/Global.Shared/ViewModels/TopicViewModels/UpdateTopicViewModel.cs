﻿namespace Global.Shared.ViewModels.TopicViewModels
{
    public class UpdateTopicViewModel: CreateTopicViewModel
    {
        public bool IsDeleted { get; set; }

    }
}
