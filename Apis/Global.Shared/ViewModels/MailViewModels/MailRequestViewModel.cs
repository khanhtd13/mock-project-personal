namespace Global.Shared.ViewModels.MailViewModels
{
    public class MailRequestViewModel
    {
        public string? ToAddresses { get; set; }

        public string? CCAddresses { get; set; }

        public string? MailType { get; set; }

        public bool IsSignatureLong { get; set; } = true;
    }
}