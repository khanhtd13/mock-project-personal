﻿using Global.Shared.ViewModels.ModuleViewModels;

namespace Global.Shared.ViewModels.PlanViewModels
{
    public class PlanGetByIdViewModel
    {
        public Guid Id { get; set; }
        public string? CourseName { get; set; }
        public string? CourseCode { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public Guid? ModificationBy { get; set; }
        public Guid? DeleteBy { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<ModuleViewModel>? Modules { get; set; }
    }
}
