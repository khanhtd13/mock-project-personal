﻿namespace Global.Shared.ViewModels.PlanViewModels
{
    public class PlanUpdateViewModel: PlanAddViewModel
    {
        public bool IsDeleted { get; set; }
    }
}
