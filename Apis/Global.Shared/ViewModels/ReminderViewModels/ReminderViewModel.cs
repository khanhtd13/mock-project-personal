﻿namespace Global.Shared.ViewModels.ReminderViewModels
{
    public class ReminderViewModel
    {
        public string Subject  { get; set; }

        public string Description { get; set; }
        public string ReminderEmail { get; set; }
    }
}
