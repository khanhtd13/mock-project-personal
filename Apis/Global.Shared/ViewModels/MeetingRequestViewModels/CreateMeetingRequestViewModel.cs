﻿namespace Global.Shared.ViewModels.MeetingRequestViewModels
{
    public class CreateMeetingRequestViewModel
    {
        public string OrganizerEmail { get; set; }

        public string Subject { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
        
        public string[] Attendees { get; set; }
    }
}
