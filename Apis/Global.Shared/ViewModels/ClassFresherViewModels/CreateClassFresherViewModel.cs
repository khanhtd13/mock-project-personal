﻿
namespace Global.Shared.ViewModels
{
    public class CreateClassFresherViewModel
    {
        public Guid Id { get; set; }
        public string? ClassCode { get; set; }
        public string? CLassName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string? Location { get; set; }
        public string? NameAdmin1 { get; set; }
        public string? EmailAdmin1 { get; set; }
        public string? NameAdmin2 { get; set; }
        public string? EmailAdmin2 { get; set; }
        public string? NameAdmin3 { get; set; }
        public string? EmailAdmin3 { get; set; }
        public string? NameTrainer1 { get; set; }
        public string? EmailTrainer1 { get; set; }
        public string? NameTrainer2 { get; set; }
        public string? EmailTrainer2 { get; set; }
        public string? NameTrainer3 { get; set; }
        public string? EmailTrainer3 { get; set; }
        public bool IsDone { get; set; }
        public int PlanId { get; set; }
        public double Budget { get; set; }
        public List<FresherViewModel> Freshers { get; set; }
    }
}
