﻿namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class UpdateFeedbackViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
