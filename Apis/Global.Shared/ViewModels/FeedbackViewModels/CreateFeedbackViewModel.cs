﻿namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class CreateFeedbackViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
