﻿namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class FeedbackQuestionViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public Guid FeedBackId { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public DateTime? DeletionDate { get; set; }
    }
}
