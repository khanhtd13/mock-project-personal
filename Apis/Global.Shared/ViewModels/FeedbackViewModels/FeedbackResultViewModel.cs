﻿namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class FeedbackResultViewModel
    {
        public Guid Id { get; set; }
        public Guid FeedBackId { get; set; }
        public Guid QuestionId { get; set; }
        public Guid AccountFresherId { get; set; }
        public string QuestionTitle { get; set; }
        public string AccountName { get; set; }
        public string Fullname { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public DateTime? DeletionDate { get; set; }
    }
}
