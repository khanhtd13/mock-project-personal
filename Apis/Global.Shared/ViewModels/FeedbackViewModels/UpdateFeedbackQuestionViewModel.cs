﻿namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class UpdateFeedbackQuestionViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public Guid FeedBackId { get; set; }
    }
}
