﻿using System.ComponentModel.DataAnnotations;

namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class CreateFeedbackQuestionViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public Guid? FeedBackId { get; set; }
    }
}
