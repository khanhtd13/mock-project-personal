﻿namespace Global.Shared.ViewModels.FeedbackViewModels
{
    public class SearchFeedbackQuestionViewModel
    {
        public string? Title { get; set; }
        public Guid? FeedBackId { get;set; }
        public string? CreateBy { get;set; }
        public DateTime? CreationDate { get; set; }
        public int PageIndex { get; set; } = 0;
        public int PageSize { get; set; } = 10;
    }
}
