﻿namespace Global.Shared.Commons
{
    public static class Constant
    {
        public const string DATE_TIME_FORMAT_MMddyyyy = "MM/dd/yyyy";
        public const string ID_CAN_NOT_EMPTY_NOTICE = "Id can not be empty";
        public const string RETURN_NULL_NOTICE = "Can't update Report because Server return null";
        public const string CAN_NOT_UPDATE_REPORT_NOTICE = "Can't update Report";
        public const string UPDATE_REPORT_SUCCESSFULLY_NOTICE = "Update Report succesfully";
        public const string COURSECODE_CAN_NOT_EMPTY_NOTICE = "CourseCode can not be empty";
        public const string CREATE_REPORT_SUCCESSFULLY_NOTICE = "Create Report succesfully";
        public const string CAN_NOT_CREATE_REPORT_NOTICE = "Can't create Report";
        public const string FILEPATH_DELIVERY_TEMPLATE = "/Global.Shared/FileExcelTemplates/EmployeeTrainingDelivery.xlsx";
        public const string FILEPATH_HISTORY_TEMPLATE = "/Global.Shared/FileExcelTemplates/EmployeeTrainingHistory.xlsx";

        public const string EMPLOYEE_INFO_HEADER = "Employee Info";
        public const string COURSE_INFO_HEADER = "Course Info";
        public const string TRAINEE_INFO_HEADER = "Trainee Info";
        public const string VALIDATION_AND_SUPPORT_INFO_HEADER = "Validation & Supporting Info";

        public const string WORKSHEET_RECORD_OF_CHANGES = "Record of changes";
        public const string WORKSHEET_UNI_AND_FALCULTY_LIST = "Uni& Faculty_List";
        public const string WORKSHEET_FINANCE_OBLIGATION = "Finance obligation";
        public const string WORKSHEET_GUIDELINE = "Guideline";
        public const string WORKSHEET_COURSES_SEMINARS_WORKSHOPS = "Courses, seminars, workshops";
        public const string WORKSHEET_EXAMS_AND_CERTIFICATE_SUPPORT = "Exams & Certificate support";

        public const string EXPORT_FILENAME_PREFIX_HISTORY = "FA_HCM_Employee Training History Database_";
        public const string EXPORT_FILENAME_PREFIX_DELIVERY = "FA_HCM_Employee Training Delivery Database_";
        public const string EXPORT_FILE_EXTENSION = ".xlsx";

        public const string EXCEPTION_NOT_FOUND_FEEDBACK = "Not found Feedback with id";
        public const string EXCEPTION_NOT_FOUND_FEEDBACK_QUESTION = "Not found Feedback Question with id";
        public const string EXCEPTION_REMOVE_FAILED = "Can't remove this object";

        public const string DOMAIN_EMAIL_FSOFT = "@fsoft.com.vn";
        public const string IMPORT_FAIL = "Import fail!";
        public const string EXCEPTION_NOT_FOUND_FRESHER = "Fresher not found";
        public const string EXCEPTION_CREATE_CLASS = "Create class fail!";
        public const string EXCEPTION_CLASS_NOT_FOUND = "CLass Fresher not found";
        public const string EXCEPTION_UPDATE_CLASS = "Update class fail!";
        public const string EXCEPTION_LIST_FRESHER_NOT_FOUND = "List fresher not found";
        public const string EXCEPTION_CHANGE_STATUS_FRESHER = "Change staus for fresher fail!";
        public const string EXCEPTION_ID_FRESHER_EMPTY = "Id fresher is empty";

        public const string PROJECT_NAME = "fresher-management-system";
        public const string SOLUTION_NAME = "Apis";
    }
}
