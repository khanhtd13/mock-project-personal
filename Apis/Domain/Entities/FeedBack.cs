﻿namespace Domain.Entities
{
    public class FeedBack: BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public ICollection<FeedBackQuestion>? FeedBackQuestions { get; set; }
        public ICollection<FeedBackResult>? FeedBackResults { get; set; }
    }
}
