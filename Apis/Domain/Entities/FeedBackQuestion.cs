﻿namespace Domain.Entities
{
    public class FeedBackQuestion: BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public Guid FeedBackId { get; set; }
        public virtual FeedBack? FeedBack { get; set; }
        public virtual ICollection<FeedBackResult>? FeedBackResults { get; set; }
    }
}
