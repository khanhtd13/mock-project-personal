﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Audit : BaseEntity
    {
        public string IdClass { get; set; }
        public string ModuleId { get; set; }
        public string FresherId { get; set; }
        public string AuditorId { get; set; }
        public string QuestionQ1 { get; set; }
        public string CommentQ1 { get; set; }
        public string EvaluateQ1 { get; set; }
        public string QuestionQ2 { get; set; }
        public string CommentQ2 { get; set; }
        public string EvaluateQ2 { get; set; }
        public string QuestionQ3 { get; set; }
        public string CommentQ3 { get; set; }
        public string EvaluateQ3 { get; set; }
        public string QuestionQ4 { get; set; }
        public string CommentQ4 { get; set; }
        public string EvaluateQ4 { get; set; }
        public DateTime DateStart { get; set; }
        public string PracticeComment { get; set; }
        public decimal PracticeScore { get; set; }
        public decimal AuditScore { get; set; }
        public RankEnum Rank { get; set; }
        public string AuditComment { get; set; }
    }
}
