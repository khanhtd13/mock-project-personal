﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Score : BaseEntity
    {
        public Guid FresherId { get; set; }

        public Guid ScoreMarkerId { get; set; }

        public Guid ModuleId { get; set; }

        public TypeScoreEnum TypeScore { get; set; }

        public double? ModuleScore { get; set; }

    }
}
