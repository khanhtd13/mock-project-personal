﻿namespace Domain.Entities
{
    public class ResultMonth : BaseEntity
    {
        public Guid AccountId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public double GPA { get; set; }

        public double Disciplinary { get; set; }

        public double AcademicMark { get; set; }
    }
}
