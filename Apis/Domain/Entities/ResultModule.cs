﻿namespace Domain.Entities
{
    public class ResultModule : BaseEntity
    {
        public Guid AccountId { get; set; }

        public Guid ModuleId { get; set; }

        public double AssignmentAvg { get; set; }

        public double FinalAudit { get; set; }

        public double WeightedNumberAssignment { get; set; }

        public double WeighttedNumberFinal { get; set; }
    }
}
