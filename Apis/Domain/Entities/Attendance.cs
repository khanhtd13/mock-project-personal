﻿using Domain.Enums;

namespace Domain.Entities
{
    public class Attendance : BaseEntity
    {
        public Guid FresherId { get; set; }
        public StatusAttendanceEnum Status { get; set; }
        public bool IsAttended { get; set; } = true;
        public DateTime AttendDate { get; set; }
        public string? Note { get; set; }
    }
}
