﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class FreshersJobRank
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public string JobRankCode { get; set; } = null!;
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public string JobRankName { get; set; } = null!;
    }
}
