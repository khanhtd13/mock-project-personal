﻿namespace Domain.Entities
{
    public class PlanInformation : BaseEntity
    {
        public Guid? ClassId { get; set; }
        public string? PlanName { get; set; }
        public string? ModuleName { get; set; }
        public string? TopicName { get; set; }
        public string? Pic { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public double? Duration { get; set; }
        public string? NoteDetail { get; set; }
    }
}
