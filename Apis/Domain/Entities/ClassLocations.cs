﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class ClassLocations
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public string LocationCode { get; set; } = null!;
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public string LocationName { get; set; } = null!;
    }
}
