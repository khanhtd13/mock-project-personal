﻿namespace Domain.Entities
{
    public class FeedBackResult : BaseEntity
    {
        public Guid FeedBackId { get; set; }
        public Guid QuestionId { get; set; }
        public Guid AccountFresherId { get; set; }
        public string AccountName { get; set; }
        public string QuestionTitle { get; set; }
        public string Fullname { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public virtual FeedBack? FeedBack { get; set; }
        public virtual FeedBackQuestion? FeedBackQuestion { get; set; }

    }
}
