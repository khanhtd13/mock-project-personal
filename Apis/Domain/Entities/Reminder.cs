﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public  class Reminder : BaseEntity
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime RemindTime1 { get; set; }
        public DateTime? RemindTime2 { get; set; }
        public bool IsCompleted { get; set; }
        public ReminderTypeEnum ReminderType { get; set; }
        public string ReminderEmail { get; set; }
    }
}
