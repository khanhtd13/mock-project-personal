﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class FresherMajor
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public int MajorPriority { get; set; }
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public string MajorName { get; set; } = null!;

    }
}
