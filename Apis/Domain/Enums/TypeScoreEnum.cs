﻿namespace Domain.Enums
{
    public enum TypeScoreEnum
    {
        AssignmentScore,
        BonusScore,
        PenaltyScore,
        AuditScore,
        PracticeScore
    }
}
