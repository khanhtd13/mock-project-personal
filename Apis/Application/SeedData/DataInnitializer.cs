﻿using Newtonsoft.Json;

namespace Application.SeedData
{
    public static class DataInnitializer
    {
        public static List<T> SeedData<T>(string filename)
        {
            var myJsonString = File.ReadAllText($"../Application/SeedData/Samples/{filename}.json");
            var data = JsonConvert.DeserializeObject<List<T>>(myJsonString);
            return data;
        }
    }
}
