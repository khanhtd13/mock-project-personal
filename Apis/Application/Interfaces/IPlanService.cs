﻿using Global.Shared.Commons;
using Global.Shared.ViewModels.PlanInfomationViewModels;
using Global.Shared.ViewModels.PlanViewModels;

namespace Application.Interfaces
{
    public interface IPlanService
    {
        Task<Pagination<PlanGetViewModel>> GetAllAsync(int pageIndex, int pageSize);
        Task<PlanAddViewModel> AddItemPlanAsync(
                        PlanAddViewModel planAddViewModel);
        Task<PlanGetByIdViewModel> GetPlanByIdAsync(Guid id);
        Task<PlanUpdateViewModel> UpdatePlanAsync(Guid id,
                        PlanUpdateViewModel planUpdateViewModel);
        Task<List<PlanInformationViewModel>> ChoosePlanForClassAsync(
                        ChoosePlanForClassViewModel planInfomationAddViewModel);
        Task SeedDataPlans();
    }
}
