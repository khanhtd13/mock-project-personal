﻿using Global.Shared.Commons;
using Global.Shared.ViewModels.TopicViewModels;

namespace Application.Interfaces
{
    public interface ITopicService
    {
        Task<Pagination<TopicViewModel>> GetAllAsync(int pageIndex, int pageSize);
        Task<TopicViewModel> GetTopicByIdAsync(Guid topicId);
        Task<List<TopicViewModel>> GetTopicByModuleIdAsync(Guid moduleId);
        Task<CreateTopicViewModel> AddTopicAsync(
                CreateTopicViewModel createTopicViewModel);
        Task<UpdateTopicViewModel> UpdateTopicAsync(
                Guid topicId, UpdateTopicViewModel updateTopicViewModel);
        Task SeedDataTopics();
    }
}
