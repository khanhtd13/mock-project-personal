﻿using Global.Shared.ViewModels.PlanInfomationViewModels;

namespace Application.Interfaces
{
    public interface IPlanInformationService
    {
        Task<List<PlanInformationViewModel>?> GetPlanDetailByClassIdAsync(Guid classId);
        Task<bool?> UpdatePlanInfoAsync(
                    Guid planId, PlanInformationViewModel planInformationViewModels);
    }
}
