﻿using Global.Shared.ViewModels.ScoreViewModels;

namespace Application.Interfaces
{
    public interface IScoreService
    {
        Task<List<ScoreViewModel>?> GetAllScoreAsync();
        Task<ScoreViewModel?> CreateScoreAsync(CreateScoreViewModel
                                                                  createScoreViewModel);
        Task<ScoreViewModel?> UpdateScoreAsync(UpdateScoreViewModel
                                                                updateScoreViewModel);
        Task DeleteScoreAsync(Guid id);
    }
}
