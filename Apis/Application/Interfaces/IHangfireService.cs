﻿using System.Linq.Expressions;

namespace Application.Interfaces
{
    public interface IHangfireService
    {
        public void CreateFireAndForgetTask(Expression<Func<Task>> expression);
        public void CreateFireAndForgetTask<TValue>(Expression<Func<TValue, Task>> methodCall);
        public void CreateDelayedTask(Expression<Func<Task>> expression, DateTime dateExecute);
        public void CreateDelayedTask<TValue>(Expression<Func<TValue, Task>> methodCall, DateTime dateExecute);
    }
}
