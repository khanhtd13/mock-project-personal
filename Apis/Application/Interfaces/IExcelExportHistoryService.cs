﻿using Microsoft.AspNetCore.Mvc;

namespace Application.Interfaces
{
    public interface IExcelExportHistoryService
    {
        public Task<FileContentResult> Export<TParams>(TParams values);
    }
}
