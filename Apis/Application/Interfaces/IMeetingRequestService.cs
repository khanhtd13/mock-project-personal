﻿using Global.Shared.ViewModels.MeetingRequestViewModels;

namespace Application.Interfaces
{
    public interface IMeetingRequestService
    {
        Task<string> CreateMeetingRequestAsync(
            CreateMeetingRequestViewModel createMeetingRequest,
            IDeviceCodeNotifier deviceCodeNotifier);
    }
}
