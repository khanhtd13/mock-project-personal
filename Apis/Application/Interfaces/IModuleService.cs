﻿using Global.Shared.Commons;
using Global.Shared.ViewModels.ModuleViewModels;

namespace Application.Interfaces
{
    public interface IModuleService
    {
        Task<Pagination<ModuleViewModel>?> GetAllAsync(int pageIndex, int pageSize);
        Task<ModuleViewModel?> GetByIdAsync(Guid id);
        Task<List<ModuleViewModel>?> GetByPlanIdAsync(Guid planId);
        Task<ModuleAddViewModel?> AddModuleAsync(ModuleAddViewModel moduleAddViewModel);
        Task<ModuleUpdateViewModel?> UpdateModuleAsync(Guid id,
                            ModuleUpdateViewModel moduleUpdateViewModel);
        Task SeedDataModules();
    }
}
