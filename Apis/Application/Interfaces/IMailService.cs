﻿using Global.Shared.ViewModels.MailViewModels;


namespace Application.Interfaces
{
    public interface IMailService
    {
        Task<MailViewModel> SendAsync(MailRequestViewModel request);
    }
}