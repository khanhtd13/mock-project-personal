﻿using Global.Shared.ViewModels.ImportViewModels;
using Microsoft.AspNetCore.Http;

namespace Application.Interfaces
{
    public interface IImportDataService
    {
        public Task<PackageReponseImportViewModel?>
            GetDataFromFileExcelAsync(IFormFile? fileExcel);
        public Task<PackageReponseImportViewModel>
            CreateClassCodeFromPackageDataReponseAsync(PackageReponseImportViewModel package);
    }
}

