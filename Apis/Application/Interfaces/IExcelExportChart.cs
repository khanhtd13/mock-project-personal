﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Application.Interfaces
{
    public interface IExcelExportChartService
    {
        public Task<FileContentResult> Export(Dictionary<string,float> values);
    }
}