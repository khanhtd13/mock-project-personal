﻿using Global.Shared.Settings.Mail;
using System.Security;

namespace Application.Interfaces
{
    public interface IUserMailCredentialService
    {
        UserMailCredential Credential { get; }
    }
}