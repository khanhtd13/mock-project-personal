﻿using Domain.Enums;
using Global.Shared.ViewModels;

namespace Application.Interfaces
{
    public interface IFresherService
    {
        Task<FresherViewModel> GetFresherByIdAsync(Guid id);
        Task<FresherViewModel> ChangeFresherStatus(Guid id, StatusFresherEnum status);
        Task SeedDataFresher();
        
    }
}
