﻿using Microsoft.AspNetCore.Mvc;

namespace Application.Interfaces
{
    public interface IExcelExportDeliveryService
    {
        public Task<FileContentResult> Export();
    }
}