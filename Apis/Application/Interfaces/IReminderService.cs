﻿using Domain.Enums;
using Global.Shared.ViewModels.ReminderViewModels;

namespace Application.Interfaces
{
    public interface IReminderService
    {
        Task<bool> CreateReminderAsync(CreateReminderViewModel reminderViewModel);

        Task<IList<ReminderViewModel>> GetAllReminderByDateAsync(DateTime date);
        Task SendReminderMailAsync(DateTime date);
    }
}
