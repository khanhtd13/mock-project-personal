﻿using Global.Shared.Commons;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Http;

namespace Application.Interfaces
{
    public interface IClassFresherService
    { 
        Task<ClassFresherViewModel> CreateClassFresherAsync(CreateClassFresherViewModel createClassFresherViewModel);
        Task<ClassFresherViewModel> GetClassFresherByIdAsync(Guid id);
        Task<ClassFresherViewModel> GetClassHasFresherByIdAsync(Guid id);
        Task<List<string>> GetAllClassCodeAsync();
        Task<Pagination<ClassFresherViewModel>> GetAllClassFreshersPagingsionAsync(int pageIndex = 0, int pageSize = 10);
        Task<ClassFresherViewModel> UpdateClassFresher(ClassFresherViewModel classFresherViewModel);
        /// <summary>
        /// Get a list fresher to add into new class 
        /// </summary>
        /// <param name="location">Get by location of fresher</param>
        /// <param name="skill">Get by skill of fresher</param>
        /// <param name="year">Get by year of fresher</param>
        /// <returns>A list fresher</returns>
        Task<List<FresherViewModel>> GetFreshersByClassCodeAsync(string classCode);
        Task<List<ClassFresherViewModel>> CreateCLassFresherFromImportedExcelFile(IFormFile fileExcel);
        Task SeedData ();
    }
}
