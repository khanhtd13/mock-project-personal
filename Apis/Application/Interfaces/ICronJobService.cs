﻿namespace Application.Interfaces
{
    public interface ICronJobService
    {
        public Task AutoGetChemicalAsync();
        public Task AutoGetChemicalPagingsionAsync();
    }
}
