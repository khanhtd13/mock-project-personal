﻿using Application.Interfaces;
using Application.SeedData;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Global.Shared.Commons;
using Global.Shared.Exeption;
using Global.Shared.ViewModels;

namespace Application.Services
{
    public class FresherService : IFresherService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public FresherService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<FresherViewModel> ChangeFresherStatus(Guid id, StatusFresherEnum status)
        {
            var fresherToUpdaeStatus = await _unitOfWork.FresherRepository.GetByIdAsync(id);
            if(fresherToUpdaeStatus == null)
                throw new AppException(Constant.EXCEPTION_NOT_FOUND_FRESHER);
            fresherToUpdaeStatus.Status = status;
            _unitOfWork.FresherRepository.Update(fresherToUpdaeStatus);

            var isSucccess = await _unitOfWork.SaveChangeAsync() > 0;
            //check if not found a fresher
            if (!isSucccess)
                throw new AppException(Constant.EXCEPTION_CHANGE_STATUS_FRESHER);
            return _mapper.Map<FresherViewModel>(fresherToUpdaeStatus);
        }

        public async Task<FresherViewModel> GetFresherByIdAsync(Guid id)
        {
            if (id == Guid.Empty)
                throw new AppException(Constant.EXCEPTION_ID_FRESHER_EMPTY);
            var fresher = await _unitOfWork.FresherRepository.GetByIdAsync(id);

            // check if not found fresher
            if (fresher == null)
                throw new AppException(Constant.EXCEPTION_NOT_FOUND_FRESHER);
            return _mapper.Map<FresherViewModel>(fresher);
        }
     
        public async Task SeedDataFresher()
        {
            var data = DataInnitializer.SeedData<Fresher>("Freshers");
            _unitOfWork.FresherRepository.SeedData(data);
            await _unitOfWork.SaveChangeAsync();
        }

        
    }
}
