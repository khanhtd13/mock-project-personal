﻿using Application.Interfaces;
using Hangfire;
using System.Linq.Expressions;

namespace Application.Services
{
    public class HangfireService : IHangfireService
    {
        private readonly IBackgroundJobClient _backgroundJob;
        private readonly ICurrentTime _currentTime;
        public HangfireService(IBackgroundJobClient backgroundJob, ICurrentTime currentTime)
        {
            _backgroundJob = backgroundJob;
            _currentTime = currentTime;
        }
        public void CreateDelayedTask(Expression<Func<Task>> expression, DateTime dateExecute)
        {
            var now = _currentTime.GetCurrentTime();
            var time = dateExecute.Subtract(now);
            _backgroundJob.Schedule(expression, time);
        }
        public void CreateDelayedTask<TValue>(Expression<Func<TValue, Task>> methodCall, DateTime dateExecute)
        {
            var now = _currentTime.GetCurrentTime();
            var time = dateExecute.Subtract(now);
            _backgroundJob.Schedule(methodCall, time);
        }
        public void CreateFireAndForgetTask(Expression<Func<Task>> expression)
        {
            _backgroundJob.Enqueue(expression);
        }
        public void CreateFireAndForgetTask<TValue>(Expression<Func<TValue, Task>> methodCall)
        {
            _backgroundJob.Enqueue(methodCall);

        }
    }
}
