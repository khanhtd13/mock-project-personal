﻿using Application.Interfaces;
using AutoMapper;
using Global.Shared.ViewModels.PlanInfomationViewModels;

namespace Application.Services
{
    public class PlanInformationService : IPlanInformationService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public PlanInformationService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<List<PlanInformationViewModel>?>
                                     GetPlanDetailByClassIdAsync(Guid classId)
        {
            var planInfomations = await _unitOfWork.PlanInformationRepository
                                                    .GetByClassIdAsync(classId);
            if (planInfomations.Count <= 0)
            {
                return null;
            }
            var planInfoViewModels = _mapper.Map
                                <List<PlanInformationViewModel>>(planInfomations);
            return planInfoViewModels;
        }
        public async Task<bool?> UpdatePlanInfoAsync(
                    Guid planId, PlanInformationViewModel
                                    planInformationViewModel)
        {
            var planInfo = await _unitOfWork.PlanInformationRepository
                                            .GetByIdAsync(planId);
            if (planInfo == null)
            {
                return false;
            }
            planInfo = _mapper.Map(planInformationViewModel, planInfo);
            _unitOfWork.PlanInformationRepository.Update(planInfo);
            var rowsAffect = await _unitOfWork.SaveChangeAsync() > 0;
            if (!rowsAffect)
            {
                throw new Exception("SaveChange Failed!");
            }
            return true;
        }
    }
}
