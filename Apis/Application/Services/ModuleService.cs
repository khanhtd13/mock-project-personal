﻿using Application.Interfaces;
using Application.SeedData;
using AutoMapper;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.ModuleViewModels;

namespace Application.Services
{
    public class ModuleService : IModuleService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public ModuleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<ModuleAddViewModel?> AddModuleAsync(
                                ModuleAddViewModel moduleAddViewModel)
        {
            var module = _mapper.Map<Module>(moduleAddViewModel);
            await _unitOfWork.ModuleRepository.AddAsync(module);
            var rowsExpected = await _unitOfWork.SaveChangeAsync();
            if (rowsExpected <= 0)
            {
                throw new Exception("SaveChange Failed!");
            }
            return moduleAddViewModel;
        }
        public async Task<Pagination<ModuleViewModel>?> GetAllAsync(int pageIndex, int pageSize)
        {
            var modules = await _unitOfWork.ModuleRepository.FindAsync(null, null, pageIndex, pageSize);
            if (modules==null)
            {
                return null;
            }
            var moduleGetAllViewModules = _mapper.Map<Pagination<ModuleViewModel>>(modules);
            return moduleGetAllViewModules;
        }
        public async Task<ModuleViewModel?> GetByIdAsync(Guid id)
        {
            var module = await _unitOfWork.ModuleRepository
                                          .GetByIdAsync(id,
                                                    x => x.Topics);
            if (module == null)
            {
                return null;
            }
            var moduleGetByIdViewModule = _mapper.Map<ModuleViewModel>(module);
            return moduleGetByIdViewModule;
        }
        public async Task<List<ModuleViewModel>?> GetByPlanIdAsync(Guid planId)
        {
            var modules = await _unitOfWork.ModuleRepository.GetModuleByPlanId(planId);
            var moduleGetByPlanId = _mapper.Map<List<ModuleViewModel>>(modules);
            return moduleGetByPlanId;
        }
        public async Task<ModuleUpdateViewModel?> UpdateModuleAsync(
                                 Guid id, ModuleUpdateViewModel moduleUpdateViewModel)
        {
            var module = await _unitOfWork.ModuleRepository.GetByIdAsync(id);
            if (module == null)
            {
                return null;
            }
            module = _mapper.Map(moduleUpdateViewModel, module);
            //update item Module
            _unitOfWork.ModuleRepository.Update(module);
            var rowsEffected = await _unitOfWork.SaveChangeAsync() > 0;
            if (!rowsEffected)
            {
                throw new Exception("Not SaveChange");
            }
            return moduleUpdateViewModel;
        }
        public async Task SeedDataModules()
        {
            var data = DataInnitializer.SeedData<Module>("Modules");
            _unitOfWork.ModuleRepository.SeedData(data);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
