﻿using Application.Interfaces;
using Application.SeedData;
using AutoMapper;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.TopicViewModels;

namespace Application.Services
{
    public class TopicService : ITopicService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TopicService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<CreateTopicViewModel> AddTopicAsync(
                                CreateTopicViewModel createTopicViewModel)
        {
            //Check module is exist
            var module = await _unitOfWork.ModuleRepository
                                          .GetByIdAsync(createTopicViewModel.ModuleId);
            if (module == null)
            {
                return null;
            }
            var topicModel = _mapper.Map<Topic>(createTopicViewModel);
            await _unitOfWork.TopicRepository.AddAsync(topicModel);
            //update DurationTotal of Module
            module.DurationTotal = module.DurationTotal + topicModel.Duration;
            _unitOfWork.ModuleRepository.Update(module);
            var rowsAffect = await _unitOfWork.SaveChangeAsync() > 0;
            //Check SaveChange
            if (!rowsAffect)
            {
                throw new Exception("SaveChange Failed");
            }
            return createTopicViewModel;
        }
        public async Task<Pagination<TopicViewModel>> GetAllAsync(int pageIndex,int pageSize)
        {
            var result = await _unitOfWork.TopicRepository.FindAsync(null, null, pageIndex, pageSize);
            var topicListModel = _mapper.Map<Pagination<TopicViewModel>>(result);
            return topicListModel;
        }
        public async Task<TopicViewModel> GetTopicByIdAsync(Guid topicId)
        {
            var result = await _unitOfWork.TopicRepository.GetByIdAsync(topicId);
            var topicModel = _mapper.Map<TopicViewModel>(result);
            return topicModel;
        }
        public async Task<List<TopicViewModel>> GetTopicByModuleIdAsync(Guid moduleId)
        {
            var topicList = await _unitOfWork.TopicRepository.GetByModuleId(moduleId);
            var topicModel = _mapper.Map<List<TopicViewModel>>(topicList);
            return topicModel;
        }
        public async Task<UpdateTopicViewModel> UpdateTopicAsync(
                        Guid topicId, UpdateTopicViewModel updateTopicViewModel)
        {
            var topic = await _unitOfWork.TopicRepository.GetByIdAsync(topicId);
            if (topic == null)
            {
                throw new Exception("Not Found Topic!");
            }
            double oldDuration = topic.Duration;
            double newDuration = updateTopicViewModel.Duration;
            
            var topicUpdate = _mapper.Map(updateTopicViewModel, topic);
            _unitOfWork.TopicRepository.Update(topicUpdate);
            //update DurationTotal of Module
            var module = await _unitOfWork.ModuleRepository
                                          .GetByIdAsync(topicUpdate.ModuleId);
            
            module.DurationTotal = module.DurationTotal - oldDuration + newDuration;
            _unitOfWork.ModuleRepository.Update(module);
            var rowsAffectModule = await _unitOfWork.SaveChangeAsync() > 0;
            //Check SaveChange
            if (!rowsAffectModule)
            {
                throw new Exception("SaveChange Failed");
            }
            return updateTopicViewModel;
        }
        public async Task SeedDataTopics()
        {
            var data = DataInnitializer.SeedData<Topic>("Topics");
            _unitOfWork.TopicRepository.SeedData(data);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}

