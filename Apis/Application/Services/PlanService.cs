﻿using Application.Interfaces;
using Application.SeedData;
using AutoMapper;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.PlanInfomationViewModels;
using Global.Shared.ViewModels.PlanViewModels;
using System.Globalization;

namespace Application.Services
{
    public class PlanService : IPlanService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public PlanService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<PlanInformationViewModel>> ChoosePlanForClassAsync(
                        ChoosePlanForClassViewModel choosePlanForClassViewModel)
        {
            var classId = choosePlanForClassViewModel.ClassId;
            var planId = choosePlanForClassViewModel.PlanId;
            DateTime startDate = DateTime.ParseExact(
                                        choosePlanForClassViewModel.StartDate,
                                        "dd/MM/yyyy",
                                         CultureInfo.InvariantCulture);
            //Get Plan by id
            var plan = await _unitOfWork.PlanRepository.GetByIdAsync(planId);
            if (plan == null)
            {
                throw new Exception("Not Found Plan!");
            }
            //Find Module by PlanId
            var modules = await _unitOfWork.ModuleRepository
                                           .GetModuleByPlanId(planId);
            if (modules == null)
            {
                throw new Exception("Not Found Module Of !" + planId);
            }
            //new list PlanInformation
            var planInfoAddViewModels = new List<PlanInformationViewModel>();
            for (int i = 0; i < modules.Count; i++)
            {
                var topics = await _unitOfWork.TopicRepository
                                              .GetByModuleId(modules[i].Id);
                var itemPlanInfoAddViewModels = _mapper.Map<List<PlanInformationViewModel>>(topics);
                planInfoAddViewModels.AddRange(itemPlanInfoAddViewModels);
            }
            planInfoAddViewModels = planInfoAddViewModels
                                            .Select(x => { x.ClassId = classId; return x; })
                                            .ToList();
            var planInfoAfterSetDate = SetDateForPlanInfomation(planInfoAddViewModels,
                                                                startDate);
            var planInformations = _mapper.Map<List<PlanInformation>>(planInfoAfterSetDate);
            await _unitOfWork.PlanInformationRepository.AddRangeAsync(planInformations);
            var rowsAffect = await _unitOfWork.SaveChangeAsync();
            if (rowsAffect != planInformations.Count)
            {
                throw new Exception("SaveChange Failed!");
            }
            return planInfoAddViewModels;
        }
        private List<PlanInformationViewModel> SetDateForPlanInfomation(
            List<PlanInformationViewModel> planInfoViewModels, DateTime startDate)
        {
            for (int j = 0; j < planInfoViewModels.Count; j++)
            {
                //add 2 day if StartDate == Saturday and 1 day StartDate == Sunday
                if (startDate.DayOfWeek == DayOfWeek.Saturday)
                {
                    startDate = startDate.AddDays(2);
                }
                if (startDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    startDate = startDate.AddDays(1);
                }
                //Set StartDate For list
                planInfoViewModels[j].StartDate = startDate;
                var endDate = startDate.AddDays(
                                Convert.ToDouble(planInfoViewModels[j].Duration - 1));
                //Skip day if is weekend
                for (var date = planInfoViewModels[j].StartDate;
                    date <= endDate; date = date.Value.AddDays(1))
                {
                    if ((date.Value.DayOfWeek == DayOfWeek.Saturday) ||
                        (date.Value.DayOfWeek == DayOfWeek.Sunday))
                    {
                        endDate = endDate.AddDays(1);
                    }
                }
                //Set EndDate For List
                planInfoViewModels[j].EndDate = endDate;
                //Set StartDate For Next Topic = EndDate Old Topic + 1 day
                startDate = endDate.AddDays(1);
            };
            return planInfoViewModels;
        }
        public async Task<PlanAddViewModel> AddItemPlanAsync(
                                        PlanAddViewModel planAddViewModel)
        {
            var plan = _mapper.Map<Plan>(planAddViewModel);
            await _unitOfWork.PlanRepository.AddAsync(plan);
            var rowsAffect = await _unitOfWork.SaveChangeAsync() > 0;
            if (!rowsAffect)
            {
                throw new Exception("Not SaveChange!");
            }
            return planAddViewModel;
        }
        public async Task<Pagination<PlanGetViewModel>> GetAllAsync(int pageIndex, int pageSize)
        {
            var plans = await _unitOfWork.PlanRepository.FindAsync(null, null, pageIndex, pageSize);
            var planViewModels = _mapper.Map<Pagination<PlanGetViewModel>>(plans);
            return planViewModels;
        }
        public async Task<PlanGetByIdViewModel> GetPlanByIdAsync(Guid id)
        {
            var plan = await _unitOfWork.PlanRepository
                                        .GetByIdAsync(
                                                id, x => x.Modules);
            if (plan == null)
            {
                throw new Exception("Bad Request!");
            }
            var planGetByIDModel = _mapper.Map<PlanGetByIdViewModel>(plan);
            return planGetByIDModel;
        }
        public async Task<PlanUpdateViewModel> UpdatePlanAsync(Guid id,
                                        PlanUpdateViewModel planUpdateViewModel)
        {
            //Check item is exist
            var resultPlan = await _unitOfWork.PlanRepository.GetByIdAsync(id);
            if (resultPlan == null)
            {
                return null;
            }
            resultPlan = _mapper.Map(planUpdateViewModel, resultPlan);
            //update item planEntity
            _unitOfWork.PlanRepository.Update(resultPlan);
            var rowsAffected = await _unitOfWork.SaveChangeAsync() > 0;
            if (!rowsAffected)
            {
                throw new Exception("SaveChange Failed!");
            }
            return planUpdateViewModel;
        }
        public async Task SeedDataPlans()
        {
            var data = DataInnitializer.SeedData<Plan>("Plans");
            _unitOfWork.PlanRepository.SeedData(data);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
