﻿using Application.Interfaces;
using AutoMapper;
using Domain.Enums;
using Global.Shared.Helpers;
using Global.Shared.Settings;
using Global.Shared.Settings.Mail;
using Global.Shared.ViewModels.MailViewModels;

namespace Application.Services
{
    public class MailService : IMailService
    {
        private readonly IClaimsService _claimsService; // for checking malicious user.
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        private readonly SmtpClientSetting _smtpClientSetting;
        private readonly UserMailCredential _userMailCredential;
        private readonly IMailTemplateManager _mailTemplateManager;

        public MailService(
            IUnitOfWork unitOfWork,
            IClaimsService claimsService,
            IMapper mapper,
            RootSetting rootSetting,
            IMailTemplateManager mailTemplateManager,
            IUserMailCredentialService userMailCredentialService)
        {
            _claimsService = claimsService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            if (rootSetting.SmtpClientSetting is null)
                throw new ArgumentNullException("Settings not set.");

            _smtpClientSetting = rootSetting.SmtpClientSetting;
            _userMailCredential = userMailCredentialService.Credential;
            _mailTemplateManager = mailTemplateManager;

            //var userId = _claimsService.GetCurrentUserId;
            //var userEmail = _unitOfWork.UserRepository.GetByIdAsync(userId).Result!.Email;
        }

        public async Task<MailViewModel> SendAsync(MailRequestViewModel request)
        {
            // Below statements for cheking malicious user.
            //var userId = _claimsService.GetCurrentUserId;
            //var userEmail = _unitOfWork.UserRepository.GetByIdAsync(userId).Result!.Email;

            request.ToAddresses = "mock4ccount2@outlook.com";

            var template = GetMailTemplate(request);
            var body = BuildBody(template);

            var mailSetting = new MailSetting(_userMailCredential.Address!, request, body);
            var mail = MailHelper.CreateMail(mailSetting);

            var response = await mail.SendAsync(_userMailCredential, _smtpClientSetting);

            var viewModel = _mapper.Map<MailViewModel>(response);

            return viewModel;

        }

        private MailTemplateResult GetMailTemplate(MailRequestViewModel request)
        {
            var templatePath = GetTemplatePath();

            var basePath = Path.Combine(templatePath, _mailTemplateManager.Filenames["Base"]);
            var stylePath = Path.Combine(templatePath, _mailTemplateManager.Filenames["Styles"]);

            var signaturePath = Path.Combine(templatePath,
                _mailTemplateManager.Filenames[request.IsSignatureLong ? "SignatureLong" : "SignatureLong"]);

            var bodyPath = Path.Combine(templatePath, _mailTemplateManager.Filenames[request.MailType!]);

            var result = new MailTemplateResult
            {
                Base = File.ReadAllText(basePath),
                Styles = File.ReadAllText(stylePath),
                Body = File.ReadAllText(bodyPath),
                Signature = File.ReadAllText(signaturePath)
            };

            return result;
        }

        private string BuildBody(MailTemplateResult result)
        {
            return result
                        .Base
                        .Replace("[$$var(style)]", result.Styles)
                        .Replace("[$$var(main)]", result.Body)
                        .Replace("[$$var(signature)]", result.Signature);
        }

        private string GetTemplatePath()
        {
            var currentDirectory = new DirectoryInfo(Directory.GetCurrentDirectory());
            var parentDirectory = currentDirectory?.Parent;
            if (parentDirectory is null || !parentDirectory.Exists)
                throw new InvalidOperationException("Path doesn't exist.");

            var destinationPath = Path.Combine(parentDirectory!.FullName, "Global.Shared", "Templates", "Mail");

            return destinationPath;
        }
    }
}