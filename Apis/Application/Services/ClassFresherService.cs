﻿using Application.Interfaces;
using Application.SeedData;
using AutoMapper;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.Exeption;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Http;

namespace Application.Services
{
    public class ClassFresherService : IClassFresherService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IImportDataService _importDataService;

        public ClassFresherService(IUnitOfWork unitOfWork, IMapper mapper, IImportDataService importDataService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _importDataService = importDataService;
        }

        /// <summary>
        /// update information of class after import file excel
        /// </summary>
        /// <param name="createClassFresherViewModel"></param>
        /// <returns></returns>
        /// <exception cref="AppException"></exception>
        public async Task<ClassFresherViewModel> CreateClassFresherAsync(
                                                CreateClassFresherViewModel createClassFresherViewModel)
        {
            var classFresher = _mapper.Map<ClassFresher>(createClassFresherViewModel);
            var listFresher = _mapper.Map<List<Fresher>>(createClassFresherViewModel.Freshers);

            classFresher.Freshers.Clear();

            const string domainFresher = Constant.DOMAIN_EMAIL_FSOFT;

            List<Fresher> freshersAddToClass = new List<Fresher>();
            // for each list fresher and change class code, email fresher
            foreach (var fresher in listFresher)
            {
                var fresherUpdate = await _unitOfWork.FresherRepository.GetByIdAsync(fresher.Id);
                if (fresherUpdate == null)
                    throw new AppException(Constant.EXCEPTION_NOT_FOUND_FRESHER);
                fresherUpdate.Email = fresher.AccountName + domainFresher;
                fresherUpdate.ClassFresherId = classFresher.Id;
                _unitOfWork.FresherRepository.Update(fresherUpdate);
                freshersAddToClass.Add(fresherUpdate);
            };

            classFresher.Freshers = freshersAddToClass;
            _unitOfWork.ClassFresherRepository.Update(classFresher);
            // check save is success
            var isSuccess = await _unitOfWork.SaveChangeAsync() == 0;
            if (isSuccess)
                throw new AppException(Constant.EXCEPTION_CREATE_CLASS);
            return _mapper.Map<ClassFresherViewModel>(classFresher);
        }

        private async Task ImportClassFreshersAndFreshersAtFirstTime(List<ClassFresher> listClassFresher, List<Fresher> listFresher)
        {
            foreach (var classFresher in listClassFresher)
            {
                await ImportClassFresherAndFreshersIfNotExisted(classFresher,listFresher);
            }
        }

        private async Task ImportClassFresherAndFreshersIfNotExisted(ClassFresher classFresher, List<Fresher> listFresher)
        {
            if (classFresher.RRCode != null)
            {
                var checkClassFresherIfExisted = await _unitOfWork.ClassFresherRepository.CheckExistedClassAsync(classFresher.RRCode);
                if (checkClassFresherIfExisted)
                {
                    var classFresherExisted = await _unitOfWork.ClassFresherRepository.GetClassFresherByRRCodeAsync(classFresher.RRCode);
                    classFresher.Id = classFresherExisted.Id;
                    var listFresherWithRRCode = listFresher.Where(p => p.RRCode.Equals(classFresher.RRCode));
                    foreach (var fresher in listFresherWithRRCode)
                    {
                        if (await _unitOfWork.FresherRepository.CheckExistedFresherByAccountNameAsync(fresher.AccountName))
                        {
                            var fresherUpdateInfo = await _unitOfWork.FresherRepository.
                                GetFresherByAccountNameAndRRCodeAsync(fresher.AccountName, classFresher.RRCode);
                            fresherUpdateInfo = _mapper.Map(fresher, fresherUpdateInfo);
                            fresher.Id = fresherUpdateInfo.Id;
                            fresher.ClassFresherId = fresherUpdateInfo.ClassFresherId;

                            _unitOfWork.FresherRepository.Update(fresherUpdateInfo);
                        }
                        else
                        {
                            var classfresher = await _unitOfWork.ClassFresherRepository.GetClassFresherByRRCodeAsync(classFresher.RRCode);
                            fresher.ClassFresherId = classfresher.Id;
                            await _unitOfWork.FresherRepository.AddAsync(fresher);
                        }
                    }
                }
                else
                {
                    await _unitOfWork.ClassFresherRepository.AddAsync(classFresher);
                    classFresher.Id = Guid.NewGuid();
                    var listFresherRR = listFresher.Where(p => p.RRCode.Equals(classFresher.RRCode));
                    foreach (var fresher in listFresherRR)
                    {
                        fresher.ClassFresherId = classFresher.Id;
                        await _unitOfWork.FresherRepository.AddAsync(fresher);
                    }
                }
            }
        }

        private List<ClassFresherViewModel> GetResultOfClassFresherAfterImport(List<ClassFresher> listClassFresher, List<Fresher> listFresher)
        {
            List<ClassFresherViewModel> resultClassFresher = new List<ClassFresherViewModel>();

            var listClassFresherAfterImport = _mapper.Map<List<ClassFresherViewModel>>(listClassFresher);
            var listFresherAfterImport = _mapper.Map<List<FresherViewModel>>(listFresher);

            foreach (var classFresher in listClassFresherAfterImport)
            {
                List<FresherViewModel> fresherViewModel = new List<FresherViewModel>();
                foreach (var fresherImport in listFresherAfterImport)
                {
                    if (fresherImport.ClassCode.Equals(classFresher.ClassCode))
                    {
                        fresherViewModel.Add(fresherImport);
                    }
                }
                classFresher.Freshers = fresherViewModel;
                resultClassFresher.Add(classFresher);
            }
            return resultClassFresher;
        }

        /// <summary>
        /// create class and fresher when import from fiel excel
        /// </summary>
        /// <param name="fileExcel"></param>
        /// <returns></returns>
        /// <exception cref="AppException"></exception>
        public async Task<List<ClassFresherViewModel>> CreateCLassFresherFromImportedExcelFile(IFormFile fileExcel)
        {
            var getPackageReponse = await _importDataService.GetDataFromFileExcelAsync(fileExcel);
            if (getPackageReponse == null)
                throw new AppException(Constant.IMPORT_FAIL);
            //luu classFresher
            var listClassFresherViewModel = _mapper.Map<List<ClassFresherViewModel>>(getPackageReponse.ListclassImpVM);
            var listClassFresher = _mapper.Map<List<ClassFresher>>(listClassFresherViewModel);
            // luu fresher
            var listFresherViewModel = _mapper.Map<List<FresherViewModel>>(getPackageReponse.ListfresherImpVM);
            var listFresher = _mapper.Map<List<Fresher>>(listFresherViewModel);
            await ImportClassFreshersAndFreshersAtFirstTime(listClassFresher, listFresher);

            var isSuccess = await _unitOfWork.SaveChangeAsync() == 0;
            if (isSuccess) throw new AppException(Constant.IMPORT_FAIL);

            return GetResultOfClassFresherAfterImport(listClassFresher,listFresher);
        }

        public async Task<Pagination<ClassFresherViewModel>> GetAllClassFreshersPagingsionAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listClass = await _unitOfWork.ClassFresherRepository.ToPagination(pageIndex, pageSize);
            // check if not found class fresher
            if (listClass == null)
                throw new AppException(Constant.EXCEPTION_CLASS_NOT_FOUND);
            return _mapper.Map<Pagination<ClassFresherViewModel>>(listClass);
        }

        public async Task<ClassFresherViewModel> GetClassFresherByIdAsync(Guid id)
        {
            var classFresher = await _unitOfWork.ClassFresherRepository.GetByIdAsync(id);
            if (classFresher == null)
                throw new AppException(Constant.EXCEPTION_CLASS_NOT_FOUND);
            return _mapper.Map<ClassFresherViewModel>(classFresher);
        }

        public async Task<ClassFresherViewModel> GetClassHasFresherByIdAsync(Guid id)
        {
            var classFresher = await _unitOfWork.ClassFresherRepository.GetClassHasFresherAsync(id);
            if (classFresher == null)
                throw new AppException(Constant.EXCEPTION_CLASS_NOT_FOUND);
            return _mapper.Map<ClassFresherViewModel>(classFresher);
        }

        public async Task<ClassFresherViewModel> UpdateClassFresher(ClassFresherViewModel classFresherViewModel)
        {
            var classFresher = _mapper.Map<ClassFresher>(classFresherViewModel);
            _unitOfWork.ClassFresherRepository.Update(classFresher);

            var isSuccess = await _unitOfWork.SaveChangeAsync() == 0;
            // check save is success   
            if (isSuccess)
                throw new AppException(Constant.EXCEPTION_UPDATE_CLASS);
            return _mapper.Map<ClassFresherViewModel>(classFresher);
        }

        public async Task<List<FresherViewModel>> GetFreshersByClassCodeAsync(string classCode)
        {
            var listFresher = await _unitOfWork.FresherRepository.GetFresherByClassCodeAsync(classCode);
            if (listFresher == null)
                throw new AppException(Constant.EXCEPTION_LIST_FRESHER_NOT_FOUND);
            return _mapper.Map<List<FresherViewModel>>(listFresher);
        }

        public async Task SeedData()
        {
            var data = DataInnitializer.SeedData<ClassFresher>("ClassFresher");
            _unitOfWork.ClassFresherRepository.SeedDataClassFresher(data);
            await _unitOfWork.SaveChangeAsync();
        }

        public async Task<List<string>> GetAllClassCodeAsync()
        {
            return await _unitOfWork.ClassFresherRepository.GetAllClassCodeAsync();
        }
    }
}
