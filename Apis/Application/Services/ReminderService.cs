﻿using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using Global.Shared.Helpers;
using Global.Shared.Settings;
using Global.Shared.Settings.Mail;
using Global.Shared.Settings.Reminder;
using Global.Shared.ViewModels.MailViewModels;
using Global.Shared.ViewModels.ReminderViewModels;

namespace Application.Services
{
    public class ReminderService : IReminderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly RootSetting _rootSetting;
        private readonly ReminderSettings _reminderSettings;

        public ReminderService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            RootSetting rootSetting,
            ReminderSettings reminderSettings)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _rootSetting = rootSetting;
            _reminderSettings = reminderSettings;
        }

        public async Task<bool> CreateReminderAsync(CreateReminderViewModel reminderViewModel)
        {
            var reminder = _mapper.Map<Reminder>(reminderViewModel);
            if (reminderViewModel.ReminderType == ReminderTypeEnum.Audit)
            {
                reminder.RemindTime1 = reminderViewModel.EventTime.AddDays(_reminderSettings.AuditReminderTime.TheFirstTime);
                reminder.RemindTime2 = reminderViewModel.EventTime.AddDays(_reminderSettings.AuditReminderTime.TheSecondTime);
                await _unitOfWork.ReminderRepository.AddAsync(reminder);
            }
            if (reminderViewModel.ReminderType == ReminderTypeEnum.ContractTransfer)
            {
                reminder.RemindTime1 = reminderViewModel.EventTime.AddDays(_reminderSettings.ContractTransferReminderTime);
                reminder.RemindTime2 = null ;
                await _unitOfWork.ReminderRepository.AddAsync(reminder);
            }
            var affectedRows = await _unitOfWork.SaveChangeAsync();
            return affectedRows >= 1;
        }

        public async Task<IList<ReminderViewModel>> GetAllReminderByDateAsync(DateTime date)
        {
            var allReminders = await _unitOfWork.ReminderRepository.GetAllReminderByDateAsync(date);
            return _mapper.Map<IList<ReminderViewModel>>(allReminders);
        }

        public async Task SendReminderMailAsync (DateTime date)
        {
           var reminders = await GetAllReminderByDateAsync(date);
            foreach(var reminder in reminders)
            {
                //to
                var reminderTo = reminder.ReminderEmail;
                //from
                var reminderFrom = "thepluralman1st@outlook.com";
                //body 
                var reminderMailBody = reminder.Description;
                //
                var mailRequest = new MailRequestViewModel
                {
                    ToAddresses = reminderTo
                };
                var mailSetting = new MailSetting(reminderFrom, mailRequest, reminderMailBody);

                var mail = MailHelper.CreateMail(mailSetting);

                var userCredential = new UserMailCredential
                {
                    Address = reminderFrom,
                    SecureString = "thepluralman1"
                };

                var smtp = _rootSetting.SmtpClientSetting;
               await  mail.SendAsync(userCredential , smtp!);
            }
        }
    }
}
