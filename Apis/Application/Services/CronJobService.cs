﻿using Application.Interfaces;

namespace Application.Services
{
    public class CronJobService : ICronJobService
    {
        private readonly IChemicalService _chemicalService;

        public CronJobService(IChemicalService chemicalService)
        {
            _chemicalService = chemicalService;
        }

        public async Task AutoGetChemicalAsync()
        {
            await _chemicalService.GetChemicalAsync();
        }

        public async Task AutoGetChemicalPagingsionAsync()
        {
            await _chemicalService.GetChemicalPagingsionAsync();
        }
    }
}
