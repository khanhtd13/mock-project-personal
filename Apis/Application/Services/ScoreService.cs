﻿using Application.Interfaces;
using AutoMapper;
using Domain.Entities;
using Global.Shared.ViewModels.ScoreViewModels;

namespace Application.Services
{
    public class ScoreService : IScoreService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ScoreService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ScoreViewModel?> CreateScoreAsync(CreateScoreViewModel
                                                                          createScoreViewModel)
        {
            var score = _mapper.Map<Score>(createScoreViewModel);
            await _unitOfWork.ScoreRepository.AddAsync(score);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return _mapper.Map<ScoreViewModel>(score);
            }
            return null;
        }

        public async Task DeleteScoreAsync(Guid id)
        {
            var score = await _unitOfWork.ScoreRepository.GetByIdAsync(id);
            if (score != null) _unitOfWork.ScoreRepository
                                                     .SoftRemove(score);

            await _unitOfWork.SaveChangeAsync();
        }

        public async Task<List<ScoreViewModel>?> GetAllScoreAsync()
        {
            var scores = await _unitOfWork.ScoreRepository.GetAllDeleteFalseAsync();
            var scoreViewModel = _mapper.Map<List<ScoreViewModel>>(scores);

            return scoreViewModel;
        }

        public async Task<ScoreViewModel?> UpdateScoreAsync(UpdateScoreViewModel
                                                                          updateScoreViewModel)
        {
            var score = await _unitOfWork.ScoreRepository
                                                .GetByIdAsync(updateScoreViewModel.Id);
            if (score == null) throw new Exception($"Not Found Score with Id:{updateScoreViewModel.Id}!!");

            var scoreMap = _mapper.Map(updateScoreViewModel, score);

            _unitOfWork.ScoreRepository.Update(scoreMap);

            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess) return _mapper.Map<ScoreViewModel>(scoreMap);
            return null;
        }
    }
}
