﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface ITopicRepository:IGenericRepository<Topic>
    {
        Task<List<Topic>> GetByModuleId(Guid moduleId);
        void SeedData(ICollection<Topic> data);
    }
}
