﻿using Domain.Entities;
using Global.Shared.ViewModels.PlanInfomationViewModels;

namespace Application.Repositories
{
    public interface IPlanInformationRepository : IGenericRepository<PlanInformation>
    {
        Task<List<PlanInformation>> GetByClassIdAsync(Guid classId);
    }
}
