﻿using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.FeedbackViewModels;

namespace Application.Repositories
{
    public interface IFeedbackRepository : IGenericRepository<FeedBack>
    {
        public Task<FeedBack?> GetFeedbackWithQuestionAndResultById(Guid id);
        public new Task<IList<FeedBack>> GetAllAsync();
        public new Task<Pagination<FeedBack>> SearchAsync(SearchFeedbackViewModel searchFeedback);
    }
}
