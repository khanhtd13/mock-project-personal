﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IClassFresherRepository  : IGenericRepository<ClassFresher>
    {
        /// <summary>
        /// Seacrh class fresher by key word
        /// </summary>
        /// <param name="keyword">Keyword: location, class code, class name</param>
        /// <returns>A list class fresher</returns>   
        Task<bool> CheckExistedClassAsync(string rrCode);
        Task<ClassFresher> GetClassFresherByRRCodeAsync(string rrCode);
        Task<ClassFresher> GetClassHasFresherAsync(Guid id);
        Task<List<string>> GetAllClassCodeAsync();
        void SeedDataClassFresher(ICollection<ClassFresher> data);
        Task<ClassFresher?> GetClassFresherByClassCodeAsync(string classCode);
    }
}
    