﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IReminderRepository:IGenericRepository<Reminder>
    {
        Task<IList<Reminder>> GetAllReminderByDateAsync(DateTime date);
        
        
    }
}
