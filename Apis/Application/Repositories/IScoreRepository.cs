﻿using Domain.Entities;
using Domain.Enums;

namespace Application.Repositories
{
    public interface IScoreRepository : IGenericRepository<Score>
    {
        Task<IList<Score>> GetTypeScore(TypeScoreEnum typeScoreEnum, Guid fresherId, Guid moduleId);
        Task<IList<Score>> GetAllDeleteFalseAsync();
    }
}
