﻿using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.FeedbackViewModels;

namespace Application.Repositories
{
    public interface IFeedbackQuestionRepository : IGenericRepository<FeedBackQuestion>
    {
        public Task<Pagination<FeedBackQuestion>> SearchAsync(SearchFeedbackQuestionViewModel searchFeedback);

    }
}
