﻿using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.FeedbackViewModels;

namespace Application.Repositories
{
    public interface IFeedbackResultRepository : IGenericRepository<FeedBackResult>
    {
        public Task<IList<FeedBackResult>> GetAllResultOfFeedbackAsync(Guid feedbackId);
        public new Task<Pagination<FeedBackResult>> SearchAsync(SearchFeedbackResultViewModel searchFeedback);

    }
}
