﻿using Domain.Entities;
using Domain.Enums;

namespace Application.Repositories
{
    public interface IFresherRepository : IGenericRepository<Fresher>
    {
        Task<List<Fresher>> GetFresherByClassCodeAsync(string classCode);
        Task<Fresher?> GetFresherByAccountNameAndRRCodeAsync(string accountName, string rrCode);
        Task<bool> CheckExistedFresherByAccountNameAsync(string accountName);
        void SeedData(ICollection<Fresher> data);
    }
}
