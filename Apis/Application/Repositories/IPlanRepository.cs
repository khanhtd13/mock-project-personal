﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IPlanRepository : IGenericRepository<Plan>
    {
        void SeedData(ICollection<Plan> data);

    }
}
