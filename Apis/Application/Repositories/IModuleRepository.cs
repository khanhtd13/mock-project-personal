﻿using Domain.Entities;

namespace Application.Repositories
{
    public interface IModuleRepository:IGenericRepository<Module>
    {
        Task<List<Module>> GetModuleByPlanId(Guid planId);
        void SeedData(ICollection<Module> data);
    }
}
