﻿using Application.Repositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public IChemicalRepository ChemicalRepository { get; }
        public IClassFresherRepository ClassFresherRepository { get; }
        public IFresherRepository FresherRepository { get; }
        public IFresherReportRepository FresherReportRepository { get; }
        public IFeedbackRepository FeedbackRepository { get; }
        public IFeedbackQuestionRepository FeedbackQuestionRepository { get; }
        public IFeedbackResultRepository FeedbackResultRepository { get; }

        public IReminderRepository ReminderRepository { get; }

        public IScoreRepository ScoreRepository { get; }
        public IPlanRepository PlanRepository { get; }
        public ITopicRepository TopicRepository { get; }
        public IModuleRepository ModuleRepository { get; }
        public IPlanInformationRepository PlanInformationRepository { get; }
        public Task<int> SaveChangeAsync();
        void BeginTransaction();
        void Commit();

    }
}
