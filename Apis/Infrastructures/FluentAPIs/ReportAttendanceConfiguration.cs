﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ReportAttendanceConfiguration : IEntityTypeConfiguration<ReportAttendance>
    {
        public void Configure(EntityTypeBuilder<ReportAttendance> builder)
        {
            builder.ToTable("Report.Attendance");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.FresherId).IsRequired();
        }
    }
}
