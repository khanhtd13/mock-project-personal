﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class AttendanceConfiguration : IEntityTypeConfiguration<Attendance>
    {
        public void Configure(EntityTypeBuilder<Attendance> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.FresherId).IsRequired();

            builder.Property(x => x.Status).IsRequired();

            builder.Property(x => x.Note).HasMaxLength(200);
        }
    }
}
