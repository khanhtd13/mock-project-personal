﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class PlanConfiguration : IEntityTypeConfiguration<Plan>
    {
        public void Configure(EntityTypeBuilder<Plan> builder)
        {
            builder.ToTable("PlanConfiguration.Plan");
            builder.HasKey(p => p.Id);
            builder.Property(x => x.CourseName).IsRequired(true)
                                                .HasMaxLength(100);
            builder.HasIndex(x => x.CourseName).IsUnique(true);
            builder.Property(x => x.CourseCode).IsRequired(true)
                                               .HasMaxLength(50);
            builder.HasIndex(x => x.CourseCode).IsUnique();
        }
    }
}
