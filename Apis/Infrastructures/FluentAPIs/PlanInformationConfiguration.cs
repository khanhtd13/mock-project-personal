﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class PlanInformationConfiguration : IEntityTypeConfiguration<PlanInformation>
    {
        public void Configure(EntityTypeBuilder<PlanInformation> builder)
        {
            builder.ToTable("PlanInformation");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ModuleName).IsRequired(true)
                                                .HasMaxLength(100);
            builder.Property(x => x.TopicName).IsRequired(true)
                                                .HasMaxLength(100);
        }
    }
}
