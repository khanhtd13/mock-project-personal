﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ResultModuleConfiguration : IEntityTypeConfiguration<ResultModule>
    {
        public void Configure(EntityTypeBuilder<ResultModule> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
