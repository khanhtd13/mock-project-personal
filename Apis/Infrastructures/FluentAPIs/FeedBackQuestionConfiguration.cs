﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class FeedBackQuestionConfiguration : IEntityTypeConfiguration<FeedBackQuestion>
    {
        public void Configure(EntityTypeBuilder<FeedBackQuestion> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasQueryFilter(x => !x.IsDeleted);

            builder.Property(x => x.Title).HasMaxLength(128);
            builder.Property(x => x.Content).HasMaxLength(256);
            builder.Property(x => x.Description).HasMaxLength(128).IsRequired(false);
            builder.HasOne(x => x.FeedBack).WithMany(y => y.FeedBackQuestions).HasForeignKey(x => x.FeedBackId).IsRequired(false);
        }
    }
}
