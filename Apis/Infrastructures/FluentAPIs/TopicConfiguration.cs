﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace Infrastructures.FluentAPIs
{
    public class TopicConfiguration : IEntityTypeConfiguration<Topic>
    {
        public void Configure(EntityTypeBuilder<Topic> builder)
        {
            builder.ToTable("PlanConfiguration.Topic");
            builder.HasKey(t => t.Id);
            builder.Property(x => x.Name).IsRequired(true)
                                         .HasMaxLength(100);
            builder.HasIndex(x => x.Name);
            builder.HasOne<Module>(x => x.Module)
                   .WithMany(x => x.Topics)
                   .HasForeignKey(x => x.ModuleId);
        }
    }
}
