﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ResultMonthConfiguration : IEntityTypeConfiguration<ResultMonth>
    {
        public void Configure(EntityTypeBuilder<ResultMonth> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}
