﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class ModuleConfiguration : IEntityTypeConfiguration<Module>
    {
        public void Configure(EntityTypeBuilder<Module> builder)
        {
            builder.ToTable("PlanConfiguration.Module");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.ModuleName).IsRequired(true)
                                                .HasMaxLength(100);
            builder.Property(x => x.WeightedNumberQuizz).IsRequired(true);
            builder.Property(x => x.WeightedNumberAssignment).IsRequired(true);
            builder.Property(x => x.WeightedNumberFinal).IsRequired(true);
            builder.Property(x => x.WeightedNumberQuizz).IsRequired(true);
            builder.HasIndex(x => x.ModuleName).IsUnique(true);
            builder.HasOne<Plan>(x => x.Plan)
                   .WithMany(x => x.Modules)
                   .HasForeignKey(x => x.PlanId);
        }
    }
}
