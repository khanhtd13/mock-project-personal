﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.FluentAPIs
{
    public class FeedBackResultConfiguration : IEntityTypeConfiguration<FeedBackResult>
    {
        public void Configure(EntityTypeBuilder<FeedBackResult> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasQueryFilter(x => !x.IsDeleted);

            builder.Property(x => x.Fullname).HasMaxLength(60);
            builder.Property(x => x.Content).HasMaxLength(256);
            builder.Property(x => x.Note).HasMaxLength(128).IsRequired(false);
            builder.HasOne(x => x.FeedBack).WithMany(y => y.FeedBackResults).HasForeignKey(x => x.FeedBackId);
            builder.HasOne(x => x.FeedBackQuestion).WithMany(y => y.FeedBackResults).HasForeignKey(x => x.QuestionId);

        }
    }
}
