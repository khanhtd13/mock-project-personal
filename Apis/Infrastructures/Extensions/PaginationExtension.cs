﻿using Microsoft.EntityFrameworkCore;

namespace Global.Shared.Commons
{
    public static class PaginationExtension
    {
        public static async Task<Pagination<TEntity>> PaginateAsync<TEntity>(
            this IQueryable<TEntity> query, int pageIndex, int pageSize) where TEntity : class
        {
            if (pageIndex < 0 || pageSize <= 0)
                throw new ArgumentException();
            if (query == null)
                throw new ArgumentNullException(nameof(query));

            var totalItemsCountTask = query.CountAsync();
            var itemsTask = query.Skip(pageIndex * pageSize).Take(pageSize).AsNoTracking().ToListAsync();

            await Task.WhenAll(totalItemsCountTask, itemsTask);

            var result = new Pagination<TEntity>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = await totalItemsCountTask,
                Items = await itemsTask,
            };

            return result;
        }
    }
}
