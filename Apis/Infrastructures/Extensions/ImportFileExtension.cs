﻿using Ganss.Excel;
using Global.Shared.ViewModels.ImportViewModels;
using Microsoft.AspNetCore.Http;

namespace Infrastructures.Extensions
{
    public static class ImportFileExtension
    {
        public static List<string> GetListExcelFileInputTypeSupport()
        {
            List<string> listExcelSupported = new List<string>()
            {
                ".xlsx",
                ".xlsm",
                ".xlsb",
                ".xltx",
                ".xltm",
                ".xls",
                ".xlt",
                ".xls",
                ".xla",
                ".xlw",
                ".xlr",
                ".xlam"
            };
            return listExcelSupported;
        }
        public static bool CheckImportRECFileTypeInput(this IFormFile? fileExcel)
        {
            if (!CheckImportRECFileNull(fileExcel))
            {
                var ListExcelFileInputSupport = GetListExcelFileInputTypeSupport();
                foreach (var inputtype in ListExcelFileInputSupport)
                {
                    if (fileExcel.FileName.EndsWith(inputtype))
                    {
                        return true;
                    }
                }
                return false;
            }
            return false;
        }

        public static bool CheckImportRECFileNull(IFormFile? fileExcel)
        {
            if (fileExcel == null)
            {
                return true;
            }
            return false;
        }

        public static IEnumerable<AccountImportViewModel>? GetAccountFromImportFile(this ExcelMapper instanceMapper)
        {
            var listAccount_ImportVM = instanceMapper.Fetch<AccountImportViewModel>();
            if (listAccount_ImportVM.Any()) return listAccount_ImportVM;
            return null;
        }

        public static IEnumerable<ClassImportViewModel>? GetClassFromImportFile(this ExcelMapper instanceMapper)
        {
            var listClass_ImportVM = instanceMapper.Fetch<ClassImportViewModel>().
                    DistinctBy(p => p.RRCode);
            if (listClass_ImportVM.Any()) return listClass_ImportVM;
            return null;
        }


        public static IEnumerable<FresherImportViewModel>? GetFresherFromImportFile(this ExcelMapper instanceMapper)
        {
            var listFresher_ImportVM = instanceMapper.Fetch<FresherImportViewModel>();
            if (listFresher_ImportVM.Any()) return listFresher_ImportVM;
            return null;
        }
    }
}
