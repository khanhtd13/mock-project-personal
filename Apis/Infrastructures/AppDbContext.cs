﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Chemical> Chemicals { get; set; }
        public DbSet<Reminder> Reminders { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<PlanInformation> PlanInformations { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ClassFresher> ClassFreshers { get; set; }
        public DbSet<Fresher> Freshers { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<ReportAttendance> ReportAttendances { get; set; }
        public DbSet<Audit> Audits { get; set; }
        public DbSet<FresherReport> FresherReports { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<ResultModule> ResultModules { get; set; }
        public DbSet<ResultMonth> ResultMonths { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reminder>().HasIndex(e => new { e.RemindTime1, e.RemindTime2 });
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<FeedBack> FeedBacks { get; set; }
        public DbSet<FeedBackQuestion> FeedBackQuestions { get; set; }
        public DbSet<FeedBackResult> FeedBackResults { get; set; }
        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}
