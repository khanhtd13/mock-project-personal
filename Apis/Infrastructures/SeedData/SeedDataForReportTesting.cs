using Application.Interfaces;
using Application.Services;
using Domain.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Infrastructures.SeedData
{
    public static class SeedDataForReportTesting
    {
        public static void SeedDataInit(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.CreateScope();
            var context = serviceScope.ServiceProvider.GetService<AppDbContext>()!;
            SeedReportData(context);
        }

        public static async void SeedReportData(AppDbContext context)
        {
            var dataExists = context.FresherReports.Any();
            if (!dataExists)
            {
                var reportList = JsonConvert
                                    .DeserializeObject<List<FresherReport>>(
                                        File.ReadAllText("SeedDataJson/FresherReportsSeedData.json"));

                var fresherList = JsonConvert
                                    .DeserializeObject<List<Fresher>>(
                                        File.ReadAllText("SeedDataJson/FreshersSeedData.json"));

                context.FresherReports.AddRange(reportList);
                context.Freshers.AddRange(fresherList);

                await context.SaveChangesAsync();
            }
        }
    }
}
