﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ClassFresherRepository : GenericRepository<ClassFresher>, IClassFresherRepository
    {
        private readonly ICurrentTime _curentTime;
        private readonly IClaimsService _claimService;
        public ClassFresherRepository(AppDbContext DbContext,
            ICurrentTime currentTime,
            IClaimsService claimsService) :
            base(DbContext,
                currentTime,
                claimsService)
        {
            _curentTime = currentTime;
            _claimService = claimsService;
        }

        public async Task<ClassFresher> GetClassHasFresherAsync(Guid id)
        {
            return await _dbSet.Include(x => x.Freshers).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<string>> GetAllClassCodeAsync()
        {
            return await _dbSet.Select(x => x.ClassCode).ToListAsync();
        }

        public async Task<bool> CheckExistedClassAsync(string rrCode)
        {
            return await _dbSet.AnyAsync(x => x.RRCode.Equals(rrCode));
        }

        
        public void SeedDataClassFresher(ICollection<ClassFresher> data)
        {
            _dbSet.AddRange(data);
        }

        public async Task<ClassFresher?> GetClassFresherByClassCodeAsync(string classCode)
            => await _dbSet.Include(c => c.Freshers)
                           .FirstOrDefaultAsync(c => c.ClassCode == classCode);

        public Task<ClassFresher?> GetClassFresherByRRCodeAsync(string rrCode)
        {
            return _dbSet.FirstOrDefaultAsync(p => p.RRCode.Equals(rrCode));
        }

    }
}
