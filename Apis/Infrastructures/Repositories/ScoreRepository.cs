﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ScoreRepository : GenericRepository<Score>, IScoreRepository
    {
        public ScoreRepository(AppDbContext dbContext,
                                      ICurrentTime timeService, 
                                      IClaimsService claimsService)
                                    : base(dbContext, timeService, claimsService)
        {

        }

        public async Task<IList<Score>> GetAllDeleteFalseAsync()
        {
            return await _dbSet.Where(e => !e.IsDeleted).ToListAsync();
        }

        public async Task<IList<Score>> GetTypeScore(TypeScoreEnum typeScoreEnum, Guid fresherId, Guid moduleId)
        {
            var scores = await _dbSet.Where(e => !e.IsDeleted)
                                     .Where(e => e.TypeScore == typeScoreEnum)
                                     .Where(e => e.FresherId == fresherId)
                                     .Where(e => e.ModuleId == moduleId).ToListAsync();
            return scores;
        }
    }
}
