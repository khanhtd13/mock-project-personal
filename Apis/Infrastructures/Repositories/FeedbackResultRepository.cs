﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.Helper;
using Global.Shared.ViewModels.FeedbackViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class FeedbackResultRepository : GenericRepository<FeedBackResult>, IFeedbackResultRepository
    {
        public FeedbackResultRepository(
            AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context,
                  timeService,
                  claimsService)
        {
        }

        public async Task<IList<FeedBackResult>> GetAllResultOfFeedbackAsync(Guid feedbackId)
        {
            return await _dbSet.Where(x => x.FeedBackId == feedbackId).ToListAsync();
        }

        public async Task<Pagination<FeedBackResult>> SearchAsync(SearchFeedbackResultViewModel searchFeedback)
        {
            var queryable = _dbSet.AsQueryable();
            Expression<Func<FeedBackResult, bool>> predicate = x => true;
            if (!string.IsNullOrEmpty(searchFeedback.AccountName))
            {
                Expression<Func<FeedBackResult, bool>> subExpression = x => x.AccountName
                                                                        .ToLower()
                                                                        .Contains(
                                                                            searchFeedback.AccountName.ToLower()
                                                                            );
                predicate = ExpressionHelper<FeedBackResult>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (searchFeedback.QuestionId != null && searchFeedback.QuestionId != Guid.Empty)
            {
                Expression<Func<FeedBackResult, bool>> subExpression = x => x.QuestionId == searchFeedback.QuestionId;
                predicate = ExpressionHelper<FeedBackResult>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (searchFeedback.CreationDate != null)
            {
                Expression<Func<FeedBackResult, bool>> subExpression =
                    x => x.CreationDate.Subtract(searchFeedback.CreationDate.Value).Days == 0;
                predicate = ExpressionHelper<FeedBackResult>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            var result = await FindAsync(predicate, pageIndex: searchFeedback.PageIndex, pageSize: searchFeedback.PageSize);

            return result;
        }
    }
}
