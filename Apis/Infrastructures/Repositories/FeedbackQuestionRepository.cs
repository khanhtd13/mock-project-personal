﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.Helper;
using Global.Shared.ViewModels.FeedbackViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class FeedbackQuestionRepository : GenericRepository<FeedBackQuestion>, IFeedbackQuestionRepository
    {
        public FeedbackQuestionRepository(
            AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context,
                  timeService,
                  claimsService)
        {
        }

        public async Task<Pagination<FeedBackQuestion>> SearchAsync(SearchFeedbackQuestionViewModel searchFeedback)
        {
            var queryable = _dbSet.AsQueryable();
            Expression<Func<FeedBackQuestion, bool>> predicate = x => true;
            if (!string.IsNullOrEmpty(searchFeedback.Title))
            {
                Expression<Func<FeedBackQuestion, bool>> subExpression = 
                    x => x.Title.ToLower().Contains(searchFeedback.Title.ToLower());
                predicate = ExpressionHelper<FeedBackQuestion>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (searchFeedback.CreationDate != null)
            {
                Expression<Func<FeedBackQuestion, bool>> subExpression =
                    x => x.CreationDate.Subtract(searchFeedback.CreationDate.Value).Days == 0;
                predicate = ExpressionHelper<FeedBackQuestion>.ExpressionCombineAndAlso(predicate, subExpression);
            }   
            if (searchFeedback.FeedBackId != null && searchFeedback.FeedBackId != Guid.Empty)
            {
                Expression<Func<FeedBackQuestion, bool>> subExpression =
                    x => x.FeedBackId == searchFeedback.FeedBackId;
                predicate = ExpressionHelper<FeedBackQuestion>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            var result = await FindAsync(predicate, pageIndex: searchFeedback.PageIndex, pageSize: searchFeedback.PageSize);
            return result;
        }
    }
}
