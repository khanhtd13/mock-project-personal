﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class FresherRepository : GenericRepository<Fresher>, IFresherRepository
    {
        private readonly ICurrentTime _currentTime;
        private readonly IClaimsService _claimService;
        public FresherRepository(AppDbContext DbContext,
            ICurrentTime currentTime,
            IClaimsService claimsService) :
            base(DbContext,
                currentTime,
                claimsService)
        {
            _currentTime = currentTime;
            _claimService = claimsService;
        }

        public async Task<List<Fresher>> GetAllFresherAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<Fresher?> GetFresherByAccountNameAndRRCodeAsync(string accountName, string rrCode)
        {
            return await _dbSet.FirstOrDefaultAsync
                (x => x.AccountName.ToLower().Equals(accountName.ToLower())
                && x.RRCode.ToLower().Equals(rrCode.ToLower()));
        }

        public async Task<List<Fresher>> GetFresherByClassCodeAsync(string classCode)
        {
            var listFresher = await _dbSet.Where(x => x.ClassCode == classCode).ToListAsync();
            return listFresher;
        }

        public async Task<bool> CheckExistedFresherByAccountNameAsync(string accountName)
        {
            return await _dbSet.AnyAsync(x => x.AccountName.ToLower().Equals(accountName.ToLower()));
        }


        public void SeedData(ICollection<Fresher> data)
        {
            _dbSet.AddRangeAsync(data);
        }
    }
}
