﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ReminderRepository : GenericRepository<Reminder>, IReminderRepository
    {
        public ReminderRepository(
            ICurrentTime timeService,
            IClaimsService claimsService,
            AppDbContext appDbContext)
            : base(appDbContext, timeService, claimsService)
        {
        }

        public async Task<IList<Reminder>> GetAllReminderByDateAsync(DateTime date)
        {
            var remindList = await _dbSet.Where(r => r.RemindTime1 == date || r.RemindTime2 == date)
                                                     .ToListAsync();
            return remindList;
        }
    }
}
