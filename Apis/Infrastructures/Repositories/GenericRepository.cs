﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Global.Shared.Commons;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Infrastructures.Extensions;

namespace Infrastructures.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected DbSet<TEntity> _dbSet;
        private readonly ICurrentTime _timeService;
        private readonly IClaimsService _claimsService;

        public GenericRepository(AppDbContext context, ICurrentTime timeService, IClaimsService claimsService)
        {
            _dbSet = context.Set<TEntity>();
            _timeService = timeService;
            _claimsService = claimsService;
        }
        public async Task<IList<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<TEntity?> GetByIdAsync(Guid id)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task AddAsync(TEntity entity)
        {
            entity.CreationDate = _timeService.GetCurrentTime();
            entity.CreatedBy = _claimsService.GetCurrentUserId;
            await _dbSet.AddAsync(entity);
        }

        public void SoftRemove(TEntity entity)
        {
            entity.IsDeleted = true;
            entity.DeleteBy = _claimsService.GetCurrentUserId;
            _dbSet.Update(entity);
        }

        public void Update(TEntity entity)
        {
            entity.ModificationDate = _timeService.GetCurrentTime();
            entity.ModificationBy = _claimsService.GetCurrentUserId;
            _dbSet.Update(entity);
        }

        public async Task AddRangeAsync(ICollection<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreationDate = _timeService.GetCurrentTime();
                entity.CreatedBy = _claimsService.GetCurrentUserId;
            }
            await _dbSet.AddRangeAsync(entities);
        }

        public void SoftRemoveRange(ICollection<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
                entity.DeletionDate = _timeService.GetCurrentTime();
                entity.DeleteBy = _claimsService.GetCurrentUserId;
            }
            _dbSet.UpdateRange(entities);
        }

        public Task<Pagination<TEntity>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            return _dbSet.PaginateAsync(pageIndex, pageSize);
        }

        public void UpdateRange(ICollection<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreationDate = _timeService.GetCurrentTime();
                entity.CreatedBy = _claimsService.GetCurrentUserId;
            }
            _dbSet.UpdateRange(entities);
        }

        public Task<Pagination<TEntity>> FindAsync(
            Expression<Func<TEntity, bool>>? predicate = null,
            SortingConditionQueue<TEntity>? sortConditions = null,
            int pageIndex = 0, int pageSize = 10)
        {
            return _dbSet
                    .WhereIfNotNull(predicate)
                    .OrderBy(sortConditions)
                    .PaginateAsync(pageIndex, pageSize);
        }

        public async Task<IList<TEntity>> FindAsync(
            Expression<Func<TEntity, bool>>? predicate = null,
            SortingConditionQueue<TEntity>? sortConditions = null)
        {
            return await _dbSet
                            .WhereIfNotNull(predicate)
                            .OrderBy(sortConditions)
                            .ToListAsync();
        }
        public async Task<TEntity?> GetByIdAsync(
            Guid id,
            params Expression<Func<TEntity, object?>>[] includes
            )
        {

            IQueryable<TEntity> query = _dbSet;

            if (includes != null)
            {
                foreach (Expression<Func<TEntity, object?>> include in includes)
                query = query.Include(include);
            }
            return await query.FirstOrDefaultAsync();
        }
    }
}
