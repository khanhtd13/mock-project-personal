﻿using Application.Interfaces;
using Application.Repositories;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.Helper;
using Global.Shared.ViewModels.FeedbackViewModels;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class FeedbackRepository : GenericRepository<FeedBack>, IFeedbackRepository
    {
        public FeedbackRepository(
            AppDbContext context,
            ICurrentTime timeService,
            IClaimsService claimsService)
            : base(context,
                  timeService,
                  claimsService)
        {
        }
        public async Task<FeedBack?> GetFeedbackWithQuestionAndResultById(Guid id)
        {
            return await _dbSet.Where(x => x.Id == id)
                                .Include(x => x.FeedBackResults)
                                .Include(x => x.FeedBackQuestions)
                                .AsNoTracking()
                                .FirstOrDefaultAsync();
        }

        public async Task<Pagination<FeedBack>> SearchAsync(SearchFeedbackViewModel searchFeedback)
        {
            var queryable = _dbSet.AsQueryable();
            Expression<Func<FeedBack, bool>> predicate = x => true;
            if (!string.IsNullOrEmpty(searchFeedback.Title))
            {
                Expression<Func<FeedBack, bool>> subExpression = x => x.Title
                                                                        .ToLower()
                                                                        .Contains(
                                                                            searchFeedback.Title.ToLower()
                                                                            );
                predicate = ExpressionHelper<FeedBack>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            if (searchFeedback.CreationDate != null)
            {
                Expression<Func<FeedBack, bool>> subExpression =
                    x => x.CreationDate.Subtract(searchFeedback.CreationDate.Value).Days == 0;
                predicate = ExpressionHelper<FeedBack>.ExpressionCombineAndAlso(predicate, subExpression);
            }
            var result = await FindAsync(predicate, pageIndex: searchFeedback.PageIndex, pageSize: searchFeedback.PageSize);

            return result;
        }
    }
}
