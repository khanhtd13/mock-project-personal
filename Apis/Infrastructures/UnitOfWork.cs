using Application;
using Application.Interfaces;
using Application.Repositories;
using Infrastructures.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        private readonly IChemicalRepository _chemicalRepository;
        private readonly IFresherRepository _fresherRepository;
        private readonly IClassFresherRepository _classFresherRepository;
        private readonly IReminderRepository _reminderRepository;
        private readonly IScoreRepository _scoreRepository;
        private readonly IFresherReportRepository _fresherReportRepository;
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IFeedbackQuestionRepository _feedbackQuestionRepository;
        private readonly IFeedbackResultRepository _feedbackResult;
        
        private readonly IPlanRepository _planRepository;
        private readonly ITopicRepository _topicRepository;
        private readonly IModuleRepository _moduleRepository;
        private readonly IPlanInformationRepository _planInformationRepository;
        public UnitOfWork(AppDbContext appDbContext,
            IChemicalRepository chemicalRepository, IScoreRepository scoreRepository,
            IClassFresherRepository classFresherRepository,
            IFresherRepository fresherRepository,
            IReminderRepository reminderRepository,
	        IFresherReportRepository fresherReportRepository,
            IFeedbackRepository feedbackRepository,
            IFeedbackQuestionRepository feedbackQuestionRepository,
            IFeedbackResultRepository feedbackResultRepository,
            IPlanRepository planRepository,
            IModuleRepository moduleRepository,
            ITopicRepository topicRepository,
            IPlanInformationRepository planInformationRepository
            )
        {
            _appDbContext = appDbContext;
            _chemicalRepository = chemicalRepository;
            _reminderRepository = reminderRepository;
            _classFresherRepository = classFresherRepository;
            _fresherRepository = fresherRepository;
            _scoreRepository = scoreRepository;
            _fresherReportRepository = fresherReportRepository;
            _feedbackRepository = feedbackRepository;
            _feedbackQuestionRepository = feedbackQuestionRepository;
            _feedbackResult = feedbackResultRepository;
            _planRepository = planRepository;
            _topicRepository = topicRepository;
            _moduleRepository = moduleRepository;
            _planInformationRepository = planInformationRepository;
        }
        public IChemicalRepository ChemicalRepository { get => _chemicalRepository; }

        public IReminderRepository ReminderRepository { get => _reminderRepository; }

        public IClassFresherRepository ClassFresherRepository { get => _classFresherRepository; }

        public IFresherRepository FresherRepository { get => _fresherRepository; }

        public IScoreRepository ScoreRepository { get => _scoreRepository; }
        public IFresherReportRepository FresherReportRepository { get => _fresherReportRepository; }
        public IFeedbackRepository FeedbackRepository { get => _feedbackRepository; }
        public IFeedbackQuestionRepository FeedbackQuestionRepository { get => _feedbackQuestionRepository; }
        public IFeedbackResultRepository FeedbackResultRepository { get => _feedbackResult; }

        public IPlanRepository PlanRepository { get => _planRepository; }
        public ITopicRepository TopicRepository { get => _topicRepository; }
        public IModuleRepository ModuleRepository { get => _moduleRepository; }
        public IPlanInformationRepository PlanInformationRepository
        {
            get => _planInformationRepository;
        }
        public async Task<int> SaveChangeAsync()
        {
            return await _appDbContext.SaveChangesAsync();
        }
        public void BeginTransaction()
        {
            _appDbContext.Database.BeginTransaction();
        }
        public void Commit()
        {
            _appDbContext.Database.CommitTransaction();
        }
    }
}
