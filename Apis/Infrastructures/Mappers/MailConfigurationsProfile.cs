﻿using AutoMapper;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.ViewModels.MailViewModels;
using System.Net.Mail;

namespace Infrastructures.Mappers
{
    public class MailConfigurationsProfile : Profile
    {
        public MailConfigurationsProfile()
        {
            CreateMap<MailMessage, MailViewModel>()
                .ForMember(dest => dest.ToAddresses,
                                opt => opt.MapFrom(src => string.Join(',', src.To.Select(t => t.Address))))
                .ForMember(dest => dest.CCAddresses,
                                opt => opt.MapFrom(src => string.Join(',', src.CC.Select(t => t.Address))))
                .ForMember(dest => dest.Body,
                                opt => opt.MapFrom(src => src.Body));
        }
    }
}
