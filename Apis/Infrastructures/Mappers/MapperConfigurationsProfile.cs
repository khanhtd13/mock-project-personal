using AutoMapper;
using Domain.Entities;
using Global.Shared.Commons;
using Global.Shared.Helper;
using Global.Shared.ModelExport.ModelExportConfiguration;
using Global.Shared.ViewModels.ChemicalsViewModels;
using Global.Shared.ViewModels.MeetingRequestViewModels;
using Global.Shared.ViewModels.MeetingRequestViewModels.Internal;
using Global.Shared.ViewModels.ReminderViewModels;
using Global.Shared.ViewModels.ReportsViewModels;
using Global.Shared.ViewModels.ScoreViewModels;
using Global.Shared.ViewModels.FeedbackViewModels;
using Global.Shared.ViewModels.TopicViewModels;
using Global.Shared.ViewModels.ModuleViewModels;
using Global.Shared.ViewModels.PlanViewModels;
using Global.Shared.ViewModels.PlanInfomationViewModels;

namespace Infrastructures.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {
        public MapperConfigurationsProfile()
        {
            CreateMap<CreateChemicalViewModel, Chemical>();
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
            CreateMap<Chemical, ChemicalViewModel>()
                .ForMember(dest => dest._Id, src => src.MapFrom(x => x.Id));

            CreateMap<ReminderViewModel, Reminder>().ReverseMap();
            CreateMap<CreateReminderViewModel, Reminder>().ReverseMap();


            CreateMap<CreateMeetingRequestViewModel, MicrosoftGraphCreateMeetingRequest>();
            CreateMap<DateTime, MicrosoftGraphMeetingDateTime>()
                .ConvertUsing<DateTimeToMicrosoftGraphDateTimeConverter>();
            CreateMap<string, MicrosoftGraphMeetingAttendee>()
                .ConvertUsing<StringToMicrosoftGraphMeetingAttendeeConverter>();
            #region Score
            CreateMap<Score, ScoreViewModel>().ReverseMap();
            CreateMap<Score, CreateScoreViewModel>().ReverseMap();
            CreateMap<Score, UpdateScoreViewModel>().ReverseMap();
            #endregion
            CreateMap<FresherReport, ExportCourseReportViewModel>();
            CreateMap<UpdateFresherReportViewModel, FresherReport>();
            CreateMap<CreateFeedbackQuestionViewModel, FeedBackQuestion>();
            CreateMap<CreateFeedbackResultViewModel, FeedBackResult>();
            CreateMap<CreateFeedbackViewModel, FeedBack>();
            CreateMap<FeedBack, FeedbackViewModel>();
            CreateMap<FeedBackQuestion, FeedbackQuestionViewModel>();
            CreateMap<FeedBackResult, FeedbackResultViewModel>();
            CreateMap<UpdateFeedbackQuestionViewModel, FeedBackQuestion>();
            CreateMap<UpdateFeedbackViewModel, FeedBack>();
            #region PlanProgram_mapper

            CreateMap<Topic, CreateTopicViewModel>().ReverseMap();
            CreateMap<Topic, UpdateTopicViewModel>().ReverseMap();
            CreateMap<Topic, TopicViewModel>().ReverseMap();

            CreateMap<Plan, PlanGetViewModel>().ReverseMap();
            CreateMap<Plan, PlanGetByIdViewModel>().ReverseMap();
            CreateMap<PlanAddViewModel, Plan>().ReverseMap();
            CreateMap<Plan, PlanUpdateViewModel>().ReverseMap();

            CreateMap<ModuleAddViewModel, Module>();
            CreateMap<Module, ModuleViewModel>().ReverseMap();
            CreateMap<Module, ModuleUpdateViewModel>().ReverseMap();

            CreateMap<PlanInformation, PlanInformationViewModel>().ReverseMap();
            CreateMap<Topic, PlanInformationViewModel>()
                    .ForMember(dest => dest.ModuleName, src =>
                            src.MapFrom(x => x.Module.ModuleName))
                    .ForMember(dest => dest.PlanName, src =>
                                            src.MapFrom(x => x.Module.Plan.CourseName))
                    .ForMember(dest => dest.TopicName, src => src.MapFrom(x => x.Name));
            CreateMap<PlanInformationViewModel, PlanInformation>();

            #endregion

            CreateMap<Fresher, FresherReport>()
                .ForMember(dest => dest.Account,
                           src => src.MapFrom(f => f.AccountName))
                .ForMember(dest => dest.Name,
                           src => src.MapFrom(f => f.FirstName + " " + f.LastName))
                .ForMember(dest => dest.UniversityId,
                           src => src.MapFrom(f => f.University))
                .ForMember(dest => dest.ToeicGrade,
                           src => src.MapFrom(f => f.English))
                .ForMember(dest => dest.UniversityGPA,
                           src => src.MapFrom(f => f.GPA))
                .ForMember(dest => dest.Branch,
                           src => src.MapFrom(f => StringHelper.Split(f.RRCode)[0]
                                                 + "."
                                                 + StringHelper.Split(f.RRCode)[1]))
                .ForMember(dest => dest.ParentDepartment,
                           src => src.MapFrom(f => StringHelper.Split(f.RRCode)[2]
                                                 + "."
                                                 + StringHelper.Split(f.RRCode)[3]));

            CreateMap<ClassFresher, FresherReport>()
                .ForMember(dest => dest.CourseCode,
                           src => src.MapFrom(c => c.ClassCode))
                .ForMember(dest => dest.CourseName,
                           src => src.MapFrom(c => c.ClassName))
                .ForMember(dest => dest.CourseValidOrStartDate,
                           src => src.MapFrom(c => c.StartDate))
                .ForMember(dest => dest.CourseValidOrEndDate,
                           src => src.MapFrom(c => c.EndDate))
                .ForMember(dest => dest.Site,
                           src => src.MapFrom(c => c.Location));

            CreateMap<ClassFresher, IEnumerable<FresherReport>>()
                .ConvertUsing<ClassFresherConverter>();

            CreateMap<Fresher, ExportCourseReportViewModel>()
                .ForMember(dest => dest.Account,
                           src => src.MapFrom(f => f.AccountName))
                .ForMember(dest => dest.Name,
                           src => src.MapFrom(f => f.FirstName + " " + f.LastName))
                .ForMember(dest => dest.UniversityId,
                           src => src.MapFrom(f => f.University))
                .ForMember(dest => dest.ToeicGrade,
                           src => src.MapFrom(f => f.English))
                .ForMember(dest => dest.UniversityGPA,
                           src => src.MapFrom(f => f.GPA))
                .ForMember(dest => dest.Branch,
                           src => src.MapFrom(f => StringHelper.Split(f.RRCode)[0]
                                                 + "."
                                                 + StringHelper.Split(f.RRCode)[1]))
                .ForMember(dest => dest.ParentDepartment,
                           src => src.MapFrom(f => StringHelper.Split(f.RRCode)[2]
                                                 + "."
                                                 + StringHelper.Split(f.RRCode)[3]));

            CreateMap<ClassFresher, ExportCourseReportViewModel>()
                .ForMember(dest => dest.CourseCode,
                           src => src.MapFrom(c => c.ClassCode))
                .ForMember(dest => dest.CourseName,
                           src => src.MapFrom(c => c.ClassName))
                .ForMember(dest => dest.CourseValidOrStartDate,
                           src => src.MapFrom(c => c.StartDate))
                .ForMember(dest => dest.CourseValidOrEndDate,
                           src => src.MapFrom(c => c.EndDate))
                .ForMember(dest => dest.Site,
                           src => src.MapFrom(c => c.Location));

            CreateMap<ClassFresher, IEnumerable<ExportCourseReportViewModel>>()
                .ConvertUsing<ExportReportConverter>();

            CreateMap<UpdateWeeklyFresherReportViewModel, FresherReport>();
        }
    }
}

            
