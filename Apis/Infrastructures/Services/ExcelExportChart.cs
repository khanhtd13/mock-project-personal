﻿using Application.Interfaces;
using Global.Shared.ExportExcelExtensions;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;

namespace Infrastructures.Services
{
    public class ExcelExportChartService :  IExcelExportChartService
    {
        private readonly SaveWorkBook _saveWorkBook;
        public ExcelExportChartService(SaveWorkBook saveWorkBook)
        {
            _saveWorkBook = saveWorkBook;
        }
        public async Task<FileContentResult> Export(Dictionary<string, float> values)
        {
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                //create a WorkSheet
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Chart");

                //fill cell data with a loop, note that row and column indexes start at 1

                var i = 1;
                foreach (var item in values)
                {
                    worksheet.Cells[1, i].Value = item.Key;
                    worksheet.Cells[2, i].Value = item.Value;
                    i++;
                }
                //create a new piechart of type Pie3D
                ExcelPieChart? pieChart = worksheet.Drawings.AddChart("Pie Chart", eChartType.Pie3D) as ExcelPieChart;

                //set the title
                pieChart.Title.Text = "PieChart Example";

                //select the ranges for the pie. First the values, then the header range    
                pieChart.Series.Add(ExcelRange.GetAddress(2, 1, 2, 10), ExcelRange.GetAddress(1, 1, 1, 10));

                //position of the legend
                pieChart.Legend.Position = eLegendPosition.Right;

                //show the percentages in the pie
                pieChart.DataLabel.ShowPercent = true;
                pieChart.DataLabel.ShowValue = true;

                //size of the chart
                pieChart.SetSize(500, 400);

                //add the chart at cell C5
                pieChart.SetPosition(4, 0, 2, 0);
                return await _saveWorkBook.SaveFileAsync(excelPackage, "");
            }
        }
    }
}
