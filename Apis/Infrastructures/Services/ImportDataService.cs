﻿using Application;
using Application.Interfaces;
using Application.SeedData;
using Domain.Entities;
using Ganss.Excel;
using Global.Shared.ViewModels.ImportViewModels;
using Infrastructures.Extensions;
using Microsoft.AspNetCore.Http;

namespace Infrastructures.Services
{

    public class ImportDataService : IImportDataService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ImportDataService(IUnitOfWork _unitOfWork  )
        {
            this._unitOfWork = _unitOfWork;
        }
        public async Task<PackageReponseImportViewModel?>
            GetDataFromFileExcelAsync(IFormFile? fileExcel)
        { 
            if (fileExcel.CheckImportRECFileTypeInput())
            {
                var file = fileExcel.OpenReadStream();
                var instanceMapper = new ExcelMapper(file);
                if (instanceMapper == null) return null;
                var listAccount_ImportVM = instanceMapper.GetAccountFromImportFile();
                var listClass_ImportVM = instanceMapper.GetClassFromImportFile();
                var listFresher_ImportVM = instanceMapper.GetFresherFromImportFile();

                if (listAccount_ImportVM == null || listClass_ImportVM == null ||
                    listFresher_ImportVM == null) return null;

                //encapsule data into a package to reponse back
                var packageDataReponse = new PackageReponseImportViewModel()
                {
                    ListaccountImpVM = listAccount_ImportVM,
                    ListclassImpVM = listClass_ImportVM,
                    ListfresherImpVM = listFresher_ImportVM
                };
                return await CreateClassCodeFromPackageDataReponseAsync(packageDataReponse);
            }
            return null;
        }

        public int YearWithTwoNumber() => DateTime.Now.Year - 2000;

        public async Task<PackageReponseImportViewModel>
            CreateClassCodeFromPackageDataReponseAsync(PackageReponseImportViewModel package)
        {
            var fresherFromPackageList = package.ListfresherImpVM.ToList();
            var classFresherFromPackageList = package.ListclassImpVM.ToList();

            var fresherFromDBList = await _unitOfWork.FresherRepository.GetAllAsync();
            var classFresherFromDBList = await _unitOfWork.ClassFresherRepository.GetAllAsync();

            var locationsList = DataInnitializer.SeedData<ClassLocations>("ClassLocations");
            var jobRanksList = DataInnitializer.SeedData<FreshersJobRank>("FreshersJobRank");
            var majorList = DataInnitializer.SeedData<FresherMajor>("FreshersMajor");

            var listClassCodeCreateAtThisImport = new Dictionary<string, int>();

            if (jobRanksList == null || majorList == null) return package;

            fresherFromPackageList.ToList().ForEach(s => s.Major = s.Major.Split("-")[1]);

            foreach (var newclass in classFresherFromPackageList)
            {
                var freshersInformationToCreateClass =
                    fresherFromPackageList.FirstOrDefault(p => p.RRCode.Equals(newclass.RRCode));
                if (freshersInformationToCreateClass == null || newclass.RRCode == null) return package;

                var getClassCodeIfClassFresherExistInDB = classFresherFromDBList.FirstOrDefault(p => p.RRCode.Equals(newclass.RRCode));
                if (getClassCodeIfClassFresherExistInDB != null)
                {
                    newclass.ClassCode = getClassCodeIfClassFresherExistInDB.ClassCode;
                    fresherFromPackageList.Where(p => p.RRCode.Equals(newclass.RRCode)).ToList().
                        ForEach(s => s.ClassCode = newclass.ClassCode);
                }
                else
                {
                    /*RRCode : FSO.HCM.FHO.FA.G0.SG_2022.57_4 
                * => FSO 
                * => HCM (Get Location from here)
                * => FHO
                * => FA
                * => GO
                * => SG_2022
                * => 57_4
                */
                    var propertiesOfRRCode = newclass.RRCode.Split(".");
                    var classSkill = freshersInformationToCreateClass.Skill.ToUpper();

                    var getClassJobRankInJson = jobRanksList.
                        Find(p => p.JobRankName.Equals(freshersInformationToCreateClass.JobRank));
                    var classJobRank = (getClassJobRankInJson == null) ? null : getClassJobRankInJson.JobRankCode;

                    //HCM22_FR_NET_
                    // Count if this Class Code has existed in db 
                    string classCode = $"{propertiesOfRRCode[1]}{YearWithTwoNumber()}_" +
                                                             $"{classJobRank}_{classSkill}_";

                    var indexInt = classFresherFromDBList.Count(p => p.ClassCode.Contains(classCode)) + 1 +
                        listClassCodeCreateAtThisImport.Count(p => p.Key.Contains(classCode));
                    var indexString = (indexInt < 9) ? $"0{indexInt}" : $"{indexInt}";

                    classCode += indexString;
                    listClassCodeCreateAtThisImport.Add(classCode, indexInt);
                    var getLocationName = locationsList.
                        Find(p => p.LocationCode.Equals(propertiesOfRRCode[1]));
                    newclass.Location = (getLocationName == null) ? null : getLocationName.LocationName;
                    newclass.ClassCode = classCode;

                    fresherFromPackageList.Where(p => p.RRCode.Equals(newclass.RRCode)).ToList().
                        ForEach(s => s.ClassCode = classCode);
                }
               
            }

            package.ListfresherImpVM = fresherFromPackageList;
            package.ListclassImpVM = classFresherFromPackageList;
            return package;
        }

    }
}
