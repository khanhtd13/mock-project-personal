﻿using Application.Interfaces;
using Global.Shared.ViewModels.ModuleViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class ModuleController : BaseController
    {
        private readonly IModuleService _moduleService;
        public ModuleController(IModuleService moduleService)
        {
            _moduleService = moduleService;
        }
        [HttpPost]
        public async Task<IActionResult> SeedData()
        {
            await _moduleService.SeedDataModules();
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> GetAllModule(int pageIndex, int pageSize)
        {
            var response = await _moduleService.GetAllAsync(pageIndex, pageSize);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetModuleById([FromRoute] Guid id)
        {
            var response = await _moduleService.GetByIdAsync(id);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
        [HttpGet("{planId}")]
        public async Task<IActionResult> GetModuleByPlanId([FromRoute] Guid planId)
        {
            var response = await _moduleService.GetByPlanIdAsync(planId);
            return Ok(response);
        }
        [HttpPost]
        public async Task<IActionResult> AddModule(
                                ModuleAddViewModel moduleAddViewModel)
        {
            var response = await _moduleService.AddModuleAsync(moduleAddViewModel);
            return Ok(response);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateModule(
                    [FromRoute] Guid id, ModuleUpdateViewModel moduleUpdateViewModel)
        {
            var response = await _moduleService.UpdateModuleAsync(
                                             id, moduleUpdateViewModel);
            if (response == null)
            {
                return NotFound();
            }
            return Ok(response);
        }
    }
}
