﻿using Application.Interfaces;
using Global.Shared.ModelExport.ModelExportConfiguration;
using Infrastructures.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class ExportExcelController : BaseController
    {
        private readonly IExcelExportHistoryService _excelExportHistoryService;
        private readonly IExcelExportDeliveryService _excelExportDeliveryService;
        private readonly IExcelExportChartService _excelExportChartService;

        public ExportExcelController(IExcelExportHistoryService excelExportHistoryService,
                                     IExcelExportDeliveryService excelExportDeliveryService,
                                     IExcelExportChartService excelExportChartService)
        {
            _excelExportHistoryService = excelExportHistoryService;
            _excelExportDeliveryService = excelExportDeliveryService;
            _excelExportChartService = excelExportChartService;
        }

        [HttpPost]
        [Route("/api/export/employee-training-history")]
        public async Task<IActionResult> ExportEmployeeTrainingHistoryAsync([FromBody] List<ExportCourseReportViewModel> values)
        {
            return await _excelExportHistoryService.Export(values);
        }

        [HttpPost]
        [Route("/api/export/employee-training-delivery")]
        public async Task<IActionResult> ExportEmployeeTrainingDeliveryAsync()
        {
            return await _excelExportDeliveryService.Export();
        }

        [HttpPost]
        [Route("/api/export/chart")]
        public async Task<IActionResult> ExportChartAsync([FromBody] Dictionary<string, float> data)
        {
            return await _excelExportChartService.Export(data);
        }
    }
}
