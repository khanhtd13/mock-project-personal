﻿using Application.Interfaces;
using Global.Shared.ViewModels.FeedbackViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class FeedbackController : BaseController
    {
        private readonly IFeedbackService _feedbackService;

        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateFeedbackQuestionAsync([FromRoute] CreateFeedbackQuestionViewModel feedbackQuestion)
        {
            var result = await _feedbackService.CreateFeedbackQuestionAsync(feedbackQuestion);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateFeedbackResultAsync([FromRoute] CreateFeedbackResultViewModel feedbackResult)
        {
            var result = await _feedbackService.CreateFeedbackResultAsync(feedbackResult);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateFeedbackAsync(CreateFeedbackViewModel feedbackResult)
        {
            var result = await _feedbackService.CreateFeedbackAsync(feedbackResult);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateFeedbackAsync([FromRoute] UpdateFeedbackViewModel updateFeedback)
        {
            var result = await _feedbackService.UpdateFeedbackAsync(updateFeedback);
            return Ok(result);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateFeedbackQuestionAsync([FromRoute]UpdateFeedbackQuestionViewModel updateFeedbackQuestion)
        {
            var result = await _feedbackService.UpdateFeedbackQuestionAsync(updateFeedbackQuestion);
            return Ok(result);
        }
        [HttpGet]
        public async Task<IActionResult> GetFeedbackByIdAsync(Guid id)
        {
            var result = await _feedbackService.GetFeedbackByIdAsync(id);
            return Ok(result);
        } 
        
        [HttpGet]
        public async Task<IActionResult> GetFeedbackResultByIdAsync(Guid id)
        {
            var result = await _feedbackService.GetFeedbackResultByIdAsync(id);
            return Ok(result);
        }  

        [HttpGet]
        public async Task<IActionResult> GetFeedbackQuestionByIdAsync(Guid id)
        {
            var result = await _feedbackService.GetFeedbackQuestionByIdAsync(id);
            return Ok(result);
        }  

        [HttpGet]
        public async Task<IActionResult> GetAllResultOfFeedbackAsync(Guid feedbackId)
        {
            var result = await _feedbackService.GetAllResultOfFeedbackByFeedbackIdAsync(feedbackId);
            return Ok(result);
        } 

        [HttpGet]
        public async Task<IActionResult> SearchFeedbackQuestionAsync([FromQuery] SearchFeedbackQuestionViewModel searchFeedbackQuestion)
        {
            var result = await _feedbackService.SearchFeedbackQuestionAsync(searchFeedbackQuestion);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> SearchFeedbackResultAsync([FromQuery] SearchFeedbackResultViewModel searchFeedbackResult)
        {
            var result = await _feedbackService.SearchFeedbackResultAsync(searchFeedbackResult);
            return Ok(result);
        }   
        
        [HttpGet]
        public async Task<IActionResult> SearchFeedbackAsync([FromQuery]SearchFeedbackViewModel searchFeedback)
        {
            var result = await _feedbackService.SearchFeedbackAsync(searchFeedback);
            return Ok(result);
        }   

        [HttpDelete]
        public async Task<IActionResult> DeleteFeedbackAsync(Guid id)
        {
            await _feedbackService.DeleteFeedbackAsync(id);
            return Ok();
        }      
        [HttpDelete]
        public async Task<IActionResult> DeleteFeedbackQuestionAsync(Guid id)
        {
            await _feedbackService.DeleteFeedbackQuestionAsync(id);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Seed()
        {
            await _feedbackService.SeedData();
            return Ok();
        }
    }
}
