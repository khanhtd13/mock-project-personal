using Application.Interfaces;
using Global.Shared.Settings;
using Global.Shared.ViewModels;
using Global.Shared.ViewModels.MailViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class MailsController : BaseController
    {
        private readonly IMailService _mailService;

        public MailsController(IMailService mailService)
        {
            _mailService = mailService;
        }

        [HttpPost]
        public async Task<MailViewModel> Send(MailRequestViewModel request)
        {
            return await _mailService.SendAsync(request);
        }
    }
}