﻿using Application.Interfaces;
using Domain.Enums;
using Global.Shared.Commons;
using Global.Shared.ViewModels;
using Global.Shared.ViewModels.ImportViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class ClassFresherController : BaseController
    {
        private readonly IClassFresherService _classFresherService;
        private readonly IFresherService _fresherService;
        public ClassFresherController(IClassFresherService classFresherService,
                                        IFresherService fresherService)

        {
            _classFresherService = classFresherService;
            _fresherService = fresherService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllClassFresherPagingsionAsync(int pageIndex = 0, int pageSize = 10)
        {
            var listFresher = await _classFresherService.GetAllClassFreshersPagingsionAsync(pageIndex, pageSize);
            return Ok(listFresher);

        }

        [HttpPost("seed")]
        public async Task<IActionResult> Seed()
        {
            await _classFresherService.SeedData();
            await _fresherService.SeedDataFresher();
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetClassFresherByIdAsync(Guid id)
        {
            var classFresher = await _classFresherService.GetClassFresherByIdAsync(id);
            return Ok(classFresher);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateClassFresherAsync([FromBody] CreateClassFresherViewModel classFresherCreateViewModel)
        {
            var create = await _classFresherService.CreateClassFresherAsync(classFresherCreateViewModel);
            return Ok(create);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateClassFresherInfomationAsync([FromBody] ClassFresherViewModel classFresherViewModel)
        {
            var classFresherUpdate = await _classFresherService.UpdateClassFresher(classFresherViewModel);
            return Ok(classFresherUpdate);
        }

        [HttpGet("fresher/addtoclass")]
        public async Task<IActionResult> GetFresherByClassCodeAsync(string classCode)
        {
            var listFresher = await _classFresherService.GetFreshersByClassCodeAsync(classCode);
            return Ok(listFresher);
        }

        [HttpGet("fresher/{id}")]
        public async Task<IActionResult> GetFresherByIdAsync(Guid id)
        {
            var fresher = await _fresherService.GetFresherByIdAsync(id);
            return Ok(fresher);
        }

        [HttpPost("fresher/{id}/changestaus")]
        public async Task<IActionResult> ChangStatusFresherAsync(Guid id, StatusFresherEnum status)
        {
            var result = await _fresherService.ChangeFresherStatus(id, status);
            return Ok(result);
        }

        [HttpPost("import")]
        public async Task<IActionResult> CreateClassFresherFromImportedFileAsync(IFormFile fileExcel)
        {
            var GetPackageReponse = await _classFresherService.CreateCLassFresherFromImportedExcelFile(fileExcel);
            return Ok(GetPackageReponse);
        }
        [HttpGet("{id}/hasfreshers")]
        public async Task<IActionResult> GetClassHasFresherByClassIdAsync(Guid id)
        {
            var classFresher = await _classFresherService.GetClassHasFresherByIdAsync(id);
            return Ok(classFresher);
        }
        [HttpGet("classcode")]
        public async Task<IActionResult> GetAllClassCodeAsync()
        {
            var listClassCode = await _classFresherService.GetAllClassCodeAsync();
            return Ok(listClassCode);
        }
    }
}
