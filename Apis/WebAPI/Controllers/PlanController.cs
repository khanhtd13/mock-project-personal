﻿using Application.Interfaces;
using Global.Shared.ViewModels.PlanInfomationViewModels;
using Global.Shared.ViewModels.PlanViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class PlanController : BaseController
    {
        private readonly IPlanService _planService;
        
        public PlanController(IPlanService planService)
        {
            _planService = planService;
        }
        [HttpPost]
        public async Task<IActionResult> SeedData()
        {
            await _planService.SeedDataPlans();
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> GetAllPlan(int pageIndex, int pageSize)
        {
            return Ok(await _planService.GetAllAsync(pageIndex, pageSize));
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlanById([FromRoute] Guid id)
        {
            return Ok(await _planService.GetPlanByIdAsync(id));
        }
        [HttpPost]
        public async Task<IActionResult> AddPlan(
                                PlanAddViewModel planAddViewModel)
        {
            return Ok(await _planService.AddItemPlanAsync(planAddViewModel));
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePlan(
                [FromRoute] Guid id, PlanUpdateViewModel planUpdateViewModel)
        {
            return Ok(await _planService.UpdatePlanAsync(
                                            id, planUpdateViewModel));
        }
        [HttpPost]
        public async Task<IActionResult> ChoosePlanForClassAsync(
                        ChoosePlanForClassViewModel planInfomationAddViewModel)
        {
            return Ok(await _planService.ChoosePlanForClassAsync(
                                                planInfomationAddViewModel));
        }
    }
}
