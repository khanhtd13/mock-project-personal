﻿using Application.Interfaces;
using Global.Shared.ViewModels.TopicViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class TopicsController : BaseController
    {
        private readonly ITopicService _topicService;
        public TopicsController(ITopicService topicService)
        {
            _topicService = topicService;
        }
        [HttpPost]
        public async Task<IActionResult> SeedData()
        {
            await _topicService.SeedDataTopics();
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> GetAll(int pageIndex, int pageSize)
        {
            var result = await _topicService.GetAllAsync(pageIndex, pageSize);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTopicById([FromRoute] Guid id)
        {
            var result = await _topicService.GetTopicByIdAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
        [HttpGet("{moduleId}")]
        public async Task<IActionResult> GetTopicByModuleId(
                                            [FromRoute] Guid moduleId)
        {
            var result = await _topicService
                                .GetTopicByModuleIdAsync(moduleId);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> AddTopic(
                        CreateTopicViewModel createTopicViewModel)
        {
            var result = await _topicService
                                .AddTopicAsync(createTopicViewModel);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTopic(
            [FromRoute] Guid id, UpdateTopicViewModel updateTopicViewModel)
        {
            var result = await _topicService.UpdateTopicAsync(
                                                 id, updateTopicViewModel);
            if (result == null)
            {
                return BadRequest();
            }
            return Ok(result);
        }
    }
}
