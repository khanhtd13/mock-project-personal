﻿using Application.Interfaces;
using Global.Shared.ViewModels.ScoreViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    public class ScoreController : BaseController
    {
        private readonly IScoreService _scoreService;
        public ScoreController(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }

        [HttpGet]
        public async Task<List<ScoreViewModel>?> GetAllScore()
        {
            return await _scoreService.GetAllScoreAsync();
        }

        [HttpPost]
        public async Task<ScoreViewModel?> CreateScore(CreateScoreViewModel
                                                                     createScoreViewModel)
        {
            return await _scoreService.CreateScoreAsync(createScoreViewModel);
        }

        [HttpPut]
        public async Task<ScoreViewModel?> UpdateScore(UpdateScoreViewModel
                                                                     updateScoreViewModel)
        {
            return await _scoreService.UpdateScoreAsync(updateScoreViewModel);
        }

        [HttpDelete]
        public async Task DeleteScore(Guid id)
        {
            await _scoreService.DeleteScoreAsync(id);
        }
    }
}
