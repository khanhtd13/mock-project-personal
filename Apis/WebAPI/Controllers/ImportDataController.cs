﻿using Application.Interfaces;
using Global.Shared.ViewModels.ImportViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/class-freshers-management")]
    public class ImportDataController : BaseController
    {
        private readonly IImportDataService _importDataService;
        public ImportDataController(IImportDataService _importDataService)
        {
            this._importDataService = _importDataService;
        }

        [HttpPost("import")]
        public async Task<PackageReponseImportViewModel?> GetDataFromImportFile(IFormFile fileExcel)
        {
            var GetPackageReponse = await _importDataService.GetDataFromFileExcelAsync(fileExcel);
            return GetPackageReponse;
        }
    }
}
