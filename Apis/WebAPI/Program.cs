using Infrastructures;
using WebAPI;
using WebAPI.Middlewares;
using Infrastructures.SeedData;
using Hangfire;
using Infrastructures.SeedData;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

var configuration = builder.Configuration;

builder.Services.AddInfrastructuresService(configuration, builder.Environment);
builder.Services.AddWebAPIService();

builder.Services.AddMeetingRequestServices(configuration);
builder.Services.AddRootSetting(configuration);
builder.Services.AddReminderSetting(configuration);
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins("http://localhost:3000",
                                              "http://localhost:8080")
                          .AllowAnyHeader()
                          .AllowAnyMethod();
                      });
});

var app = builder.Build();

SeedDataForReportTesting.SeedDataInit(app);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseStaticFiles();
app.UseMiddleware<ExceptionMiddleware>();
app.UseMiddleware<PerformanceMiddleware>();
app.MapHealthChecks("/healthchecks");
app.UseHttpsRedirection();
app.UseCors(MyAllowSpecificOrigins);
// todo authentication
app.UseCors(x => x.AllowAnyHeader()
      .AllowAnyMethod()
      .WithOrigins("*"));
app.UseAuthorization();

app.MapControllers();
app.UseHangfireDashboard();
app.UseCronJobs();
app.Run();

// this line tell intergrasion test
// https://stackoverflow.com/questions/69991983/deps-file-missing-for-dotnet-6-integration-tests
public partial class Program { }
