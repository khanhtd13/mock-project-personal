﻿using Application.Interfaces;
using Hangfire;

namespace WebAPI.Middlewares
{
    public static class HangfireMiddleware
    {
        public static void UseCronJobs(this IApplicationBuilder builder)
        {
            RecurringJob.AddOrUpdate<ICronJobService>(x=> x.AutoGetChemicalAsync(), Cron.Minutely());
            RecurringJob.AddOrUpdate<ICronJobService>(x=> x.AutoGetChemicalPagingsionAsync(), Cron.Minutely());
        }
    }
}
