﻿using Application.Interfaces;
using FluentValidation.AspNetCore;
using Global.Shared.Commons;
using Global.Shared.ExportExcelExtensions;
using Global.Shared.Settings;
using Global.Shared.Settings.Reminder;
using Hangfire;
using Hangfire.MemoryStorage;
using Infrastructures.Services;
using Microsoft.Identity.Client;
using OfficeOpenXml;
using System.Diagnostics;
using WebAPI.Middlewares;
using WebAPI.Services;

namespace WebAPI
{

    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService(this IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
            services.AddHealthChecks();
            services.AddSingleton<ExceptionMiddleware>();
            services.AddSingleton<PerformanceMiddleware>();
            services.AddSingleton<Stopwatch>();
            services.AddScoped<IClaimsService, ClaimsService>();
            services.AddScoped<IUserMailCredentialService, UserMailCredentialService>();
            services.AddScoped<IImportDataService, ImportDataService>();
            services.AddScoped<IExcelExportHistoryService, ExcelExportHistoryService>();
            services.AddScoped<IExcelExportDeliveryService, ExcelExportDeliveryService>();
            services.AddScoped<IExcelExportChartService, ExcelExportChartService>();
            services.AddScoped<SaveWorkBook>();
            services.AddHttpContextAccessor();
            services.AddFluentValidation(p =>
            {
                p.RegisterValidatorsFromAssemblyContaining<Program>(lifetime: ServiceLifetime.Singleton);
                p.DisableDataAnnotationsValidation = false;
            });

            services.AddHttpClient();
            services.AddHangfireServer();
            services.AddHangfire(x =>
            {
                x.UseMemoryStorage();
            });
            return services;
        }

        public static IServiceCollection AddMeetingRequestServices(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            var publicClientApplication = PublicClientApplicationBuilder
                                                .Create(configuration["AzureAd:ClientId"])
                                                .WithTenantId(configuration["AzureAd:TenantId"])
                                                .WithDefaultRedirectUri()
                                                .Build();
            return services.AddSingleton(publicClientApplication);
        }

        public static IServiceCollection AddRootSetting(
            this IServiceCollection services, IConfiguration configuration)
        {
            var rootSetting = new RootSetting();
            configuration.GetSection(nameof(RootSetting)).Bind(rootSetting);
            services.AddSingleton(rootSetting);

            return services;
        }
        public static IServiceCollection AddReminderSetting (
            this IServiceCollection services , IConfiguration configuration)
        {
            var reminderSetting = new ReminderSettings();
            configuration.GetSection(nameof(ReminderSettings)).Bind(reminderSetting);
            services.AddSingleton(reminderSetting);
            return services;
        }
    }
}
