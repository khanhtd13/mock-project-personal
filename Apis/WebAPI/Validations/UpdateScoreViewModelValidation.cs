﻿using Domain.Enums;
using FluentValidation;
using Global.Shared.ViewModels.ScoreViewModels;

namespace WebAPI.Validations
{
    public class UpdateScoreViewModelValidation : AbstractValidator<UpdateScoreViewModel>
    {
        public UpdateScoreViewModelValidation()
        {
            RuleFor(e => e.TypeScore).IsInEnum<UpdateScoreViewModel, TypeScoreEnum>();

            RuleFor(e => e.ModuleScore).NotEmpty();

        } 
    }
}
