﻿using Domain.Enums;
using FluentValidation;
using Global.Shared.ViewModels.ScoreViewModels;

namespace WebAPI.Validations
{
    public class CreateScoreViewModelValidation : AbstractValidator<CreateScoreViewModel>
    {
        public CreateScoreViewModelValidation()
        {
            RuleFor(e => e.TypeScore).IsInEnum<CreateScoreViewModel, TypeScoreEnum>();

            RuleFor(e => e.ModuleScore).NotEmpty();

            RuleFor(e => e.ScoreMarkerId).NotNull();

            RuleFor(e => e.ModuleId).NotNull();

            RuleFor(e => e.FresherId).NotNull();
        }
    }
}
