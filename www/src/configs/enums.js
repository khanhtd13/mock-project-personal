const FresherReportStatus = ['Active', 'DropOut', 'Failed', 'Passed']
const FresherReportCompletionStatus = ['D', 'C', 'B', 'A', 'A+']

module.exports = {
    FresherReportStatus,
    FresherReportCompletionStatus
}