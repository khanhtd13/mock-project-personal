import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableRow,
  TableContainer,
  TableCell,
} from "@mui/material";
import UserListHead from "./HeaderTable";
////////////////////////////////////

const TABLE_HEAD = [
  { id: "accountName", label: "Account", alignRight: false },
  { id: "classCode", label: "Class Code", alignRight: false },
  { id: "Contract Type", label: "Contract Type", alignRight: false },
  { id: "dob", label: "DoB", alignRight: false },
  { id: "fullName", label: "Full Name", alignRight: false },
  { id: "onboardDate", label: "On Board", alignRight: false },
  { id: "phone", label: "Phone", alignRight: false },
  { id: "email", label: "Email", alignRight: false },
  { id: "status", label: "Status", alignRight: false },
  { id: "skill", label: "Skill", alignRight: false },
];

export default function DataTableImportClass({ classFresher }) {
  const [isOpen, setIsopen] = useState(false);
  const showListFresher = () => {
    if (isOpen) setIsopen(false);
    else setIsopen(true);
  };
  const [confirmClass, setConfirmClass] = useState({
    nameAdmin1: "",
    nameAdmin2: "",
    nameAdmin3: "",
    nameTrainer1: "",
    nameTrainer2: "",
    nameTrainer3: "",
    emailAdmin1: "",
    emailadmin2: "",
    emailAdmin3: "",
    emailTrainer1: "",
    emailTrainer2: "",
    emailTrainer3: "",
    budget: classFresher.budget,
    className: "",
    classCode: classFresher.classCode,
    rrCode: classFresher.rrCode,
    location: classFresher.location,
    planId: "",
    startDate: "",
    endDate: "",
  });

  const ConfirmClass = () => {
    console.log(confirmClass);
  };

  return (
    <div className="import-class">
      <div className="detail-import-class">
        <div className="info-detail-import-class">
          <span>Class Code: {classFresher.classCode}</span>
          <span>Location: {classFresher.location}</span>
          <span>RR Code: {classFresher.rrCode}</span>
        </div>
        <div className="info-detail-import-class">
          <span>
            Start Date
            <input
              type="date"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  startDate: e.target.value,
                }))
              }
            />
          </span>
          <span>
            End Date
            <input
              type="date"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  endDate: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Plan
            <select
              id="standard-select"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  planId: e.target.value,
                }))
              }
            >
              <option value="Option 1">Huy Diet</option>
              <option value="Option 2">Tieu Diet Fresher</option>
              <option value="Option 3">Clear Fresher</option>
              <option value="Option 4">Fire Fresher</option>
            </select>
          </span>
        </div>
        <div className="info-detail-import-class">
          <span>
            Admin 1
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  nameAdmin1: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Admin 2
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  nameAdmin2: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Admin 3
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  nameAdmin3: e.target.value,
                }))
              }
            />
          </span>
        </div>
        <div className="info-detail-import-class">
          <span>
            Email Admin 1
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  emailAdmin1: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Email Admin 2
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  emailAdmin2: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Email Admin 3
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  emailAdmin3: e.target.value,
                }))
              }
            />
          </span>
        </div>
        <div className="info-detail-import-class">
          <span>
            Trainer 1
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  nameTrainer1: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Trainer 2
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  nameTrainer2: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Trainer 3
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  nameTrainer3: e.target.value,
                }))
              }
            />
          </span>
        </div>
        <div className="info-detail-import-class">
          <span>
            Email Trainer 1
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  emailTrainer1: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Email Trainer 2
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  emailTrainer2: e.target.value,
                }))
              }
            />
          </span>
          <span>
            Email Trainer 3
            <input
              type="text"
              onChange={(e) =>
                setConfirmClass((pre) => ({
                  ...pre,
                  emailTrainer3: e.target.value,
                }))
              }
            />
          </span>
        </div>
        <div className="btn-confirm">
          <button onClick={() => ConfirmClass()} className="btn-show">
            Confirm
          </button>
        </div>
      </div>
      <div style={{ width: "100%" }}>
        <button className="btn-show" onClick={() => showListFresher()}>
          Show List Fresher
        </button>
        {isOpen ? (
          <TableContainer sx={{ minWidth: 800 }}>
            <Table>
              <UserListHead headLabel={TABLE_HEAD} />
              <TableBody>
                {classFresher.freshers.map((row) => {
                  const {
                    accountName,
                    classCode,
                    contactType,
                    email,
                    firstName,
                    lastName,
                    phone,
                    skill,
                    dob,
                    status,
                    onBoard,
                  } = row;
                  return (
                    <TableRow key={phone}>
                      <TableCell align="left">{accountName}</TableCell>
                      <TableCell align="left">{classCode}</TableCell>
                      <TableCell align="left">{contactType}</TableCell>
                      <TableCell align="left">{dob}</TableCell>
                      <TableCell align="left">
                        {lastName + " " + firstName}
                      </TableCell>
                      <TableCell align="left">{onBoard}</TableCell>
                      <TableCell align="left">{phone}</TableCell>
                      <TableCell align="left">{email}</TableCell>
                      <TableCell align="left">{status}</TableCell>
                      <TableCell align="left">{skill}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        ) : null}
      </div>
    </div>
  );
}
