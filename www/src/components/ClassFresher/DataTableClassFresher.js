import React from "react";
import UserListHead from "./HeaderTable";
import {
  Table,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
} from "@mui/material";
import { useHistory } from "react-router-dom";

const TABLE_HEAD = [
  { id: "className", label: "Name", alignRight: false },
  { id: "classCode", label: "Class Code", alignRight: false },
  { id: "location", label: "Location", alignRight: false },
  { id: "isDone", label: "Status", alignRight: false },
  { id: "startDate", label: "Start Date", alignRight: false },
  { id: "endDate", label: "End Date", alignRight: false },
  { id: "nameAdmin1", label: "Admin", alignRight: false },
  { id: "nameTrainer1", label: "Trainer", alignRight: false },
  { id: "planId", label: "Plan", alignRight: false },
  { id: "budget", label: "Budget", alignRight: false },
];

export default function DataTableClassFresher({ classFresher }) {
  const history = useHistory();
  const GotoDetail = (e) => {
    history.push(`classes/detail/${e}`, { state: { id: e } });
  };

  return (
    <TableContainer sx={{ minWidth: 800 }}>
      <Table>
        <UserListHead headLabel={TABLE_HEAD} />
        <TableBody>
          {classFresher.map((row) => {
            const {
              className,
              classCode,
              location,
              isDone,
              startDate,
              endDate,
              nameAdmin1,
              nameTrainer1,
              planId,
              budget,
              id,
            } = row;

            return (
              <TableRow
                onClick={() => GotoDetail(id)}
                hover
                key={id}
                tabIndex={-1}
                role="checkbox"
                style={{
                  backgroundColor: nameAdmin1 !== null ? "" : "#cae0a8",
                }}
              >
                <TableCell align="left">{className}</TableCell>
                <TableCell align="left">{classCode}</TableCell>
                <TableCell align="left">{location}</TableCell>
                <TableCell align="left">
                  {isDone ? "Done" : "Process"}
                </TableCell>
                <TableCell align="left">{startDate.slice(0, 10)}</TableCell>
                <TableCell align="left">{endDate.slice(0, 10)}</TableCell>
                <TableCell align="left">{nameAdmin1}</TableCell>
                <TableCell align="left">{nameTrainer1}</TableCell>
                <TableCell align="left">{planId}</TableCell>
                <TableCell align="left">{budget}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
