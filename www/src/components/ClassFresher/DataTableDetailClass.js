import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableRow,
  TableContainer,
  TableCell,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from "@mui/material";
import UserListHead from "./HeaderTable";

const TABLE_HEAD = [
  { id: "accountName", label: "Account", alignRight: false },
  { id: "fullName", label: "Full Name", alignRight: false },
  { id: "classCode", label: "Class Code", alignRight: false },
  { id: "Contract Type", label: "Contract Type", alignRight: false },
  { id: "dob", label: "DoB", alignRight: false },
  { id: "jobRank", label: "Job Rank", alignRight: false },
  { id: "onboardDate", label: "On Board", alignRight: false },
  { id: "status", label: "Status", alignRight: false },
  { id: "skill", label: "Skill", alignRight: false },
  { id: "phone", label: "Phone", alignRight: false },
];

export default function DataTableDetailClass({ freshers }) {
  const [frSt, setFrSt] = useState();

  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const changeStatus = (e) => {
    setFrSt(e.target.value);
    setOpen(true);
  };
  return (
    <div style={{ width: "100%" }}>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Change Status Fresher"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Do you want to change this fresher's status to "{frSt}""
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleClose} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
      {freshers !== undefined ? (
        <TableContainer sx={{ minWidth: 800 }}>
          <Table>
            <UserListHead headLabel={TABLE_HEAD} />
            <TableBody>
              {freshers.map((row) => {
                const {
                  accountName,
                  classCode,
                  contactType,
                  dob,
                  phone,
                  jobRank,
                  onBoard,
                  skill,
                  firstName,
                  lastName,
                  id,
                  nameAdmin1,
                } = row;
                return (
                  <TableRow key={id}>
                    <TableCell align="left">{accountName}</TableCell>
                    <TableCell align="left">{firstName + lastName}</TableCell>
                    <TableCell align="left">{classCode}</TableCell>
                    <TableCell align="left">{contactType}</TableCell>
                    <TableCell align="left">{dob.slice(0, 10)}</TableCell>
                    <TableCell align="left">{jobRank}</TableCell>
                    <TableCell align="left">{onBoard.slice(0, 10)}</TableCell>
                    <TableCell align="left">
                      <select id="standard-select" onChange={changeStatus}>
                        <option value="DropOut">Drop out</option>
                        <option value="Active">Active</option>
                        <option value="Failed">Failed</option>
                      </select>
                    </TableCell>
                    <TableCell align="left">{skill}</TableCell>
                    <TableCell align="left">{phone}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      ) : (
        <span>Class has no freshers</span>
      )}
    </div>
  );
}
