import React from "react";
import {Link} from "react-router-dom";

class NavItem extends React.Component {

    render() {
        return (
            <>
                <li className={`${this.props.active ? "active" : ""} nav-item`} onClick={this.props.onClick} data-order={this.props.order}>
                    <Link to={this.props.link} className="nav-link" data-order={this.props.order}>
                        <i className={this.props.iconClassesName} />
                        <span data-order={this.props.order}>{this.props.text}</span>
                    </Link>
                </li>
                <hr className="sidebar-divider my-0" />
            </>
        );
    }
}

export default NavItem;