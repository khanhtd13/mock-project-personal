import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Dialog,
  Typography,
} from "@mui/material";
import { Grid } from "@mui/material";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";
import { useEffect } from "react";
import { useState } from "react";
import { AddNewFeedbackQuestion } from "../../components/feedback/createQuestionDialog";
import axios from "axios";
import { useParams } from "react-router-dom";

export const UpdateFeedback = (props) => {
  const [open, setOpen] = useState(false);
  const [feedback, setFeedback] = useState(props.feedback);
  const [questions, setQuestions] = useState(props.questions ?? []);
  const { id } = useParams();

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    axios
      .get(`https://localhost:5001/api/Feedback/GetFeedbackById?id=${id}`)
      .then((response) => {
        setFeedback(response.data);
      });
  }, []);

  useEffect(() => {
    axios
      .get(
        `https://localhost:5001/api/Feedback/SearchFeedbackQuestion?FeedBackId=${id}&PageSize=100`
      )
      .then((response) => {
        setQuestions(response.data.items);
      });
  }, []);

  const addQuestion = (param) => {
    let newQuestions = questions;
    newQuestions.push(param);
    setQuestions(newQuestions);
  };

  const removeQuestion = (param) => {
    setQuestions([
      ...questions.slice(0, param),
      ...questions.slice(param + 1, questions.length),
    ]);
  };

  const onCreateNewFeedbackClick = () => {
    console.log(questions);
  };

  return (
    <>
      <Box sx={{ p: 3 }}>
        <Card>
          <CardHeader title="Update feedback information" />
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  id="title"
                  label="Title"
                  value={feedback?.title}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  id="description"
                  value={feedback?.description}
                  label="Description"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  id="content"
                  value={feedback?.content}
                  label="Content"
                  variant="outlined"
                />
              </Grid>

              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  type="datetime-local"
                  id="date_start"
                  value={feedback?.startDate}
                  label="Date Start"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  type="datetime-local"
                  id="date_end"
                  value={feedback?.endDate}
                  label="Date End"
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                sm={12}
                display="flex"
                justifyContent="end"
                margin={"10px"}
              >
                <Button variant="contained" onClick={handleClickOpen}>
                  Add Question
                </Button>
              </Grid>
            </Grid>

            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                sm={12}
                display="flex"
                justifyContent="end"
                margin={"10px"}
              >
                <Button
                  variant="contained"
                  style={{marginRight: "10px"}}
                  onClick={onCreateNewFeedbackClick}
                >
                  Back
                </Button>
                <Button variant="contained" onClick={onCreateNewFeedbackClick}>
                  Update
                </Button>
              </Grid>
            </Grid>
            {questions?.map((question, index) => {
              return (
                <Box key={index} margin="5px" padding="5px">
                  <Card>
                    <CardHeader title={`Question ${index + 1}`} />
                    <CardContent>
                      <Grid container spacing={2}>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          display="flex"
                          margin={"10px"}
                        >
                          <Typography fontWeight={600}>Title:</Typography>
                          <Typography>{question.title}</Typography>
                        </Grid>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          display="flex"
                          margin={"10px"}
                        >
                          <Typography fontWeight={600}>Description:</Typography>
                          <Typography>{question.description}</Typography>
                        </Grid>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          display="flex"
                          margin={"10px"}
                        >
                          <Typography fontWeight={600}>Content:</Typography>
                          <Typography>{question.content}</Typography>
                        </Grid>
                      </Grid>
                      <Button
                        style={{
                          float: "right",
                          paading: "15px",
                          margin: "5px",
                          marginRight: "10px",
                        }}
                        onClick={() => removeQuestion(index)}
                      >
                        Update
                      </Button>
                      <Button
                        style={{
                          float: "right",
                          paading: "15px",
                          margin: "5px",
                        }}
                        onClick={() => removeQuestion(index)}
                      >
                        Remove
                      </Button>
                    </CardContent>
                  </Card>
                </Box>
              );
            })}
          </CardContent>
        </Card>
      </Box>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <AddNewFeedbackQuestion
          handleClose={handleClose}
          addQuestion={addQuestion}
        />
      </Dialog>
    </>
  );
};
