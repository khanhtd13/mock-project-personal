import { Box, Card, CardContent, CardHeader } from "@mui/material";
import { Grid } from "@mui/material";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";
import { useState } from "react";

export const AddNewFeedbackQuestion = (props) => {
  const { handleClose, addQuestion } = props;

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [content, setContent] = useState('');

  const onTitleChange = (e) => {
    setTitle(e.target.value);
  }
  const onDescriptionChange = (e) => {
    setDescription(e.target.value);
  }
  const onContentChange = (e) => {
    setContent(e.target.value);
  }
  const onAddClick = () => {
    let question = {title, description, content}
    addQuestion(question);
    handleClose();
  }

  return (
    <Box sx={{ p: 3 }}>
      <Card>
        <CardHeader title="Create new Question" />
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                id="title"
                label="Title"
                variant="outlined"
                defaultValue={title}
                onChange={onTitleChange}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                id="description"
                label="Description"
                variant="outlined"
                defaultValue={description}
                onChange={onDescriptionChange}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                id="content"
                label="Content"
                variant="outlined"
                defaultValue={content}
                onChange={onContentChange}
              />
            </Grid>
          </Grid>
          <Grid
            container
            spacing={2}
            marginTop="10px"
            marginRight="10px"
            display="flex"
            justifyContent="end"
          >
            <Grid item xs={12} sm={10} display="flex" justifyContent="end">
              <Button variant="contained" onClick={handleClose}>
                Close
              </Button>
            </Grid>
            <Grid item xs={12} sm={2} display="flex" justifyContent="end">
              <Button variant="contained" onClick={onAddClick}>Add</Button>
            </Grid>
          </Grid>
          <Grid
            container
            spacing={2}
            marginTop="10px"
            marginRight="10px"
            display="flex"
            justifyContent="end"
          ></Grid>
        </CardContent>
      </Card>
    </Box>
  );
};
