import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Dialog,
  Typography,
} from "@mui/material";
import { Grid } from "@mui/material";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";
import { useState } from "react";
import { AddNewFeedbackQuestion } from "../../components/feedback/createQuestionDialog";

export const AddNewFeedback = () => {
  const [open, setOpen] = useState(false);
  const [questions, setQuestions] = useState([]);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const addQuestion = (param) => {
    let newQuestions = questions;
    newQuestions.push(param);
    setQuestions(newQuestions);
  };

  const removeQuestion = (param) => {
    setQuestions([
      ...questions.slice(0, param),
      ...questions.slice(param + 1, questions.length),
    ]);
  };

  const onCreateNewFeedbackClick = () => {
    console.log(questions);
  };

  return (
    <>
      <Box sx={{ p: 3 }}>
        <Card>
          <CardHeader title="Create new feedback" />
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  id="title"
                  label="Title"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  id="description"
                  label="Description"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  id="Content"
                  label="content"
                  variant="outlined"
                />
              </Grid>

              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  type="datetime-local"
                  id="date_start"
                  label="Date Start"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  type="datetime-local"
                  id="date_end"
                  label="Date End"
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                sm={12}
                display="flex"
                justifyContent="end"
                margin={"10px"}
              >
                <Button variant="contained" onClick={handleClickOpen}>
                  Add Question
                </Button>
              </Grid>
            </Grid>

            <Grid container spacing={2}>
              <Grid
                item
                xs={12}
                sm={12}
                display="flex"
                justifyContent="end"
                margin={"10px"}
              >
                <Button variant="contained" onClick={onCreateNewFeedbackClick}>
                  Create New Feedback
                </Button>
              </Grid>
            </Grid>
            {questions?.map((question, index) => {
              return (
                <Box key={index} margin="5px" padding="5px">
                  <Card>
                    <CardHeader title={`Question ${index + 1}`} />
                    <CardContent>
                      <Grid container spacing={2}>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          display="flex"
                          margin={"10px"}
                        >
                          <Typography fontWeight={600}>Title:</Typography>
                          <Typography >{question.title}</Typography>
                        </Grid>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          display="flex"
                          margin={"10px"}
                        >
                          <Typography fontWeight={600}>Description:</Typography>
                          <Typography>{question.description}</Typography>
                        </Grid>
                        <Grid
                          item
                          xs={12}
                          sm={12}
                          display="flex"
                          margin={"10px"}
                        >
                          <Typography fontWeight={600}>Content:</Typography>
                          <Typography >{question.content}</Typography>
                        </Grid>
                      </Grid>
                      <Button
                        style={{
                          float: "right",
                          paading: "15px",
                          margin: "5px",
                        }}
                        onClick={() => removeQuestion(index)}
                      >
                        Remove
                      </Button>
                    </CardContent>
                  </Card>
                </Box>
              );
            })}
          </CardContent>
        </Card>
      </Box>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <AddNewFeedbackQuestion
          handleClose={handleClose}
          addQuestion={addQuestion}
        />
      </Dialog>
    </>
  );
};
