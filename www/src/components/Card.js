import React from "react";
import { Link } from 'react-router-dom';
import ActivatedNavItemContext from "../contexts/ActivatedNavItemContext";

class Card extends React.Component {
    static contextType = ActivatedNavItemContext;

    handleCardItemClick = (e) => {
        let order = e.currentTarget.attributes.getNamedItem('data-order').value;
        this.context.setActivatedItem(order);
    }

    render() {
        return (
            <Link to={this.props.linkTo} className="col-xl-3 col-md-6 mb-4" style={{ textDecoration: "none" }} data-order={this.props.order} onClick={this.handleCardItemClick}>
                <div className="card border-left-primary shadow h-100 py-2">
                    <div className="card-body">
                        <div className="row no-gutters align-items-center">
                            <div className="col mr-2">
                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    {this.props.head}
                                </div>
                                <div className="h5 mb-0 font-weight-bold text-gray-800">{this.props.title}</div>
                            </div>
                            <div className="col-auto">
                                <i className={this.props.iconLabelClassesName + " fa-2x text-gray-300"}></i>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        )
    }
}

export default Card;