import React from "react";
import NavItem from "./NavItem";
import Constants from "../Constants";
import ActivatedNavItemContext from "../contexts/ActivatedNavItemContext";
import { Link } from "react-router-dom";

class Sidebar extends React.Component {
  static contextType = ActivatedNavItemContext;

  handleOnNavItemClick = (e) => {
    let order = e.currentTarget.attributes.getNamedItem("data-order").value;
    this.context.setActivatedItem(order);
  };

  render() {
    return (
      <ul
        className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
        id="accordionSidebar"
      >
        <Link
          className="sidebar-brand d-flex align-items-center justify-content-center"
          to="/"
          data-order={0}
          onClick={this.handleOnNavItemClick}
        >
          <div className="sidebar-brand-icon rotate-n-15">
            <i className="fas fa-laugh-wink"></i>
          </div>
          <div className="sidebar-brand-text mx-3">
            Fresher management system <sup>2</sup>
          </div>
        </Link>

        <hr className="sidebar-divider my-0" />
        {Constants.Routes.map((item, index) => {
          return (
            <NavItem
              key={item.text}
              text={item.text}
              iconClassesName={item.iconClassesName}
              link={item.link}
              active={index === this.props.activatedItem}
              onClick={this.handleOnNavItemClick}
              order={index}
            />
          );
        })}
      </ul>
    );
  }
}

export default Sidebar;
