import React, { useState, useEffect } from "react";
import axios from "axios";
import DataTableClassFresher from "../../components/ClassFresher/DataTableClassFresher";
import { URL_API } from "../../contexts/Const";
import { useHistory } from "react-router-dom";

export default function ClassFresherPage() {
  const [listClass, setListClass] = useState();
  const [page, setPage] = useState(0);
  useEffect(() => {
    let isActive = false;
    if (!isActive) {
      axios
        .get(
          `${URL_API}ClassFresher/GetAllClassFresherPagingsion?pageIndex=${page}&pageSize=10`
        )
        .then((res) => {
          setListClass(res.data.items);
        });
    }
    return () => {
      isActive = true;
    };
  }, [page]);

  const GotoNextPage = () => {
    if (page !== 0) {
      let next = page + 1;
      setPage(next);
    }
  };

  const history = useHistory();
  const GotoImport = (e) => {
    history.push(`classes/import`);
  };

  return (
    <div className="class-fresher">
      <div className="group-btn-classfresher">
        <button onClick={GotoImport} className="btn-classfresher">
          Import Class
        </button>
        <button className="btn-classfresher">Create Class</button>
      </div>
      {listClass !== undefined ? (
        <DataTableClassFresher classFresher={listClass}></DataTableClassFresher>
      ) : (
        <div>No class</div>
      )}
      <div className="group-btn-classfresher">
        <button className="btn-classfresher">Previous</button>
        <button onClick={GotoNextPage} className="btn-classfresher">
          Next
        </button>
      </div>
    </div>
  );
}
