import React, { useState, useEffect } from "react";
import axios from "axios";
import { useParams, useHistory } from "react-router-dom";
import { URL_API } from "../../contexts/Const";
import DataTableClassFresher from "../../components/ClassFresher/DataTableDetailClass";
import { Grid, Box } from "@mui/material";

export default function DetailClassFresher() {
  let { id } = useParams();
  const [classFresher, setClassFresher] = useState();

  const history = useHistory();
  const GoToListClass = () => {
    history.push(`/classes`);
  };
  useEffect(() => {
    axios
      .get(`${URL_API}ClassFresher/GetClassHasFresherById/${id}/hasfreshers`)
      .then((data) => {
        setClassFresher(data.data);
      });
  }, []);

  return (
    <div className="class-fresher">
      <button onClick={GoToListClass} className="btn-classfresher">
        Back to list class
      </button>
      {classFresher !== undefined ? (
        <div>
          <Box>
            <h4>Class {classFresher.classCode}</h4>
            <div className="detail-class">
              <Grid
                container
                xs={12}
                style={{ backgroundColor: "whitesmoke", color: "black" }}
              >
                <Grid item xs={4}>
                  <span>Class Name: {classFresher.className}</span>
                </Grid>
                <Grid item xs={4}>
                  <span>Class Code: {classFresher.classCode}</span>
                </Grid>
                <Grid item xs={4}>
                  <span>
                    Status : {classFresher.isDone ? "Done" : "Process"}
                  </span>
                </Grid>
              </Grid>
            </div>
            <div className="detail-class">
              <Grid
                container
                xs={12}
                style={{ backgroundColor: "whitesmoke", color: "black" }}
              >
                <Grid item xs={4}>
                  <span>Location: {classFresher.location}</span>
                </Grid>
                <Grid item xs={4}>
                  <span>Start Date: {classFresher.startDate}</span>
                </Grid>
                <Grid item xs={4}>
                  <span>End Date : {classFresher.endDate}</span>
                </Grid>
              </Grid>
            </div>
            <div className="detail-class">
              <Grid
                container
                xs={12}
                style={{ backgroundColor: "whitesmoke", color: "black" }}
              >
                <Grid item xs={4}>
                  <span>Plan ID: {classFresher.planId}</span>
                </Grid>
                <Grid item xs={4}>
                  <span>Budget: {classFresher.budget}</span>
                </Grid>
                <Grid item xs={4}></Grid>
              </Grid>
            </div>
          </Box>
          <Box>
            <h4>Admin</h4>
            <Grid
              container
              xs={12}
              style={{ backgroundColor: "whitesmoke", color: "black" }}
            >
              <Grid item xs={2}>
                <div className="detail-class-admin-trainer">
                  <span>Name: {classFresher.nameAdmin1}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Name: {classFresher.nameAdmin2}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Name: {classFresher.nameAdmin3}</span>
                </div>
              </Grid>
              <Grid item xs={10}>
                <div className="detail-class-admin-trainer">
                  <span>Email : {classFresher.emailAdmin1}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Email : {classFresher.emailAdmin2}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Email : {classFresher.emailAdmin3}</span>
                </div>
              </Grid>
            </Grid>
          </Box>
          <Box>
            <h4>Trainer</h4>
            <Grid
              container
              xs={12}
              style={{ backgroundColor: "whitesmoke", color: "black" }}
            >
              <Grid item xs={2}>
                <div className="detail-class-admin-trainer">
                  <span>Name: {classFresher.nameTrainer1}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Name: {classFresher.nameTrainer2}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Name: {classFresher.nameTrainer2}</span>
                </div>
              </Grid>
              <Grid item xs={10}>
                <div className="detail-class-admin-trainer">
                  <span>Email : {classFresher.emailTrainer1}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Email : {classFresher.emailTrainer1}</span>
                </div>
                <div className="detail-class-admin-trainer">
                  <span>Email : {classFresher.emailAdmin3}</span>
                </div>
              </Grid>
            </Grid>
          </Box>
          <h4>List freshers</h4>
          <DataTableClassFresher
            freshers={classFresher.freshers}
          ></DataTableClassFresher>
        </div>
      ) : (
        <div>Something was wrong</div>
      )}
    </div>
  );
}
