import React, { useState } from "react";
import { URL_API } from "../../contexts/Const";
import DataTableImportClass from "../../components/ClassFresher/DataTableImportClass";
import LoadingScreen from "react-loading-screen";

export default function ImportClassFresherPage() {
  const [selectedFile, setSelectedFile] = useState();
  const [classFresher, setClassFresher] = useState();
  const [hasFile, setHasFile] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmission = () => {
    if (hasFile) {
      setIsLoading(true);
      const formData = new FormData();
      formData.append("fileExcel", selectedFile);
      fetch(`${URL_API}ClassFresher/CreateClassFresherFromImportFile/import`, {
        method: "POST",
        body: formData,
      })
        .then((response) => response.json())
        .then((result) => {
          setTimeout(() => {
            setClassFresher(result);
            setIsLoading(false);
            console.log(result);
          }, 1000);
        });
    }
  };

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setHasFile(true);
  };

  return (
    <div className="class-fresher">
      <LoadingScreen
        loading={isLoading}
        bgColor="#f1f1f1"
        spinnerColor="#9ee5f8"
        textColor="#676767"
        text="Loading"
        opacity="0.8"
      >
        <div>
          <input
            id="fusk"
            type="file"
            name="file"
            onChange={changeHandler}
            style={{ color: "black", fontSize: "15px" }}
          />
          <p style={{ color: "black" }}>Select a excel file to import class</p>
          <div>
            <button className="btn-classfresher" onClick={handleSubmission}>
              Import Class
            </button>
          </div>
        </div>
        {classFresher !== undefined ? (
          <div className="container-import-class">
            {classFresher.map((fresher) => {
              return (
                <DataTableImportClass
                  key={fresher.rrCode}
                  classFresher={fresher}
                ></DataTableImportClass>
              );
            })}
          </div>
        ) : null}
      </LoadingScreen>
    </div>
  );
}
