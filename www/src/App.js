import Empty from "./components/Empty";

import Sidebar from "./components/Sidebar";
import Content from "./route/Home";
import FeedbackPage from './routes/feedback'; 
// import PlanData from './components/Plan/Index';
// import page class fresher
import ClassFresher from "./pages/ClassFresher";
import UpdateClassFresher from "./pages/ClassFresher/UpdateClassFresherPage";
import ImportClassFresher from "./pages/ClassFresher/ImportClassFresherPage";
import CreateClassFresher from "./pages/ClassFresher/CreateClassFresherPage";
import DetailClassFresher from "./pages/ClassFresher/DetailClassFresher";
import { AddNewFeedback} from './components/feedback/addNewFeedback';
import { FeedbackDetails } from './components/feedback/feedbackDetail';
import { UpdateFeedback } from './components/feedback/updateFeedback';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React from "react";

import ActivatedNavItemContext from "./contexts/ActivatedNavItemContext";
import FresherReport from './routes/fresherReport';
import Contact from './routes/contact';
import About from './routes/about';
import NotFound from './routes/not-found';
import DataTable from './components/DataTable'

class App extends React.Component {
  state = {
    // the first item in nav bar should be default activated item
    activatedItem: 0,
    setActivatedItem: (item) => {
      this.setState({
        activatedItem: item,
      });
    },
  };

    render() {
        return (
            <ActivatedNavItemContext.Provider value={this.state}>
                <Router>
                    <Sidebar activatedItem={this.state.activatedItem} />
                    <Switch>
                        <Route path="/" exact component={Content} />
                        <Route path="/attendances" component={Empty} />
                        <Route path="/scores" component={Empty} />
                        <Route path="/classes" component={Empty} />
            <Route path="/classes/detail/:id" component={DetailClassFresher} />
            <Route path="/classes/update/:id" component={UpdateClassFresher} />
            <Route path="/classes/import" component={ImportClassFresher} />
            <Route path="/classes/create" component={CreateClassFresher} />
            <Route path="/freshers" component={Empty} />
            <Route path="/audit" component={Empty} />
                        <Route path="/plan" component={PlanData} />
                        <Route path='/contact' component={Contact} />
                        <Route path='/about' component={About} />
                        <Route path='/fresher-report' component={FresherReport} />
                        <Route path='/test' component={DataTable} />
                        <Route path="/feedback/create" component={AddNewFeedback} />
                        <Route path="/feedback/update/:id/" component={UpdateFeedback} />
                        <Route path="/feedback/:id/" component={FeedbackDetails} />
                        <Route path="/feedback" component={FeedbackPage} />
                        <Route component={NotFound} />
                    </Switch>
                </Router>
            </ActivatedNavItemContext.Provider>
        )
    }
}

export default App;
