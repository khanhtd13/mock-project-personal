const ApplicationConstants = {
    Routes: [
        {
            text: "Home",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/"
        },
        {
            text: "Attendances",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/attendances"
        },
        {
            text: "Scores",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/scores"
        },
        {
            text: "Classes",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/classes"
        },
        {
            text: "Freshers",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/freshers"
        },
        {
            text: "Audit",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/audit"
        },
        {
            text: "Plan",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/plan"
        },
        {
            text: "Monthly/Weekly Report",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/fresher-report"
        },
        {
            text: "Feedback",
            iconClassesName: "fas fa-fw fa-tachometer-alt",
            link: "/feedback"
        }
    ]
}

export default ApplicationConstants;
