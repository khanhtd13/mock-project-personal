﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Settings;
using Global.Shared.Settings.Mail;
using Global.Shared.Settings.Reminder;
using Global.Shared.Settings;
using Global.Shared.Settings.Mail;
using Global.Shared.Settings.Reminder;
using Global.Shared.ViewModels.ReminderViewModels;
using Moq;
using System.Net.Mail;

namespace Application.Tests.Services
{
    public class ReminderServiceTests : SetupTest
    {
        private readonly IReminderService _reminderService;
        private readonly RootSetting _rootSetting;
        private readonly ReminderSettings _reminderSettings;
        public ReminderServiceTests()
        {
            _rootSetting = new RootSetting()
            {
                SmtpClientSetting = new SmtpClientSetting
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = true,
                    Host = "smtp-mail.outlook.com",
                    Port = 587,
                    UseDefaultCredentials = false
                }
            };
            _reminderSettings = new ReminderSettings
            {
                AuditReminderTime = new AuditReminderTime()
            };

            _reminderService = new ReminderService(_unitOfWorkMock.Object, _mapperConfig, _rootSetting, _reminderSettings);
        }

        [Fact]
        public async Task CreateReminderAsync_ShouldCreateSucceededWhenParameterTrue()
        {
            //arrange
            var mocks = _fixture.Build<CreateReminderViewModel>().Create();

            _unitOfWorkMock.Setup(x => x.ReminderRepository.AddAsync(It.IsAny<Reminder>()))
                .Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(1);
            //act
            var result = await _reminderService.CreateReminderAsync(mocks);

            //assert
            _unitOfWorkMock.Verify(
                x => x.ReminderRepository.AddAsync(It.IsAny<Reminder>()), Times.Once());

            _unitOfWorkMock.Verify(x => x.SaveChangeAsync(), Times.Once());
            result.Should().BeTrue();
        }

        [Fact]
        public async Task CreateReminderAsync_ShouldCreateFailedWhenParameterFalse()
        {
            //arrange
            var mocks = _fixture.Build<CreateReminderViewModel>().Create();

            _unitOfWorkMock.Setup(
                x => x.ReminderRepository.AddAsync(It.IsAny<Reminder>())).Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(0);

            //act
            var result = await _reminderService.CreateReminderAsync(mocks);

            //assert
            _unitOfWorkMock.Verify(
                x => x.ReminderRepository.AddAsync(It.IsAny<Reminder>()), Times.Once());

            _unitOfWorkMock.Verify(x => x.SaveChangeAsync(), Times.Once());

            result.Should().BeFalse();

        }
        [Fact]
        public async Task GetAllReminderByDateAsync_ShouldReturnNotNull()
        {
            //arrange
            var mocks = _fixture.Build<Reminder>().CreateMany(100).ToList();
            var expectedResult = _mapperConfig.Map<List<ReminderViewModel>>(mocks);
            var dateTime = _currentTimeMock.Object.GetCurrentTime();

            _unitOfWorkMock
                .Setup(x => x.ReminderRepository.GetAllReminderByDateAsync(dateTime))
                .ReturnsAsync(mocks);

            //act
            var result = await _reminderService.GetAllReminderByDateAsync(dateTime);

            //assert
            _unitOfWorkMock
                .Verify(x => x.ReminderRepository.GetAllReminderByDateAsync(dateTime), Times.Once);
            result.Should().BeEquivalentTo(expectedResult);
        }
        public async Task GetAllReminderByDateAsync_ShouldReturnNull()
        {
            //arrange
            var mocks = _fixture.Build<Reminder>().CreateMany(0).ToList();
            List<Reminder>? expectedResult = null ;
            
            var dateTime = _currentTimeMock.Object.GetCurrentTime();

            _unitOfWorkMock
                .Setup(x => x.ReminderRepository.GetAllReminderByDateAsync(dateTime))
                .ReturnsAsync(mocks);

            //act
            var result = await _reminderService.GetAllReminderByDateAsync(dateTime);

            //assert
            _unitOfWorkMock
                .Verify(x => x.ReminderRepository.GetAllReminderByDateAsync(dateTime), Times.Once);
            result.Should().BeEquivalentTo(expectedResult);
        }
    }
}
