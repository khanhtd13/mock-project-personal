﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Enums;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Exeption;
using Global.Shared.ViewModels;
using Moq;

namespace Application.Tests.Services
{
    public class FresherServiceTest : SetupTest
    {
        private readonly IFresherService _fresherService;
        public FresherServiceTest()
        {
            _fresherService = new FresherService(_unitOfWorkMock.Object, _mapperConfig);
        }

        [Fact]
        public async Task GetFresherByIdAsync_ShoudReturnCorrectData()
        {
            //arrange
            var mock = _fixture.Build<Fresher>()
                .Without(x => x.ClassFresher)
                .Create();
            var id = Guid.NewGuid();

            var expectedResult = _mapperConfig.Map<FresherViewModel>(mock);

            _unitOfWorkMock.Setup(x => x.FresherRepository.GetByIdAsync(id))
                .ReturnsAsync(mock);

            //act
            var result = await _fresherService
                .GetFresherByIdAsync(id);

            // assert
            _unitOfWorkMock.Verify(x => x.FresherRepository.GetByIdAsync(id), Times.Once());
            result.Should().BeEquivalentTo(expectedResult);


        }

        [Fact]
        public async Task GetFresherByIdAsync_ShoudReturnNullData()
        {
            //arrange
            var mock = (Fresher?)null;
            Guid id = Guid.NewGuid();
            var expectedResult = _mapperConfig.Map<FresherViewModel>(mock);

            _unitOfWorkMock.Setup(x => x.FresherRepository.GetByIdAsync(id)).Callback(() =>
           throw new AppException($"Fresher with id = {id} not found"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>(async () => await _fresherService.GetFresherByIdAsync(id));
            //assert
            Assert.Equal(ex.Message, $"Fresher with id = {id} not found");
        }

        [Fact]
        public async Task ChangeFresherStatus_shouldReturnCorrectData()
        {
            //arrage
            Guid id = Guid.NewGuid();
            var status = It.IsAny<StatusFresherEnum>();
            var mock = _fixture.Build<Fresher>().Without(x => x.ClassFresher).With(x => x.Id, id).Create();
            mock.Status = status;
            _unitOfWorkMock.Setup(x => x.FresherRepository.GetByIdAsync(id)).ReturnsAsync(mock);
            var expectedResult = _mapperConfig.Map<FresherViewModel>(mock);
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(1);
            //Act
            var result = await _fresherService.ChangeFresherStatus(id, status);
            //Assert
            result.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task ChangeFresherStatus_shouldReturnThrowExceptionWithIdNotFound()
        {
            //arrage
            Guid id = Guid.NewGuid();
            var status = It.IsAny<StatusFresherEnum>();
            var mock = (Fresher?)null;
            _unitOfWorkMock.Setup(x => x.FresherRepository.GetByIdAsync(id)).Callback(() =>
           throw new AppException($"Fresher with id = {id} not found"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>(async () => await _fresherService.ChangeFresherStatus(id, status));
            //Assert
            Assert.Equal(ex.Message, $"Fresher with id = {id} not found");
        }

        [Fact]
        public async Task ChangeFresherStatus_shouldReturnThrowExceptionWithSaveChangeFail()
        {
            //arrage
            Guid id = Guid.NewGuid();
            var status = It.IsAny<StatusFresherEnum>();
            var mock = _fixture.Build<Fresher>().Without(x => x.ClassFresher).With(x => x.Id, id).Create();
            mock.Status = status;
            _unitOfWorkMock.Setup(x => x.FresherRepository.GetByIdAsync(id)).ReturnsAsync(mock);
            var expectedResult = _mapperConfig.Map<FresherViewModel>(mock);
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(0);
            //Act
            var ex = await Assert.ThrowsAsync<AppException>(async () => await _fresherService.ChangeFresherStatus(id, status));
            //Assert
            Assert.Equal(ex.Message, "Change staus for fresher fail!");
        }
    }
}
