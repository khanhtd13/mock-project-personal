﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.ViewModels.PlanViewModels;
using Moq;

namespace Application.Tests.Services
{
    public class PlanServiceTest : SetupTest
    {
        private readonly IPlanService _planService;
        public PlanServiceTest()
        {
            _planService = new PlanService(
                                _unitOfWorkMock.Object,
                                _mapperConfig
                                );
        }
        [Fact]
        public async Task GetAllAsync_ShouldReturn_ViewModel()
        {
            //Arrange
            var expectedResult = _fixture.Build<Pagination<PlanGetViewModel>>()
                                         .Create();
            var mocks = _mapperConfig.Map<Pagination<Plan>>(expectedResult);
            _unitOfWorkMock.Setup(x => x.PlanRepository.FindAsync(null, null, 0, 10))
                           .ReturnsAsync(mocks);
            //Act
            var result = await _planService.GetAllAsync(0,10);
            result.Should().BeEquivalentTo(expectedResult);
        }
        [Fact]
        public async Task GetPlanByIdAsync_ShouldReturn_CorrectData_IfFound()
        {
            //Arrange
            var expectedResult = _fixture.Build<PlanGetByIdViewModel>()
                                        .Create();
            var mock = _mapperConfig.Map<Plan>(expectedResult);
            _unitOfWorkMock.Setup(x => 
                                x.PlanRepository
                           .GetByIdAsync(
                                    It.IsAny<Guid>(),
                                    x=>x.Modules
                                    ))
                           .ReturnsAsync(mock);
            //Act
            var result = await _planService.GetPlanByIdAsync(It.IsAny<Guid>());
            //Assert
            result.Should().BeEquivalentTo(expectedResult);
        }
        [Fact]
        public async Task AddItemPlanAsync_ShouldReturnData_IfSuccess()
        {
            //Arrange
            var expectedResult = _fixture.Build<PlanAddViewModel>()
                                        .Create();
            var mock = _mapperConfig.Map<Plan>(expectedResult);
            _unitOfWorkMock.Setup(x => x.PlanRepository.AddAsync(mock))
                           .Returns(Task.CompletedTask); ;
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync())
                           .ReturnsAsync(1);
            //Act
            var result = await _planService.AddItemPlanAsync(expectedResult);
            //Assert
            _unitOfWorkMock.Verify(x => x.SaveChangeAsync(), Times.Once());
            result.Should().BeEquivalentTo(expectedResult);
        }
        [Fact]
        public async Task UpdatePlanAsync_ShouldReturnNull_IfNotFoundPlan()
        {
            //Arrange
            var expectedResult = _fixture.Build<PlanUpdateViewModel>()
                                        .Create();
            var mock = new Plan();
            mock = null;
            _unitOfWorkMock.Setup(x => x.PlanRepository.GetByIdAsync(It.IsAny<Guid>()))
                           .ReturnsAsync(mock);
            //Act
            var result = await _planService.UpdatePlanAsync(
                                        It.IsAny<Guid>(), expectedResult);
            //Assert
            Assert.Null(result);
        }
        [Fact]
        public async Task UpdatePlanAsync_ShouldReturnData_IfSuccess()
        {
            //Arrange
            var expectedResult = _fixture.Build<PlanUpdateViewModel>()
                                        .Create();
            var mock = _mapperConfig.Map<Plan>(expectedResult);
            _unitOfWorkMock.Setup(x => x.PlanRepository.GetByIdAsync(It.IsAny<Guid>()))
                           .ReturnsAsync(mock);
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync())
                           .ReturnsAsync(1);
            //Act
            var result = await _planService.UpdatePlanAsync(
                                        It.IsAny<Guid>(), expectedResult);
            //Assert
            _unitOfWorkMock.Verify(x => x.SaveChangeAsync(), Times.Once());
            Assert.Equal(expectedResult, result);
        }
    }
}
