﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.ViewModels.ScoreViewModels;
using Moq;

namespace Application.Tests.ServicesTest
{
    public class ScoreServiceTest : SetupTest
    {
        private readonly IScoreService _scoreService;

        public ScoreServiceTest()
        {
            _scoreService = new ScoreService(_unitOfWorkMock.Object, _mapperConfig);
        }
        [Fact]
        public async Task GetAllScoreAsyncTest()
        {
            //arrange
            var mockList = _fixture.Build<Score>().CreateMany(100).Where(e => !e.IsDeleted).ToList();

            _unitOfWorkMock.Setup(e => e.ScoreRepository.GetAllDeleteFalseAsync())
                                                               .ReturnsAsync(mockList);
            var mockViewModelList = _mapperConfig.Map<List<ScoreViewModel>>(mockList);

            //act
            var result = await _scoreService.GetAllScoreAsync();

            //assert
            _unitOfWorkMock.Verify(e => e.ScoreRepository.GetAllDeleteFalseAsync(), Times.Once);
            result.Should().BeEquivalentTo(mockViewModelList);
        }

        [Fact]
        public async Task CreateScoreAsyncTest()
        {
            //arrange
            var mockModelCreate = _fixture.Build<CreateScoreViewModel>().Create();

            _mapperConfig.Map<Score>(mockModelCreate);

            _unitOfWorkMock.Setup(e => e.ScoreRepository.AddAsync(It.IsAny<Score>()))
                                                               .Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(e => e.SaveChangeAsync()).ReturnsAsync(1);

            _mapperConfig.Map<ScoreViewModel>(It.IsAny<Score>());

            //act
            var result = await _scoreService.CreateScoreAsync(mockModelCreate);

            //assert
            _unitOfWorkMock.Verify(e => e.ScoreRepository
                                         .AddAsync(It.IsAny<Score>()), Times.Once);
            _unitOfWorkMock.Verify(e => e.SaveChangeAsync(), Times.Once);
        }

        [Fact]
        public async Task UpdateScoreAsyncTest_FindNull()
        {
            _unitOfWorkMock.Setup(e => e.ScoreRepository.GetByIdAsync(It.IsAny<Guid>()))
                                                               .ReturnsAsync((Score?)null);

            await Assert.ThrowsAsync<Exception>
                 (async () => await _scoreService
                         .UpdateScoreAsync(new UpdateScoreViewModel()));
        }

        [Fact]
        public async Task UpdateScoreAsyncTest_ShouldReturnTrue()
        {
            var mockObj = _fixture.Build<Score>().Create();
            var mockModelUpdate = _fixture.Build<UpdateScoreViewModel>().Create();

            _unitOfWorkMock.Setup(e => e.ScoreRepository
                                        .GetByIdAsync(It.IsAny<Guid>()))
                                        .ReturnsAsync(mockObj);

            _unitOfWorkMock.Setup(e => e.SaveChangeAsync()).ReturnsAsync(1);

            _mapperConfig.Map<ScoreViewModel>(mockObj);

            var result = await _scoreService
                                .UpdateScoreAsync(mockModelUpdate);

            _unitOfWorkMock.Verify(e => e.ScoreRepository.Update(It.IsAny<Score>()));
            _unitOfWorkMock.Verify(e => e.SaveChangeAsync());
        }

        [Fact]
        public async Task UpdateScoreAsyncTest_ShouldReturnFalse()
        {
            var mockObj = new Score();
            var mockModelUpdate = new UpdateScoreViewModel();

            _unitOfWorkMock.Setup(e => e.ScoreRepository
                                        .GetByIdAsync(It.IsAny<Guid>()))
                                        .ReturnsAsync(mockObj);

            _unitOfWorkMock.Setup(e => e.SaveChangeAsync()).ReturnsAsync(0);

            var result = await _scoreService
                                .UpdateScoreAsync(mockModelUpdate);

            Assert.Null(result);
        }

        [Fact]
        public void DeleteScoreAsync()
        {
            var mockObj = new Score();
            _unitOfWorkMock.Setup(e => e.ScoreRepository
                                        .GetByIdAsync(It.IsAny<Guid>()))
                                        .ReturnsAsync(mockObj);

            _unitOfWorkMock.Setup(e => e.SaveChangeAsync()).ReturnsAsync(1);

            var result = _scoreService.DeleteScoreAsync(It.IsAny<Guid>());
            _unitOfWorkMock.Verify(e => e.ScoreRepository.SoftRemove(It.IsAny<Score>()));
            Assert.Equal(Task.CompletedTask, result);
        }

    }
}
