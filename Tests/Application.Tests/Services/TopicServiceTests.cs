﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.ViewModels.TopicViewModels;
using Moq;

namespace Application.Tests.Services
{
    public class TopicServiceTests : SetupTest
    {
        private readonly ITopicService _topicService;

        public TopicServiceTests()
        {
            _topicService = new TopicService(_unitOfWorkMock.Object, _mapperConfig);
        }
        [Fact]
        public async Task GetAllTopic_Test_ShouldReturnCorrentData()
        {
            var mocks = _fixture.Build<Pagination<TopicViewModel>>().Create();
            var topicListModel = _mapperConfig.Map<Pagination<Topic>>(mocks);
            _unitOfWorkMock.Setup(x => x.TopicRepository.FindAsync(null, null, 0, 10))
                           .ReturnsAsync(topicListModel);
            var result = await _topicService.GetAllAsync(0,10);
            result.Should().BeEquivalentTo(mocks);
        }
        [Fact]
        public async Task GetTopicById_Test_ShouldReturnCorrentData()
        {
            var mocks = _fixture.Build<TopicViewModel>().Create();
            var topicModel = _mapperConfig.Map<Topic>(mocks);
            _unitOfWorkMock.Setup(x => x.TopicRepository.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(topicModel);
            var result = await _topicService.GetTopicByIdAsync(It.IsAny<Guid>());
            result.Should().BeEquivalentTo(mocks);
        }
        [Fact]
        public async Task GetTopicByModule_Test_ShouldReturnCorrentData()
        {
            var mocks = _fixture.Build<TopicViewModel>().CreateMany(100).ToList();
            var topicList = _mapperConfig.Map<List<Topic>>(mocks);
            _unitOfWorkMock.Setup(x => x.TopicRepository.GetByModuleId(It.IsAny<Guid>()))
                .ReturnsAsync(topicList);
            var result = await _topicService.GetTopicByModuleIdAsync(It.IsAny<Guid>());
            result.Should().BeEquivalentTo(mocks);
        }
        [Fact]
        public async Task CreateTopic_Test_CompareTwoReturnDataTypes_WhenSuccessSaved()
        {
            var mocks = _fixture.Build<CreateTopicViewModel>().Create();
            var topic = _mapperConfig.Map<Topic>(mocks);
            _unitOfWorkMock.Setup(x => x.ModuleRepository.GetByIdAsync(It.IsAny<Guid>()))
                           .ReturnsAsync(new Module());
            _unitOfWorkMock.Setup(x => x.TopicRepository.GetByModuleId(It.IsAny<Guid>()))
                           .ReturnsAsync(new List<Topic>());
            _unitOfWorkMock.Setup(x => x.TopicRepository.AddAsync(topic));
            var topics = _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(1);
            var result = await _topicService.AddTopicAsync(mocks);
            Assert.Equal(mocks, result);
        }
        [Fact]
        public async Task UpdateTopic_Test_ShouldReturnCorrentData_WhenSuccessSaved()
        {
            //Arrange
            var mocks = _fixture.Build<UpdateTopicViewModel>().Create();
            var topicModel = _mapperConfig.Map<Topic>(mocks);
            _unitOfWorkMock.Setup(x => x.TopicRepository.GetByIdAsync(It.IsAny<Guid>()))
                           .ReturnsAsync(topicModel);
            var result = await _topicService.GetTopicByIdAsync(It.IsAny<Guid>());
            _unitOfWorkMock.Setup(x => x.TopicRepository.Update(topicModel));
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(1);
            //Mock Calculator
            _unitOfWorkMock.Setup(x => x.ModuleRepository.GetByIdAsync(It.IsAny<Guid>()))
                           .ReturnsAsync(new Module());
            _unitOfWorkMock.Setup(x => x.TopicRepository.GetByModuleId(It.IsAny<Guid>()))
                           .ReturnsAsync(new List<Topic>());
            //Act
            var resultUpdate = await _topicService.UpdateTopicAsync(Guid.NewGuid(), mocks);
            //Assert
            resultUpdate.Should().BeEquivalentTo(mocks);
        }
    }
}
