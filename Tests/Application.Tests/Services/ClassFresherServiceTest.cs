﻿using Application.Interfaces;
using Application.Services;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.Exeption;
using Global.Shared.ViewModels;
using Moq;

namespace Application.Tests.Services
{
    public class ClassFresherServiceTest : SetupTest
    {
        private readonly IClassFresherService _classFresherService;
        public ClassFresherServiceTest()
        {
            _classFresherService = new ClassFresherService(_unitOfWorkMock.Object, _mapperConfig, _importDataServiceMock.Object);            
        }

        [Fact]
        public async Task GetClassFresherPagingsionAsync_ShouldReturnCorrectDataWhenDidNotPassTheParameters()
        {
            //arrange
            var mockData = new Pagination<ClassFresher>
            {
                Items = _fixture.Build<ClassFresher>().Without(x => x.Freshers).CreateMany(100).ToList(),
                PageIndex = 0,
                PageSize = 100,
                TotalItemsCount = 100
            };
            var expectedResult = _mapperConfig.Map<Pagination<ClassFresher>>(mockData);

            _unitOfWorkMock.Setup(x => x.ClassFresherRepository.ToPagination(0, 10)).ReturnsAsync(mockData);

            //act
            var result = await _classFresherService.GetAllClassFreshersPagingsionAsync();

            //assert
            _unitOfWorkMock.Verify(x => x.ClassFresherRepository.ToPagination(0, 10), Times.Once());
        }

      
        [Fact]
        public async Task GetClassFresherByIdAsync_ShoudReturnCorrectData()
        {
            //arrange
            var mock = _fixture.Build<ClassFresher>()
                .Without(x => x.Freshers)
                .Create();

            var expectedResult = _mapperConfig.Map<ClassFresherViewModel>(mock);

            _unitOfWorkMock.Setup(x => x.ClassFresherRepository.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(mock);

            //act
            var result = await _classFresherService
                .GetClassFresherByIdAsync(It.IsAny<Guid>());

            // assert
            _unitOfWorkMock.Verify(x => x.ClassFresherRepository.GetByIdAsync(It.IsAny<Guid>()), Times.Once());
            result.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task GetClassFresherByIdAsync_ShoudReturnNullData()
        {
            //arrange
            var mock = (ClassFresher?)null;
            Guid id = Guid.NewGuid();
            var expectedResult = _mapperConfig.Map<ClassFresherViewModel>(mock);

            _unitOfWorkMock.Setup(x => x.ClassFresherRepository.GetByIdAsync(id)).Callback(() =>
           throw new AppException($"CLassFresher with id = {id} not found"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>(async () => await _classFresherService.GetClassFresherByIdAsync(id));
            //assert
            Assert.Equal(ex.Message, $"CLassFresher with id = {id} not found");
        }

        [Fact]
        public async Task UpdateClassFresher_ShouldReturnTrueIfUpdateSuccess()
        {
            //arrange 
            var mock = _fixture.Build<ClassFresherViewModel>().Create();

            var expectedResult = _mapperConfig.Map<ClassFresher>(mock);

            _unitOfWorkMock
                .Setup(x => x.ClassFresherRepository.Update(expectedResult));
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(1);

            //act
            var result = await _classFresherService.UpdateClassFresher(mock);

            //assert
            _unitOfWorkMock
                .Verify(
                    x => x.ClassFresherRepository.Update(It.IsAny<ClassFresher>()),
                    Times.Once());
            _unitOfWorkMock
                .Verify(x => x.SaveChangeAsync(), Times.Once());
        }

        [Fact]
        public async Task UpdateClassFresher_ShouldReturnFalseIfUpdateFail()
        {
            //arrange 
            var mock = _fixture.Build<ClassFresherViewModel>().Create();

            var expectedResult = _mapperConfig.Map<ClassFresher>(mock);

            _unitOfWorkMock
                .Setup(x => x.ClassFresherRepository.Update(expectedResult));
            _unitOfWorkMock.Setup(x => x.SaveChangeAsync()).ReturnsAsync(0);

            //act
            var result = await Assert
                            .ThrowsAsync<AppException>(
                                () => _classFresherService.UpdateClassFresher(mock));

            //assert
            _unitOfWorkMock
                .Verify(
                    x => x.ClassFresherRepository.Update(It.IsAny<ClassFresher>()),
                    Times.Once());
            _unitOfWorkMock
                .Verify(x => x.SaveChangeAsync(), Times.Once());

            result.Message.Should().BeEquivalentTo("Update class fail!");
        }

        [Fact]
        public async Task GetFresherByClassCode_ShouldReturnCorrectData()
        {
            //arrange
            var mocks = _fixture.Build<Fresher>().Without(x => x.ClassFresher)
                .CreateMany(10)
                .ToList();
            var expectedResult = _mapperConfig.Map<List<FresherViewModel>>(mocks);

            _unitOfWorkMock
                .Setup(x => x.FresherRepository
                .GetFresherByClassCodeAsync("HCM22_FR_NET_05"))
                .ReturnsAsync(mocks);

            //act
            var results = await _classFresherService
                .GetFreshersByClassCodeAsync("HCM22_FR_NET_05");

            //assert
            _unitOfWorkMock.Verify(
                x => x.FresherRepository.GetFresherByClassCodeAsync("HCM22_FR_NET_05")
                , Times.Once());
            results.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task GetFresherByClassCode_ShouldReturnAllClassIfKeywordIsEmpty()
        {
            //arrange
            var mocks = _fixture.Build<Fresher>().Without(x => x.ClassFresher)
                .CreateMany(10)
                .ToList();
            var expectedResult = _mapperConfig.Map<List<FresherViewModel>>(mocks);

            _unitOfWorkMock
                .Setup(x => x.FresherRepository
                .GetFresherByClassCodeAsync(""))
                .ReturnsAsync(mocks);

            //act
            var results = await _classFresherService
                .GetFreshersByClassCodeAsync("");

            //assert
            _unitOfWorkMock.Verify(
                x => x.FresherRepository.GetFresherByClassCodeAsync("")
                , Times.Once());
            results.Should().BeEquivalentTo(expectedResult);
        }
    }
}
