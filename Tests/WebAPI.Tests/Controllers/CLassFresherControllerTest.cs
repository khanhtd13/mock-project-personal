﻿using AutoFixture;
using Domain.Entities;
using Domain.Enums;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.Exeption;
using Global.Shared.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using WebAPI.Controllers;

namespace WebAPI.Tests.Controllers
{
    public class CLassFresherControllerTest : SetupTest
    {
        private readonly ClassFresherController _cLassFresherController;
        public CLassFresherControllerTest()
        {
            _cLassFresherController = new ClassFresherController(
                                            _classFresherServiceMock.Object,
                                            _fresherServiceMock.Object);
        }

        [Fact]
        public async Task CreateClassFresher_ShouldReturnCorrectData()
        {
            var mockModelRequest = _fixture.Build<CreateClassFresherViewModel>()
                            .Without(x => x.Freshers).Create();

            var mockModelResponse = _fixture.Build<ClassFresherViewModel>().Create();
            // arrange
            _classFresherServiceMock.Setup(
                x => x.CreateClassFresherAsync(It.IsAny<CreateClassFresherViewModel>()))
                        .ReturnsAsync(mockModelResponse);
            //act
            var result = await _cLassFresherController.UpdateClassFresherAsync(mockModelRequest);
            //assert
            _classFresherServiceMock.Verify(
              x => x.CreateClassFresherAsync(It.Is<CreateClassFresherViewModel>(
                  x => x.Equals(mockModelRequest))), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }

        [Fact]
        public async Task GetCLassFreshertById_ShouldReturnCorrectData_IfSuccess()
        {
            //arrage
            Guid id = Guid.NewGuid();
            _classFresherServiceMock.Setup(x => x.GetClassFresherByIdAsync(id))
                .ReturnsAsync(new ClassFresherViewModel { Id = id });
            //Act
            var result = await _cLassFresherController.GetClassFresherByIdAsync(id);
            //assert
            _classFresherServiceMock.Verify(
               x => x.GetClassFresherByIdAsync(id), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }


        [Fact]
        public async Task GetCLassFreshertById_ShouldReturnThrowException()
        {
            //arrage
            Guid id = Guid.NewGuid();
            _classFresherServiceMock.Setup(x => x.GetClassFresherByIdAsync
            (It.IsAny<Guid>())).Callback(() =>
            throw new AppException($"CLassFresher with id = {id} not found"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>
                (async () => await _cLassFresherController.GetClassFresherByIdAsync(id));
            //assert
            Assert.Equal(ex.Message, $"CLassFresher with id = {id} not found");
        }

        [Fact]
        public async Task GetAllClassFresherPagingsion_ShouldReturnCorrectData()
        {
            var mocks = _fixture.Build<Pagination<ClassFresherViewModel>>().Create();
            // arrange
            _classFresherServiceMock.Setup(
                x => x.GetAllClassFreshersPagingsionAsync(0, 10)).ReturnsAsync(mocks);
            // act
            var result = await _cLassFresherController.GetAllClassFresherPagingsionAsync(0, 10);

            // assert
            _classFresherServiceMock.Verify(
                x => x.GetAllClassFreshersPagingsionAsync(0, 10), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }

        [Fact]
        public async Task GetAllClassFresherPagingsion_ShouldReturnThrowException()
        {
            // arrange
            _classFresherServiceMock.Setup(x => x.GetAllClassFreshersPagingsionAsync
             (0, 10)).Callback(() =>
             throw new AppException("CLassFresher is null"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>
                (async () => await _cLassFresherController.GetAllClassFresherPagingsionAsync(0, 10));
            //assert
            Assert.Equal(ex.Message, "CLassFresher is null");
        }

        [Fact]
        public async Task GetFresherByClassCode_ShouldReturnCorrectData()
        {
            var mocks = _fixture.Build<FresherViewModel>().CreateMany(10).ToList();
            var classCode = It.IsAny<string>();
            // arrange
            _classFresherServiceMock.Setup(
                x => x.GetFreshersByClassCodeAsync(classCode)).ReturnsAsync(mocks);

            // act
            var result = await _cLassFresherController.GetFresherByClassCodeAsync(classCode);

            // assert
            _classFresherServiceMock.Verify(
                x => x.GetFreshersByClassCodeAsync(classCode), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }

        [Fact]
        public async Task GetFresherByClassCode_ShouldReturnThrowException()
        {
            // arrange
            //var mocks = _fixture.Build<FresherViewModel>().CreateMany(10).ToList();
            var classCode = It.IsAny<string>();
            _classFresherServiceMock.Setup(x => x.GetFreshersByClassCodeAsync
             (classCode)).Callback(() =>
             throw new AppException("List Fresher not found"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>
                (async () => await _cLassFresherController.GetFresherByClassCodeAsync(classCode));
            //assert
            Assert.Equal(ex.Message, "List Fresher not found");
        }

        [Fact]
        public async Task GetFreshertById_ShouldReturnCorrectData_IfSuccess()
        {
            //arrage
            Guid id = Guid.NewGuid();
            _fresherServiceMock.Setup(x => x.GetFresherByIdAsync(id))
                .ReturnsAsync(new FresherViewModel { Id = id });
            //Act
            var result = await _cLassFresherController.GetFresherByIdAsync(id);
            //assert
            //result.Id.Should().Be(id);
            _fresherServiceMock.Verify(
               x => x.GetFresherByIdAsync(id), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }


        [Fact]
        public async Task GetFreshertById_ShouldReturnThrowException()
        {
            //arrage
            Guid id = Guid.NewGuid();
            _fresherServiceMock.Setup(x => x.GetFresherByIdAsync
            (It.IsAny<Guid>())).Callback(() =>
            throw new AppException($"Fresher with id = {id} not found"));
            //Act
            var ex = await Assert.ThrowsAsync<AppException>
                (async () => await _cLassFresherController.GetFresherByIdAsync(id));
            //assert
            Assert.Equal(ex.Message, $"Fresher with id = {id} not found");
        }

        [Fact]
        public async Task ChangStatusFresher_ShouldReturnThrowException()
        {
            //arrage
            Guid id = Guid.NewGuid();
            var status = It.IsAny<StatusFresherEnum>();
            _fresherServiceMock.Setup(x => x.ChangeFresherStatus(id, status)).Callback(() =>
                throw new AppException("Changstaus for fresher fail!"));
            //act
            var ex = await Assert.ThrowsAsync<AppException>
                (async () => await _cLassFresherController.ChangStatusFresherAsync(id, status));
            //assert
            Assert.Equal(ex.Message, "Changstaus for fresher fail!");
        }

        [Fact]
        public async Task ChangStatusFresher_ShouldReturnCorrectData()
        {
            //arrage
            Guid id = Guid.NewGuid();
            var status = It.IsAny<StatusFresherEnum>();
            var mock = _fixture.Build<FresherViewModel>().Create();
            _fresherServiceMock.Setup(x => x.ChangeFresherStatus(id, status)).ReturnsAsync(mock);

            //act
            var result = await _cLassFresherController.ChangStatusFresherAsync(id, status);
            //assert
            _fresherServiceMock.Verify(
               x => x.ChangeFresherStatus(id, status), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }

        [Fact]
        public async Task CreateClassFresherFromImportFile_ShouldReturnCorrectData()
        {
            //arrage
            var mocks = _fixture.Build<ClassFresherViewModel>().Without(x => x.Freshers).CreateMany(10).ToList();

            _classFresherServiceMock.Setup(
               x => x.CreateCLassFresherFromImportedExcelFile(It.IsAny<IFormFile>())).ReturnsAsync(mocks);
            // act
            var result =await _cLassFresherController.CreateClassFresherFromImportedFileAsync(It.IsAny<IFormFile>());

            // assert
            _classFresherServiceMock.Verify(
                x => x.CreateCLassFresherFromImportedExcelFile(
                    It.IsAny<IFormFile>()), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();

        }

        [Fact]
        public async Task CreateClassFresherFromImportFile_ShouldReturnThrowException()
        {
            _classFresherServiceMock.Setup(
               x => x.CreateCLassFresherFromImportedExcelFile(It.IsAny<IFormFile>())).Callback(() =>
                   throw new AppException("Import fail!"));

            // act
            var ex = await Assert.ThrowsAsync<AppException>(async () =>
                           await _cLassFresherController.CreateClassFresherFromImportedFileAsync(It.IsAny<IFormFile>()));
            //assert
            Assert.Equal(ex.Message, "Import fail!");
        }

        [Fact]
        public async Task UpdateClassFresher_ShouldReturnCorrectData()
        {
            var mockModelRequest = _fixture.Build<ClassFresherViewModel>()
                           .Without(x => x.Freshers).Create();
            var mockModelResponse = _fixture.Build<ClassFresherViewModel>().Create();
            // arrange
            _classFresherServiceMock.Setup(
                x => x.UpdateClassFresher(It.IsAny<ClassFresherViewModel>()))
                        .ReturnsAsync(mockModelResponse);
            //act
            var result = await _cLassFresherController.UpdateClassFresherInfomationAsync(mockModelRequest);
            //assert
            _classFresherServiceMock.Verify(
              x => x.UpdateClassFresher(It.Is<ClassFresherViewModel>(
                  x => x.Equals(mockModelRequest))), Times.Once());
            var actionResult = result as OkObjectResult;
            actionResult.Should().NotBeNull();
            var obj = actionResult.Value;
            obj.Should().NotBeNull();
        }
    }
}
