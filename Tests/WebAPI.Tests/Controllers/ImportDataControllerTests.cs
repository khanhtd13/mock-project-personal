﻿using AutoFixture;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.ViewModels.ChemicalsViewModels;
using Global.Shared.ViewModels.ImportViewModels;
using Microsoft.AspNetCore.Http;
using Moq;
using WebAPI.Controllers;

namespace WebAPI.Tests.Controllers
{
    public class ImportDataControllerTests : SetupTest
    {
        private readonly ImportDataController _importDataController;

        public ImportDataControllerTests()
        {
            _importDataController = new ImportDataController(_importDataServiceMock.Object);
        }

        [Fact]
        public void GetDataFromImportFile_ShouldReturnCorrectData()
        {

            // arrange
            _importDataServiceMock.Setup(
                x => x.GetDataFromFileExcelAsync(It.IsAny<IFormFile>())).ReturnsAsync(new PackageReponseImportViewModel());
            // act
            var result = _importDataController.GetDataFromImportFile(It.IsAny<IFormFile>());
            
            _importDataServiceMock.Verify(
                x => x.GetDataFromFileExcelAsync(
                    It.IsAny<IFormFile>()), Times.Once());
            // assert
            result.Should().BeOfType<Task<PackageReponseImportViewModel>>();

        }

        
    }
}
