﻿using Domain.Tests;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.Net.Http.Headers;
using WebAPI.Controllers;
using AutoFixture;
using FluentAssertions;
using Global.Shared.ModelExport;
using Global.Shared.ModelExport.ModelExportConfiguration;

namespace WebAPI.Tests.Services
{
    public class ExcelExportControllerTest : SetupTest
    {
        private readonly MediaTypeHeaderValue _mediatypeHeader;
        private readonly ExportExcelController _exportExcelController;

        public ExcelExportControllerTest()
        {
            _exportExcelController = new ExportExcelController(_mockMEcelExportHistoryService.Object,
                                                              _mockMEcelExportDeliveryService.Object,
                                                              _mockMEcelExportChartService.Object);
            _mediatypeHeader = new MediaTypeHeaderValue("application/octet-stream");

        }

        [Fact]
        public async Task ExportEmployeeTrainingHistory_ReturnLengthShouldBeHeigherThanZero()
        {
            var bytes = _fixture.Build<byte[]>().Create();
            var fileContentResult = new FileContentResult(bytes, _mediatypeHeader);
            var employeeTrainingHistoryOfCourseConfigurations = _fixture.Build<ExportCourseReportViewModel>().CreateMany(10).ToList();
            _mockMEcelExportHistoryService.Setup(x => x.Export(It.IsAny<List<ExportCourseReportViewModel>>()))
                    .ReturnsAsync(fileContentResult);
            var result = await _exportExcelController.ExportEmployeeTrainingHistoryAsync(employeeTrainingHistoryOfCourseConfigurations) as FileContentResult;
            var lenght = result.FileContents.Length;
            lenght.Should().NotBe(0);
        }

        [Fact]
        public async Task ExportEmployeeTrainingHistory_ReturnLengthShouldBeEqualZero()
        {
            var fileContentResult = new FileContentResult(new byte[] { }, _mediatypeHeader);
            var employeeTrainingHistoryOfCourseConfigurations = _fixture.Build<ExportCourseReportViewModel>().CreateMany(10).ToList();
            _mockMEcelExportHistoryService.Setup(x => x.Export(It.IsAny<List<ExportCourseReportViewModel>>()))
                    .ReturnsAsync(fileContentResult);
            var result = await _exportExcelController.ExportEmployeeTrainingHistoryAsync(employeeTrainingHistoryOfCourseConfigurations) as FileContentResult;
            var lenght = result.FileContents.Length;
            lenght.Should().Be(0);
        }

        [Fact]
        public async Task ExportEmployeeTrainingHistory_CallMethodShouldBeOnce()
        {
            //Arrange
            var employeeTrainingHistoryOfCourseConfigurations = _fixture.Build<ExportCourseReportViewModel>().CreateMany(10).ToList();

            //Act
            await _exportExcelController.ExportEmployeeTrainingHistoryAsync(employeeTrainingHistoryOfCourseConfigurations);

            //Assert
            _mockMEcelExportHistoryService.Verify(x=>x.Export(employeeTrainingHistoryOfCourseConfigurations),Times.Once());
        }

        [Fact]
        public async Task ExportEmployeeTrainingDelivery_ReturnLengthShouldBeHeigherThanZero()
        {
            var bytes = _fixture.Build<byte[]>().Create();
            var fileContentResult = new FileContentResult(bytes, _mediatypeHeader);
            _mockMEcelExportDeliveryService.Setup(x => x.Export()).ReturnsAsync(fileContentResult);
            var result = await _exportExcelController.ExportEmployeeTrainingDeliveryAsync() as FileContentResult;
            var lenght = result.FileContents.Length;
            lenght.Should().NotBe(0);
        }

        [Fact]
        public async Task ExportEmployeeTrainingDelivery_ReturnLengthShouldBeEqualZero()
        {
            var fileContentResult = new FileContentResult(new byte[] { }, _mediatypeHeader);
            _mockMEcelExportDeliveryService.Setup(x => x.Export()).ReturnsAsync(fileContentResult);
            var result = await _exportExcelController.ExportEmployeeTrainingDeliveryAsync() as FileContentResult;
            var lenght = result.FileContents.Length;
            lenght.Should().Be(0);
        }

        [Fact]
        public async Task ExportEmployeeTrainingDelivery_CallMethodShouldBeOnce()
        {
            //Arrange

            //Act
            await _exportExcelController.ExportEmployeeTrainingDeliveryAsync();

            //Assert
            _mockMEcelExportDeliveryService.Verify(x => x.Export(), Times.Once());
        }
    }
}
