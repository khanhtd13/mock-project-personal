﻿using Application;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Moq;
using System.Linq.Expressions;

namespace Infrastructures.Tests
{
    public class UnitOfWorkTests : SetupTest
    {
        private readonly IUnitOfWork _unitOfWork;
        public UnitOfWorkTests()
        {
            _unitOfWork = new UnitOfWork(
                _dbContext,
                _chemicalRepositoryMock.Object,
                _scoreRepositoryMock.Object,
                _classFresherRepositoryMock.Object,
                _fresherRepositoryMock.Object,
                _reminderRepositoryMock.Object,
                _fresherReportRepositoryMock.Object,
                _feedbackRepositoryMock.Object,
                _feedbackQuestionRepositoryMock.Object,
                _feedbackResultRepositoryMock.Object,
                _planRepositoryMock.Object,
                _moduleRepositoryMock.Object,
                _topicRepositoryMock.Object,
                _planInforRepositoryMock.Object
                );                   
        }

        [Fact]
        public async Task TestUnitOfWork()
        {
            // arrange
            var mockData = _fixture.Build<Chemical>().CreateMany(10).ToList();

            var fresherReportListMockData = _fixture.Build<FresherReport>().CreateMany(10).ToList();
            var fresherReportMockData = _fixture.Build<FresherReport>().Create();
            var mockExpression = _fixture.Create<Expression<Func<FresherReport, bool>>>();

            _chemicalRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(mockData);
            _fresherReportRepositoryMock
                .Setup(x => x.GetMonthlyReportsByFilterAsync(mockExpression))
                .ReturnsAsync(fresherReportListMockData);

            var mockScore = _fixture.Build<Score>().CreateMany(10).ToList();

            _scoreRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(mockScore);



            // act
            var items = await _unitOfWork.ChemicalRepository.GetAllAsync();

            var reportItemsByExpression = await _unitOfWork.FresherReportRepository
                                                           .GetMonthlyReportsByFilterAsync
                                                           (mockExpression);

            // assert
            items.Should().BeEquivalentTo(mockData);
            reportItemsByExpression.Should().BeEquivalentTo(fresherReportListMockData);
        }
    }
}
