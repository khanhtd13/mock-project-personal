﻿using Application.Repositories;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Infrastructures.Repositories;

namespace Infrastructures.Tests.Repositories
{
    public class ReminderRepositoryTests : SetupTest
    {
        private readonly IReminderRepository _reminderRepository;
        
        public ReminderRepositoryTests()
        {
            _reminderRepository = new ReminderRepository(
                _currentTimeMock.Object,
                _claimsServiceMock.Object,
                _dbContext);
        }

        [Fact]
        public async Task GetAllReminderByDateAsync_ShouldReturnNotNullWhenDataExistsInDatabase()
        {
            var currentDate = _currentTimeMock.Object.GetCurrentTime().Date;
            //arrange
            var mocks = _fixture.Build<Reminder>()
                            .With(e => e.RemindTime1, currentDate.Date)
                            .With(e => e.RemindTime2, currentDate.Date)
                            .CreateMany(10)
                            .ToList();
            await _dbContext.Reminders.AddRangeAsync(mocks);
            await _dbContext.SaveChangesAsync();

            //act
            var result = await _reminderRepository.GetAllReminderByDateAsync(currentDate);

            //assert
            result.Should().BeEquivalentTo(mocks);
        }
           [Fact]
           public async Task GetAllReminderByDateAsync_ShouldReturnNullWhenDataNotExistsInDatabase()
            {
                var currentDate = _currentTimeMock.Object.GetCurrentTime().Date;
            var remindTime = _currentTimeMock.Object.GetCurrentTime().Date.AddDays(3);
                //arrange
                var mocks = _fixture.Build<Reminder>()
                                .With(e => e.RemindTime1, remindTime)
                                .With(e => e.RemindTime2, remindTime)
                                .CreateMany(10)
                                .ToList();
                await _dbContext.Reminders.AddRangeAsync(mocks);
                await _dbContext.SaveChangesAsync();
                   
                //act
                var result = await _reminderRepository.GetAllReminderByDateAsync(currentDate);

                //assert
                result.Should().BeNullOrEmpty();

            }
        }
   }
