﻿using Application.Repositories;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.ViewModels.FeedbackViewModels;
using Infrastructures.Repositories;

namespace Infrastructures.Tests.Repositories
{
    public class FeedbackResultRepositoryTests  : SetupTest
    {
        private readonly IFeedbackResultRepository _feedbackResultRepository;
        public FeedbackResultRepositoryTests()
        {
            _feedbackResultRepository = new FeedbackResultRepository(
                                                _dbContext,
                                                _currentTimeMock.Object,
                                                _claimsServiceMock.Object);
        }

        [Fact]
        public async Task GetAllAsync_Should_ReturnCorrectData()
        {
            // arrange
            var mockData = _fixture.Build<FeedBackResult>()
                                    .Without(x => x.FeedBack)
                                    .Without(x => x.FeedBackQuestion)
                                    .With(x => x.IsDeleted, false)
                                    .CreateMany(10)
                                    .ToList();
            await _dbContext.FeedBackResults.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            // act
            var result = await _feedbackResultRepository.GetAllAsync();

            // assert
            result.Should().BeEquivalentTo(mockData);
        }
        
        [Fact]
        public async Task SearchAsync_WithFilterCreateDate_ShouldReturnCorrectData()
        {
            // arrange
            var currentTime = _currentTimeMock.Object.GetCurrentTime();
            var mockCreationDateBeAssigned = _fixture.Build<FeedBackResult>()
                                                .Without(x => x.FeedBack)                                  
                                                .Without(x => x.FeedBackQuestion)
                                                .With(x => x.IsDeleted, false)
                                                .With(x => x.CreationDate, currentTime)
                                                .CreateMany(10)
                                                .ToList();
            var mockData = new List<FeedBackResult>(mockCreationDateBeAssigned);
            var mockDataSalt = _fixture.Build<FeedBackResult>()
                                        .Without(x => x.FeedBack)
                                        .Without(x => x.FeedBackQuestion)
                                        .With(x => x.IsDeleted, false)
                                        .With(x=>x.CreationDate, currentTime.AddDays(1))
                                        .CreateMany(90)
                                        .ToList();
            mockData.AddRange(mockDataSalt);

            await _dbContext.FeedBackResults.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            var mockQuery = _fixture.Build<SearchFeedbackResultViewModel>()
                        .With(x => x.CreationDate, currentTime)
                        .With(x => x.PageIndex, 0)
                        .With(x => x.PageSize, 100)
                        .Without(x => x.AccountName)
                        .Without(x => x.QuestionId)
                        .Create();
            var mockResultPagination = _fixture.Build<Pagination<FeedBackResult>>()
                                    .With(x => x.Items, mockCreationDateBeAssigned)
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.TotalItemsCount, 10)
                                    .Create();
            // act
            var result = await _feedbackResultRepository.SearchAsync(mockQuery);

            // assert
            result.Should().BeEquivalentTo(mockResultPagination);
        }

        [Fact]
        public async Task SearchAsync_WithFilterFeedbackId_ShouldReturnCorrectData()
        {
            // arrange
            var mockFeedBackQuestion = _fixture.Build<FeedBackQuestion>()
                                                .Without(x => x.FeedBackResults)
                                                .Without(x => x.FeedBack)
                                                .With(x => x.IsDeleted, false)
                                                .Create();
            var mockQuestionIdBeAssigned = _fixture.Build<FeedBackResult>()
                                    .With(x => x.QuestionId, mockFeedBackQuestion.Id)
                                    .With(x => x.IsDeleted, false)
                                    .Without(x => x.FeedBack)
                                    .Without(x => x.FeedBackQuestion)
                                    .CreateMany(10)
                                    .ToList();
            var mockData = new List<FeedBackResult>(mockQuestionIdBeAssigned);
            var mockDataSalt = _fixture.Build<FeedBackResult>()
                                    .Without(x => x.FeedBackQuestion)
                                    .Without(x => x.FeedBack)
                                    .With(x => x.IsDeleted, false)
                                    .CreateMany(90)
                                    .ToList();
            mockData.AddRange(mockDataSalt);
            var mockQuery = _fixture.Build<SearchFeedbackResultViewModel>()
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.QuestionId, mockFeedBackQuestion.Id)
                                    .Without(x => x.AccountName)
                                    .Without(x => x.CreationDate)
                                    .Create();
            await _dbContext.FeedBackQuestions.AddAsync(mockFeedBackQuestion);
            await _dbContext.FeedBackResults.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            mockQuestionIdBeAssigned = mockQuestionIdBeAssigned.Select(x => new FeedBackResult
            {
                Id = x.Id,
                Content = x.Content,
                FeedBackId = x.FeedBackId,
                QuestionId = x.QuestionId,
                QuestionTitle = x.QuestionTitle,
                AccountFresherId = x.AccountFresherId,
                AccountName = x.AccountName,
                Fullname = x.Fullname,
                Note = x.Note,
                CreationDate = x.CreationDate,
                CreatedBy = x.CreatedBy,
                DeleteBy = x.DeleteBy,
                DeletionDate = x.DeletionDate,
                IsDeleted = x.IsDeleted,
                ModificationBy = x.ModificationBy,
                ModificationDate = x.ModificationDate
            }).ToList();
            var mockResultPagination = _fixture.Build<Pagination<FeedBackResult>>()
                                    .With(x => x.Items, mockQuestionIdBeAssigned)
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.TotalItemsCount, 10)
                                    .Create();
            // act
            var result = await _feedbackResultRepository.SearchAsync(mockQuery);

            // assert
            result.Should().BeEquivalentTo(mockResultPagination);
        }
    }
}
