﻿using Application.Repositories;
using AutoFixture;
using Domain.Entities;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.Commons;
using Global.Shared.ViewModels.FeedbackViewModels;
using Infrastructures.Repositories;

namespace Infrastructures.Tests.Repositories
{
    public class FeedbackQuestionRepositoryTests : SetupTest
    {
        private readonly IFeedbackQuestionRepository _feedbackQuestionRepository;
        public FeedbackQuestionRepositoryTests()
        {
            _feedbackQuestionRepository = new FeedbackQuestionRepository(
                                                _dbContext,
                                                _currentTimeMock.Object,
                                                _claimsServiceMock.Object);
        }

        [Fact]
        public async Task GetAllAsync_Should_ReturnCorrectData()
        {
            // arrange
            var mockData = _fixture.Build<FeedBackQuestion>()
                                    .Without(x => x.FeedBackResults)
                                    .Without(x => x.FeedBack)
                                    .With(x => x.IsDeleted, false)
                                    .CreateMany(10)
                                    .ToList();
            await _dbContext.FeedBackQuestions.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            // act
            var result = await _feedbackQuestionRepository.GetAllAsync();

            // assert
            result.Should().BeEquivalentTo(mockData);
        }
        [Fact]
        public async Task SearchAsync_WithFilterTitle_ShouldReturnCorrectData()
        {
            // arrange
            var mockTitleBeAssigned = _fixture.Build<FeedBackQuestion>()
                                                .Without(x => x.FeedBack)
                                                .Without(x => x.FeedBackResults)
                                                .With(x => x.IsDeleted, false)
                                                .With(x => x.Title, "Title Test")
                                                .CreateMany(10)
                                                .ToList();
            var mockData = new List<FeedBackQuestion>(mockTitleBeAssigned);
            var mockResultPagination = _fixture.Build<Pagination<FeedBackQuestion>>()
                                    .With(x => x.Items, mockTitleBeAssigned)
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.TotalItemsCount, 10)
                                    .Create();
            var mockDataSalt = _fixture.Build<FeedBackQuestion>()
                                        .Without(x => x.FeedBackResults)
                                        .Without(x => x.FeedBack)
                                        .With(x => x.IsDeleted, false)
                                        .CreateMany(90)
                                        .ToList();
            mockData.AddRange(mockDataSalt);
            var mockQuery = _fixture.Build<SearchFeedbackQuestionViewModel>()
                                    .With(x => x.Title, "Title T")
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .Without(x => x.CreateBy)
                                    .Without(x => x.CreationDate)
                                    .Without(x => x.FeedBackId)
                                    .Create();
            await _dbContext.FeedBackQuestions.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            // act
            var result = await _feedbackQuestionRepository.SearchAsync(mockQuery);

            // assert
            result.Should().BeEquivalentTo(mockResultPagination);
        }

        [Fact]
        public async Task SearchAsync_WithFilterCreateDate_ShouldReturnCorrectData()
        {
            // arrange
            var currentTime = _currentTimeMock.Object.GetCurrentTime();
            var mockTitleBeAssigned = _fixture.Build<FeedBackQuestion>()
                                                .Without(x => x.FeedBackResults)
                                                .Without(x => x.FeedBack)
                                                .With(x => x.IsDeleted, false)
                                                .With(x => x.CreationDate, currentTime)
                                                .CreateMany(10)
                                                .ToList();
            var mockData = new List<FeedBackQuestion>(mockTitleBeAssigned);
            var mockDataSalt = _fixture.Build<FeedBackQuestion>()
                                        .Without(x => x.FeedBackResults)
                                        .Without(x => x.FeedBack)
                                        .With(x => x.IsDeleted, false)
                                        .With(x => x.CreationDate, currentTime.AddDays(1))
                                        .CreateMany(90)
                                        .ToList();
            var mockResultPagination = _fixture.Build<Pagination<FeedBackQuestion>>()
                                    .With(x => x.Items, mockTitleBeAssigned)
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.TotalItemsCount, 10)
                                    .Create();
            mockData.AddRange(mockDataSalt);

            await _dbContext.FeedBackQuestions.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            var mockQuery = _fixture.Build<SearchFeedbackQuestionViewModel>()
                        .With(x => x.CreationDate, currentTime)
                        .With(x => x.PageIndex, 0)
                        .With(x => x.PageSize, 100)
                        .Without(x => x.CreateBy)
                        .Without(x => x.Title)
                        .Without(x => x.FeedBackId)
                        .Create();
            // act
            var result = await _feedbackQuestionRepository.SearchAsync(mockQuery);

            // assert
            result.Should().BeEquivalentTo(mockResultPagination);
        }

        [Fact]
        public async Task SearchAsync_WithFilterFeedbackId_ShouldReturnCorrectData()
        {
            // arrange
            var mockFeedback = _fixture.Build<FeedBack>()
                          .Without(x => x.FeedBackQuestions)
                          .Without(x => x.FeedBackResults)
                          .With(x => x.IsDeleted, false)
                          .Create();
            var mockFeedbackIdBeAssigned = _fixture.Build<FeedBackQuestion>()
                                    .With(x => x.FeedBackId, mockFeedback.Id)
                                    .With(x => x.IsDeleted, false)
                                    .With(x => x.Title, "Title Test")
                                    .Without(x => x.FeedBackResults)
                                    .Without(x => x.FeedBack)
                                    .CreateMany(10)
                                    .ToList();
            var mockData = new List<FeedBackQuestion>(mockFeedbackIdBeAssigned);
            var mockDataSalt = _fixture.Build<FeedBackQuestion>()
                                    .Without(x => x.FeedBackResults)
                                    .Without(x => x.FeedBack)
                                    .With(x => x.IsDeleted, false)
                                    .CreateMany(90)
                                    .ToList();
            mockData.AddRange(mockDataSalt);
            var mockQuery = _fixture.Build<SearchFeedbackQuestionViewModel>()
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.FeedBackId, mockFeedback.Id)
                                    .Without(x => x.Title)
                                    .Without(x => x.CreateBy)
                                    .Without(x => x.CreationDate)
                                    .Create();
            await _dbContext.FeedBacks.AddAsync(mockFeedback);
            await _dbContext.FeedBackQuestions.AddRangeAsync(mockData);
            await _dbContext.SaveChangesAsync();

            mockFeedbackIdBeAssigned = mockFeedbackIdBeAssigned.Select(x => new FeedBackQuestion
            {
                Id = x.Id,
                Content = x.Content,
                FeedBackId = x.FeedBackId,
                Title = x.Title,
                CreationDate = x.CreationDate,
                CreatedBy = x.CreatedBy,
                DeleteBy = x.DeleteBy,
                DeletionDate = x.DeletionDate,
                Description = x.Description,
                IsDeleted = x.IsDeleted,
                ModificationBy = x.ModificationBy,
                ModificationDate = x.ModificationDate
            }).ToList();
            var mockResultPagination = _fixture.Build<Pagination<FeedBackQuestion>>()
                                    .With(x => x.Items, mockFeedbackIdBeAssigned)
                                    .With(x => x.PageIndex, 0)
                                    .With(x => x.PageSize, 100)
                                    .With(x => x.TotalItemsCount, 10)
                                    .Create();
            // act
            var result = await _feedbackQuestionRepository.SearchAsync(mockQuery);

            // assert
            result.Should().BeEquivalentTo(mockResultPagination);
        }
    }
}
