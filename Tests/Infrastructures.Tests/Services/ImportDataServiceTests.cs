﻿using Application.Interfaces;
using Domain.Tests;
using FluentAssertions;
using Global.Shared.ViewModels.ImportViewModels;
using Infrastructures.Services;
using Microsoft.AspNetCore.Http;
using Moq;

namespace Infrastructures.Tests.Services
{
    public class ImportDataServiceTests : SetupTest
    {
        private readonly IImportDataService _importDataService;
        public ImportDataServiceTests()
        {
            _importDataService = new ImportDataService(_unitOfWorkMock.Object);
        }

        [Fact]
        public async void GetDataFromFileExcelAsync_ShouldReturnNull_WhenExcelFileIsNull()
        {

            // act
            IFormFile? mock = null;
            var result = await _importDataService.GetDataFromFileExcelAsync(mock);

            _importDataServiceMock.Verify(
                x => x.CreateClassCodeFromPackageDataReponseAsync(
                    It.IsAny<PackageReponseImportViewModel>()), Times.Never());
            // assert
            result.Should().BeNull();
        }

        [Fact]
        public async void GetDataFromFileExcelAsync_ShouldBeNull_WhenExcelMapperCanNotReadTheExcelFile()
        {

            // act
            IFormFile? mock = It.IsNotNull<IFormFile>();
            var result = await _importDataService.GetDataFromFileExcelAsync(mock);

            _importDataServiceMock.Verify(
                x => x.CreateClassCodeFromPackageDataReponseAsync(
                    It.IsAny<PackageReponseImportViewModel>()), Times.Never());
            // assert
            result.Should().BeNull();
        }

    }
}
